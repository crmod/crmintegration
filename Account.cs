﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

namespace CRMIntegration
{
    public class Account
    {
        private String accountName;

        public String AccountName
        {
            get { return accountName; }
            set { accountName = value; }
        }
        private String location;

        public String Location
        {
            get { return location; }
            set { location = value; }
        }
        private String accountRowId;

        public String AccountRowId
        {
            get { return accountRowId; }
            set { accountRowId = value; }
        }
    }
}