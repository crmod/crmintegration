﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using System.Data;

namespace CRMIntegration
{
    public class CRM_myPartnerPlus_SRAssessment
    {
        public String saveMyPartnerPlusSurveyResponsesToXML(String pSRRowId,String pTemplateId, String pSRSubject, String pQ1Response, String pQ2Response, String pQ3Response)
        {
            LogToFile("Starting to save XML file");

            String xmlFilePath = ConfigurationManager.AppSettings["mypartnerplus_survey_xml"].ToString();

            Stream fs = new FileStream(xmlFilePath + "PP_myPartnerPlusSurvey_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xml",FileMode.Create);
            XmlTextWriter xmlWriter = new XmlTextWriter(fs, System.Text.Encoding.UTF8);
            String result = "";
            string[] question1;
            string[] question2;
            string[] question3;


            try
            {
                LogToFile("Spliting the variable responses");
                
                question1 = pQ1Response.Split('|');
                question2 = pQ2Response.Split('|');
                question3 = pQ3Response.Split('|');

                xmlWriter.WriteStartDocument(true);
                xmlWriter.Formatting = Formatting.Indented;
                xmlWriter.Indentation = 2;
                xmlWriter.WriteStartElement("myPartnerPlusSurvey");

                xmlWriter.WriteStartElement("ServiceRequest-Assessment");

                xmlWriter.WriteStartElement("SRRowId");
                xmlWriter.WriteString(pSRRowId);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("SRSubject");
                xmlWriter.WriteString(pSRSubject);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("AssessmentTemplateId");
                xmlWriter.WriteString(pTemplateId);
                xmlWriter.WriteEndElement();

                xmlWriter.WriteStartElement("ListOfQuestionsResponded");

                //Q1 Always has an answer
                createNodeForMyPartnerPlusFeedbackResponses(question1[0].ToString(), question1[1].ToString(), question1[2].ToString(), xmlWriter);

                //Evaluate answer in Q2, comments area should have a value before saving of record will be applied
                if(question2[2].ToString().Length > 0)
                {
                    createNodeForMyPartnerPlusFeedbackResponses(question2[0].ToString(), question2[1].ToString(), question2[2].ToString(), xmlWriter);
                }

                //Evaluate answer in Q3, comments area should have a value before saving of record will be applied
                if (question3[1].ToString().Length > 0 )
                {
                    createNodeForMyPartnerPlusFeedbackResponses(question3[0].ToString(), question3[1].ToString(), question3[2].ToString(), xmlWriter);
                }

                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
                xmlWriter.Close();

                LogToFile("Done on saving the XML");

                LogToFile("XML File created!--> " + fs);
            }
            catch (Exception ex)
            {
                result = "error";
                LogToFile("Error on saveMyPartnerPlusSurveyResponsesToXML(): " + ex.StackTrace);
            }

            LogToFile("Ended to save XML file");

            return result;
        }

        public static void createNodeForMyPartnerPlusFeedbackResponses(String pQuestionRowId, String pResponse,String pComments,XmlWriter pXmlWriter) // String pQ2Response,String pQ3Response,XmlTextWriter pXmlWriter)
        {
           
            pXmlWriter.WriteStartElement("QuestionResponses");

                pXmlWriter.WriteStartElement("QuestionRowId");
                pXmlWriter.WriteString(pQuestionRowId);
                pXmlWriter.WriteEndElement();

                pXmlWriter.WriteStartElement("Response");
                pXmlWriter.WriteString(pResponse);
                pXmlWriter.WriteEndElement();

                pXmlWriter.WriteStartElement("Comments");
                pXmlWriter.WriteString(pComments);
                pXmlWriter.WriteEndElement();

            pXmlWriter.WriteEndElement();
        }

        public static void LogToFile(String strLogText)
        {
            String filename = "LogFile_PP_myPartnerPlusSurvey_" + DateTime.Today.ToString("yyyyMMdd") + ".txt";
            String path = ConfigurationManager.AppSettings["LogFilesFolder"].ToString();
            bool isPathExist = Directory.Exists(path);
            StreamWriter swLogTransaction;

            try
            {
                if (!isPathExist)
                {
                    Directory.CreateDirectory(path);
                }

                if (!File.Exists(path + filename))
                {
                    swLogTransaction = new StreamWriter(path + filename);
                }
                else
                {
                    swLogTransaction = File.AppendText(path + filename);
                }

                try
                {
                    //Write to the File
                    swLogTransaction.WriteLine(DateTime.Now + " -- " + strLogText);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
                finally
                {
                    swLogTransaction.Dispose();
                    swLogTransaction.Close();
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }
    }
}