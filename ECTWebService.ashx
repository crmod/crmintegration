﻿<%@ WebHandler Language="C#" Class="ECTWebService" %>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Web;

/// <summary>
/// Emerson Climate Technologies Web Service
/// </summary>
public class ECTWebService : IHttpHandler
{
    private enum curTransType { AsiaPartnerWebData, DixellData, Emailer }

    /*
    EXPECTED PARAMATERS
    type   : Get Asia PartnerWeb User Data = 0; Get Dixell User Data = 1;  Send Email = 2
    param  : the username of the user account to be search
    multi  : Sending email - if recipient is more than 1
    from   : Sending email - email sender
    to     : Sending email - email recipient
    subject: Sending email - email subject
    body   : Sending email - email body
    ishtml : Sending email - email body format
    */
       
    /// <summary>
    /// Invoking web service
    /// </summary>
    /// <param name="context">contains the parameters</param>
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";
        ResponseObject ro = new ResponseObject();
        System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        
        try
        {
            string transactionType = context.Request["type"];
            curTransType currentTransactionType;

            if (!string.IsNullOrEmpty(transactionType) && this.IsValidInteger(transactionType))
            {
                if (transactionType == "0")
                {
                    currentTransactionType = curTransType.AsiaPartnerWebData;
                }
                else if (transactionType == "1") 
                {
                    currentTransactionType = curTransType.DixellData;
                }
                else if (transactionType == "2")
                {
                    currentTransactionType = curTransType.Emailer;
                }
                else
                {
                    throw new Exception("Invalid Request Parameters");
                }
            }
            else 
            {
                throw new Exception("Invalid Request Parameters");
            }

            //?type=0&param=06100098
            if (currentTransactionType == curTransType.AsiaPartnerWebData) 
            {
                string username = context.Request["param"];
                
                if (!string.IsNullOrEmpty(username)) 
                { 
                    ro.ResponseItem = this.FetchAsiaPartnerTableData(username); 
                }
                else 
                {
                    throw new Exception("Invalid Request Parameters");
                }
            }
            //?type=1&param=admin
            else if (currentTransactionType == curTransType.DixellData) 
            {
                string username = context.Request["param"];

                if (!string.IsNullOrEmpty(username))
                {
                    ro.ResponseItem = this.FetchDixellTableData(username);
                }
                else
                {
                    throw new Exception("Invalid Request Parameters");
                }
            }
            //?type=1&multi=1&from=daniel.ong@emerson.com&to=christian.robles@emerson.com,bianca.legaspi@emerson.com&subject=This%20Is%20A%20Test%20From%20Emailer%20WebService&body=test%20lang%20uli%20using%20multi%20recipient
            else if (currentTransactionType == curTransType.Emailer)
            {
                string multiRecipient = context.Request["multi"];
                string sender = context.Request["from"];
                string recipient = context.Request["to"];
                string subject = context.Request["subject"];
                string body = context.Request["body"];
                string isBodyHTML = context.Request["ishtml"];
                bool isHTML = false;

                if (!string.IsNullOrEmpty(isBodyHTML))
                {
                    if (isBodyHTML == "0")
                    {
                        isHTML = false;
                    }
                    else
                    {
                        isHTML = true;
                    }
                }

                if (!string.IsNullOrEmpty(sender) || !string.IsNullOrEmpty(recipient) || !string.IsNullOrEmpty(subject) || !string.IsNullOrEmpty(body))
                {
                    if (!string.IsNullOrEmpty(multiRecipient))
                    {
                        if (multiRecipient == "0")
                        {
                            this.SendEmail(sender, recipient, subject, body, false, isHTML);
                        }
                        else
                        {
                            this.SendEmail(sender, recipient, subject, body, true, isHTML);
                        }
                    }
                    else
                    {
                        this.SendEmail(sender, recipient, subject, body, false, isHTML);
                    }
                }
                else
                {
                    throw new Exception("Invalid Request Parameters");
                }
            }
            else
            {
                throw new Exception("Invalid Request Parameters");
            }   
        }
        catch (Exception ex)
        {
            ro.ErrorMessage = ex.Message;
        }
        
        string sJSON = oSerializer.Serialize(ro);
        context.Response.Write(sJSON);
    }

    /// <summary>
    /// Fetch Dixell User Data
    /// </summary>
    /// <param name="username">Username of the user account</param>
    /// <returns>List of users that match the criteria</returns>
    private List<DixellSecuredSiteData> FetchDixellTableData(string username) 
    {
        List<DixellSecuredSiteData> returnTable = new List<DixellSecuredSiteData>();
        string sqlSelect = "sp_GetAccountDetailsByUName";
        string sqlConnectionString = GlobalVariables.DixellConnectionString;

        using (SqlConnection sqlconn = new SqlConnection(sqlConnectionString))
        {
            using (SqlCommand sqlcomm = new SqlCommand(sqlSelect, sqlconn))
            {
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Parameters.AddWithValue("@Uname", username);

                using (SqlDataAdapter sqlda = new SqlDataAdapter(sqlcomm))
                {
                    try
                    {
                        DataTable dt = new DataTable();
                        sqlda.Fill(dt);
                        foreach (DataRow row in dt.Rows) 
                        {
                            DixellSecuredSiteData tableRow = new DixellSecuredSiteData();
                            tableRow.LastName = row["SurnameOrCompany"].ToString();
                            tableRow.FirstName = row["NameOrReference"].ToString();
                            tableRow.Email = row["Email"].ToString();
                            tableRow.AreaCode = row["AreaCode"].ToString();
                            tableRow.TownOrCity = row["AreaCode"].ToString();
                            tableRow.Country = row["Country"].ToString();
                            tableRow.IsActivated = Convert.ToBoolean(row["isActivated"]);
                            tableRow.Role = row["Role"].ToString();
                            returnTable.Add(tableRow);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error in fetching data. " + ex.Message);
                    }
                }
            }
        }
        return returnTable;
    }
    
    /// <summary>
    /// Fetch Asia PartnerWeb User Data
    /// </summary>
    /// <param name="username">Username of the user account</param>
    /// <returns>List of users that match the criteria</returns>
    private List<AsiaPartnerWebSecuredSiteData> FetchAsiaPartnerTableData(string username) 
    {
        List<AsiaPartnerWebSecuredSiteData> returnTable = new List<AsiaPartnerWebSecuredSiteData>();
        
        string sqlSelect = "sp_GetAccountDetailsByID";
        string sqlConnectionString = GlobalVariables.AsiaPartnerWebDixellConnectionString;
        
        using (SqlConnection sqlconn = new SqlConnection(sqlConnectionString)) 
        {
            using (SqlCommand sqlcomm = new SqlCommand(sqlSelect, sqlconn))
            {
                sqlcomm.CommandType = CommandType.StoredProcedure;
                sqlcomm.Parameters.AddWithValue("@username", username);
                using (SqlDataAdapter sqlda = new SqlDataAdapter(sqlcomm)) 
                {
                    try
                    {
                        DataTable dt = new DataTable();
                        sqlda.Fill(dt);
                        foreach (DataRow row in dt.Rows)
                        {
                            AsiaPartnerWebSecuredSiteData tableRow = new AsiaPartnerWebSecuredSiteData();
                            tableRow.UserName = row["ID"].ToString();
                            tableRow.FirstName = row["C_FirstName"].ToString();
                            tableRow.LastName = row["C_LastName"].ToString();
                            tableRow.MiddleName = row["C_MiddleName"].ToString();
                            tableRow.Email = row["C_Email"].ToString();
                            tableRow.Phone = row["C_Phone"].ToString();
                            tableRow.Mobile = row["C_Mobile"].ToString();
                            tableRow.Country = row["C_Country"].ToString();
                            tableRow.Job = row["C_Job"].ToString();
                            tableRow.YearsOfExperience = Convert.ToInt16(row["C_Experience"].ToString());
                            tableRow.Role = row["C_Role"].ToString();
                            tableRow.CompanyName = row["A_Name"].ToString();
                            tableRow.CompanyPhone = row["A_Phone"].ToString();
                            tableRow.CompanyPhoneExtension = row["A_Ext"].ToString();
                            tableRow.CompanyAddress = row["A_Address"].ToString();
                            tableRow.CompanyCity = row["A_City"].ToString();
                            tableRow.CompanyState = row["A_State"].ToString();
                            tableRow.CompanyCountry = row["A_Country"].ToString();
                            tableRow.CompanyZip = row["A_Zip"].ToString();
                            tableRow.TypeOfBusiness = row["A_TypeOfBusiness"].ToString();
                            tableRow.Interest = row["A_Interest"].ToString();
                            tableRow.Application = row["A_Application"].ToString();
                            returnTable.Add(tableRow);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Error in fetching data. " + ex.Message);
                    }
                }
            }
        }
        return returnTable;
    }
    
    /// <summary>
    /// Send Email
    /// </summary>
    /// <param name="emailSender">Email address of the sender</param>
    /// <param name="emailRecipient">Email address of the recipient</param>
    /// <param name="emailSubject">Email Subject</param>
    /// <param name="emailBody">Email Body</param>
    /// <param name="isMultiRecipient">True if the recipient is more than 1, False if recipient is 1</param>
    /// <param name="isBodyHTML">True if the body of the email is in HTML format, False if not</param>
    private void SendEmail(string emailSender, string emailRecipient, string emailSubject, string emailBody, bool isMultiRecipient, bool isBodyHTML) 
    {
        try
        {
            string host = GlobalVariables.ECTWebServiceEmailerHost;
            MailAddress from = new MailAddress(emailSender);
            
            SmtpClient client = new SmtpClient();
            client.Host = host;
            
            if (isMultiRecipient) 
            {
                MailMessage message = new MailMessage();
                message.To.Add(emailRecipient);
                message.From = new MailAddress(emailSender);
                message.Subject = emailSubject;
                message.IsBodyHtml = isBodyHTML;
                message.Body = emailBody;
                client.Send(message);
            } 
            else 
            {
                MailAddress to = new MailAddress(emailRecipient);
                MailMessage message = new MailMessage(from, to);
                message.Subject = emailSubject;
                message.IsBodyHtml = isBodyHTML;
                message.Body = emailBody;
                client.Send(message);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error in sending email. " + ex.Message);
        }
    }
    
    /// <summary>
    /// If class is reusable
    /// </summary>
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    
    /// <summary>
    /// Check a string if it is a valid integer
    /// </summary>
    /// <param name="stringSource">String to check</param>
    /// <returns>True if valid, False if not</returns>
    private bool IsValidInteger(string stringSource)
    {
        int n;
        bool isNumeric = int.TryParse(stringSource, out n);
        return isNumeric;
    }
}

/// <summary>
/// Static Class that holds the connection string for the webservice
/// </summary>
public static class GlobalVariables 
{
    public static string AsiaPartnerWebDixellConnectionString 
    { 
        get 
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["AsiaPartnerWebUserDB"].ConnectionString;
        } 
    }
    
    public static string DixellConnectionString 
    { 
        get 
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["DixellUserDB"].ConnectionString;
        } 
    }

    public static string ECTWebServiceEmailerHost 
    { 
        get { 
            return System.Web.Configuration.WebConfigurationManager.AppSettings["ECTWebServiceEmailerHost"]; 
        } 
    }
}

/// <summary>
/// Class that represents the structure of the Dixell User Data
/// </summary>
public class DixellSecuredSiteData 
{
    private string lastName;
    private string firstName;
    private string email;
    private string address;
    private string areaCode;
    private string townOrCity;
    private string country;
    private bool isActivated;
    private string role;

    public string LastName { get { return this.lastName; } set { this.lastName = value; } }
    public string FirstName { get { return this.firstName; } set { this.firstName = value; } }
    public string Email { get { return this.email; } set { this.email = value; } }
    public string Address { get { return this.address; } set { this.address = value; } }
    public string AreaCode { get { return this.areaCode; } set { this.areaCode = value; } }
    public string TownOrCity { get { return this.townOrCity; } set { this.townOrCity = value; } }
    public string Country { get { return this.country; } set { this.country = value; } }
    public bool IsActivated { get { return this.isActivated; } set { this.isActivated = value; } }
    public string Role { get { return this.role; } set { this.role = value; } }
}

/// <summary>
/// Class that represents the structure of the Asia PartnerWeb User Data
/// </summary>
public class AsiaPartnerWebSecuredSiteData
{
    private string lastName;
    private string firstName;
    private string middleName;
    private string email;
    private string phone;
    private string mobile;
    private string country;
    private string job;
    private int yearsOfExperience;
    private string companyName;
    private string companyPhone;
    private string companyPhoneExt;
    private string companyAddress;
    private string companyCity;
    private string companyState;
    private string companyCountry;
    private string companyZip;
    private string typeOfBusiness;
    private string application;
    private string interest;
    private string username;
    private string role;

    public string LastName { get { return this.lastName; } set { this.lastName = value; } }
    public string FirstName { get { return this.firstName; } set { this.firstName = value; } }
    public string MiddleName { get { return this.middleName; } set { this.middleName = value; } }
    public string Email { get { return this.email; } set { this.email = value; } }
    public string Phone { get { return this.phone; } set { this.phone = value; } }
    public string Mobile { get { return this.mobile; } set { this.mobile = value; } }
    public string Country { get { return this.country; } set { this.country = value; } }
    public string Job { get { return this.job; } set { this.job = value; } }
    public int YearsOfExperience { get { return this.yearsOfExperience; } set { this.yearsOfExperience = value; } }
    public string CompanyName { get { return this.companyName; } set { this.companyName = value; } }
    public string CompanyPhone { get { return this.companyPhone; } set { this.companyPhone = value; } }
    public string CompanyPhoneExtension { get { return this.companyPhoneExt; } set { this.companyPhoneExt = value; } }
    public string CompanyAddress { get { return this.companyAddress; } set { this.companyAddress = value; } }
    public string CompanyCity { get { return this.companyCity; } set { this.companyCity = value; } }
    public string CompanyState { get { return this.companyState; } set { this.companyState = value; } }
    public string CompanyCountry { get { return this.companyCountry; } set { this.companyCountry = value; } }
    public string CompanyZip { get { return this.companyZip; } set { this.companyZip = value; } }
    public string TypeOfBusiness { get { return this.typeOfBusiness; } set { this.typeOfBusiness = value; } }
    public string Application { get { return this.application; } set { this.application = value; } }
    public string Interest { get { return this.interest; } set { this.interest = value; } }
    public string UserName { get { return this.username; } set { this.username = value; } }
    public string Role { get { return this.role; } set { this.role = value; } }
}

/// <summary>
/// Standard item to be returned to the client upon calling JSON Script Services
/// </summary>
public class ResponseObject
{
    private object responseItem;
    private string errorMessage;

    public object ResponseItem { get { return this.responseItem; } set { this.responseItem = value; } }
    public string ErrorMessage { get { return this.errorMessage; } set { this.errorMessage = value; } }

    public ResponseObject() { }
    public ResponseObject(object _responseItem, string _errorMessage)
    {
        this.responseItem = _responseItem;
        this.errorMessage = _errorMessage;
    }
}
