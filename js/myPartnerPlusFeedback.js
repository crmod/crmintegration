﻿var srRowId = "";
var srNumber = "";
var srSubject = "";
var srEmail = "";

var q1ID = "" ;
var q2ID = "";
var q3ID = "";
var assessmentTemplateId = "";
function getSRId()
{
    var qrStr = window.location.search;
    var arrQrStr = new Array();
    var arrKeyVal = new Array();

    ////split each of pair
    var arr = qrStr.split('&');

    //alert(arr);
    for (var i = 0; i < arr.length; i++) {
        //splits each of field-value pair
        var index = arr[i].indexOf('=');

        var key = arr[i].substring(0, index);

        if (key.indexOf("?") >= 0)
        {
            key = key.substring(1);
        }

        var val = arr[i].substring(index + 1);

        arrKeyVal.push(key);

        arrKeyVal[key] = val;
    }

    srRowId = arrKeyVal["sid"];

    srSubject = arrKeyVal["subj"];
    var derSubj = srSubject.replace(/%20/g," ");

    srNumber = arrKeyVal["sr"];

    showMyPartnerPlusQuestions(srRowId, srNumber, derSubj, srEmail);
}

function getSRDetails(serviceRequestId)
{
    var param = new Object();
    param.pSRRowId = serviceRequestId;

    callAjaxRequestJSON("getSRDetails", param, showMyPartnerPlusQuestions, ajaxError);
}

function showMyPartnerPlusQuestions(srRowId,srNumber,srSubject,srEmail)
{
    var strHTML = "";
    q1ID = pQ1ID;
    q2ID = pQ2ID;
    q3ID = pQ3ID;
    assessmentTemplateId = tid;
    
    strHTML += "<table style='width: 863px'><tr><td></td><td></td></tr>";

    strHTML += "<tr><td></br></td></tr>";
    strHTML += "<tr><td class='label'>Service Request Number:</td></tr>";
    strHTML += "<tr><td class='text'>" + srNumber + "</td></tr>";
    strHTML += "<tr><td></br></td></tr>";

    strHTML += "<tr><td class='label'>Description:</td></tr>";
    strHTML += "<tr><td class='text'> " + srSubject.replace(/%20/g, " ") + "</td></tr>";

    strHTML += "<tr><td></br></td></tr>";
    strHTML += "<tr><td><input type='hidden' id=templateId value='" + assessmentTemplateId + "' display='none'></td></tr>";
    strHTML += "<tr><td class='label'>Was your issue resolved?</td></tr>";
    strHTML += "<tr><td class='text'><input type='radio' id='q1R_Yes' name='q1ID' onchange='javascript:validateResponseOnQ1();' checked value='Yes'>Yes <input type='radio' id='q1R_No' name='q1ID' onchange='javascript:validateResponseOnQ1IfNo();' value='No'>No</td></tr>";

    strHTML += "<tr><td></br></td></tr>";
    strHTML += "<tr><td align='center' colspan='4'><input type='button' id='btnSubmit' onclick='javascript:saveResponsesOnMyPartnerPlusSurvey()' value ='Submit' class='button'><input type='button' id='btnCancel' onclick='javascript:clearFields()' value ='Cancel' class='button' ></td></tr>";
    strHTML += "<tr><td align='center'><img alt='loading' id='regLoadingImg' src='images/ajaxLoading.gif' style='display:none; position:relative;width:30px;height:30px;align-content:center'></td></tr>";
    strHTML += "</table>";
    jQuery("#surveydetails").html(strHTML);
}

function validateResponseOnQ1()
{
    assessmentTemplateId = tid;

    var strHTML = "";

    if ($('input[id=q1R_Yes]').is(':checked'))
    {
        strHTML += "<table style='width: 863px'><tr><td></td><td></td></tr>";

        strHTML += "<tr><td></br></td></tr>";
        strHTML += "<tr><td class='label'>Service Request Number:</td></tr>";
        strHTML += "<tr><td class='text'>" + srNumber + "</td></tr>";
        strHTML += "<tr><td></br></td></tr>";

        strHTML += "<tr><td class='label'>Description:</td></tr>";
        strHTML += "<tr><td class='text'> " + srSubject.replace(/%20/g, " ") + "</td></tr>";

        strHTML += "<tr><td></br></td></tr>";
        strHTML += "<tr><td><input type='hidden' id=templateId value='" + assessmentTemplateId + "' display='none'></td></tr>";
        strHTML += "<tr><td class='label'>Was your issue resolved?</td></tr>";
        strHTML += "<tr><td class='text'><input type='radio' id='q1R_Yes' name='q1ID' onchange='javascript:validateResponseOnQ1();' checked value='Yes'>Yes <input type='radio' id='q1R_No' name='q1ID' onchange='javascript:validateResponseOnQ1IfNo();' value='No'>No</td></tr>";

        strHTML += "<tr><td></br></td></tr>";
        strHTML += "<tr><td align='center' colspan='4'><input type='button' id='btnSubmit' onclick='javascript:saveResponsesOnMyPartnerPlusSurvey()' value ='Submit' class='button'><input type='button' id='btnCancel' onclick='javascript:clearFields()' value ='Cancel' class='button' ></td></tr>";
        strHTML += "<tr><td align='center'><img alt='loading' id='regLoadingImg' src='images/ajaxLoading.gif' style='display:none; position:relative;width:30px;height:30px;align-content:center'></td></tr>";
        strHTML += "</table>";
        jQuery("#surveydetails").html(strHTML);
        $('#q1ID_Yes').attr('checked', 'checked');
    }
}

function validateResponseOnQ1IfNo()
{
    assessmentTemplateId = tid;

    var strHTML = "";

    if ($('input[id=q1R_No]').is(':checked')) {
        strHTML += "<table style='width: 863px'><tr><td></td><td></td></tr>";

        strHTML += "<tr><td></br></td></tr>";
        strHTML += "<tr><td class='label'>Service Request Number:</td></tr>";
        strHTML += "<tr><td class='text'>" + srNumber + "</td></tr>";
        strHTML += "<tr><td></br></td></tr>";

        strHTML += "<tr><td class='label'>Description:</td></tr>";
        strHTML += "<tr><td class='text'> " + srSubject.replace(/%20/g, " ") + "</td></tr>";

        strHTML += "<tr><td></br></td></tr>";
        strHTML += "<tr><td><input type='hidden' id=templateId value='" + assessmentTemplateId + "' display='none'></td></tr>";
        strHTML += "<tr><td class='label'>Was your issue resolved?</td></tr>";
        strHTML += "<tr><td class='text'><input type='radio' id='q1R_Yes' name='q1ID' onchange='javascript:validateResponseOnQ1();' value='Yes'>Yes <input type='radio' id='q1R_No' name='q1ID' onchange='javascript:validateResponseOnQ1IfNo();' value='No' checked>No</td></tr>";

        strHTML += "<tr><td></br></td></tr>";
        strHTML += "<tr><td class='label'>What could we have done better? Put your answers in comments area below:</td></tr>";
        strHTML += "<tr><td><textarea id='q2R_comments' name='q2ID' size='240' cols='160' rows='4' style='width:830px; height:50px;'></textarea></td></tr>";

        strHTML += "<tr><td></br></td></tr>";
        strHTML += "<tr><td class='label'>Would you like us to contact you?</td></tr>";
        strHTML += "<tr><td class='text'><input type='radio' id='q3R_Yes' name='q3ID' value='Yes'>Yes <input type='radio' id='q3R_No' name='q3ID' value='No'>No</td></tr>";

        strHTML += "<tr><td></br></td></tr>";
        strHTML += "<tr><td align='center' colspan='4'><input type='button' id='btnSubmit' onclick='javascript:saveResponsesOnMyPartnerPlusSurvey()' value ='Submit' class='button'><input type='button' id='btnCancel' onclick='javascript:clearFields()' value ='Cancel' class='button' ></td></tr>";
        strHTML += "<tr><td align='center'><img alt='loading' id='regLoadingImg' src='images/ajaxLoading.gif' style='display:none; position:relative;width:30px;height:30px;align-content:center'></td></tr>";
        strHTML += "</table>";

        jQuery("#surveydetails").html(strHTML);
        $('#q1ID_No').attr('checked', 'checked');
    }
}


function saveResponsesOnMyPartnerPlusSurvey()
{
    var pQ1Response = "";
    var pQ2Response = "";
    var pQ3Response = "";
    var pTemplateId = ""; 

    pTemplateId = document.getElementById("templateId").value;

    if (document.getElementById("q1R_Yes").checked == true)
    {
        //show regLoadingImg
        $("#regLoadingImg").show();
        pQ1Response = q1ID + "|" + "Yes" + "|" + "|";
        pQ2Response = q2ID + "|" + "" + "|";
        pQ3Response = q3ID + "|" + "" + "|";

        //code to save the encoded details to XML file
        saveMyPartnerPlusSurveyResponsesToXML(srRowId, pTemplateId, srSubject, pQ1Response, pQ2Response, pQ3Response);
    }
    else if (document.getElementById("q1R_No").checked == true) {
        pQ1Response = q1ID + "|" + "No" + "|" + "|";

        pQ2Response = q2ID + "|" + "" + "|" + document.getElementById("q2R_comments").value;

        if ((document.getElementById("q3R_Yes").checked == false) && (document.getElementById("q3R_No").checked == false)) {
            alert("Would you like us to contact you? Please respond on Question# 3.");
            document.getElementById("btnSubmit").disabled = false;
            document.getElementById("btnCancel").disabled = false;
        }
        else {
            //show regLoadingImg
            $("#regLoadingImg").show();

            if (document.getElementById("q3R_Yes").checked == true) {
                pQ3Response = q3ID + "|" + "Yes" + "|" + "|";

                //code to save the encoded details to XML file
                saveMyPartnerPlusSurveyResponsesToXML(srRowId, pTemplateId, srSubject, pQ1Response, pQ2Response, pQ3Response);
            }
            else if (document.getElementById("q3R_No").checked == true) {
                pQ3Response = q3ID + "|" + "No" + "|" + "|";

                //code to save the encoded details to XML file
                saveMyPartnerPlusSurveyResponsesToXML(srRowId, pTemplateId, srSubject, pQ1Response, pQ2Response, pQ3Response);
            }
        }
    }
    else
    {
        alert("Was your issue resolved? Please respond on Question# 1.");
    }
}

function saveMyPartnerPlusSurveyResponsesToXML(srRowId, assTemId, srSubject, attribId_response_q1, attribId_response_q2, attribId_response_q3)
{
    var param = new Object();
    param.pSRRowId = srRowId;
    param.pTemplateId = assTemId;
    param.pSRSubject = srSubject;
    param.pQ1Response = attribId_response_q1;
    param.pQ2Response = attribId_response_q2;
    param.pQ3Response = attribId_response_q3;

    //redirectToThankYouPage(srRowId, assTemId, srSubject, attribId_response_q1, attribId_response_q2, attribId_response_q3);
    //20170317 - Modified as requested by User Team. Replaced "redirectToThankYouPage" with "displayThankYouMsg".
    callAjaxRequestJSON("saveMyPartnerPlusSurveyResponsesToXML", param, displayThankYouMsg, ajaxError);
}

function displayThankYouMsg()
{
    alert("Thank you for taking the survey.");

    CloseWindow();
}

function CloseWindow() {
    window.open('', '_self', '');
    window.close();
}

function redirectToThankYouPage(srRowId, assTemId, srSubject, attribId_response_q1, attribId_response_q2, attribId_response_q3)
{
    //CloseWindow();


    window.open("ThankYouPage.aspx");

    //var param = new Object();
    //param.pSRRowId = srRowId;
    //param.pTemplateId = assTemId;
    //param.pSRSubject = srSubject;
    //param.pQ1Response = attribId_response_q1;
    //param.pQ2Response = attribId_response_q2;
    //param.pQ3Response = attribId_response_q3;

    //callAjaxRequestJSON("saveMyPartnerPlusSurveyResponsesToXML", param, closePage , ajaxError);
}

function closePage()
{

}

function clearFields()
{
    if (document.getElementsByName("q1ID") != undefined)
    {
        $('#q1R_Yes').attr('checked', false);
        $('#q1R_No').attr('checked', false);
    }

    if (document.getElementById("q2R_comments") != undefined)
    {
        document.getElementById("q2R_comments").value = "";
    }

    if (document.getElementsByName("q3ID") != undefined)
    {
        $('#q3R_No').attr('checked', false);
        $('#q3R_Yes').attr('checked', false);
    }

    $("#regLoadingImg").hide();
    document.getElementById("btnSubmit").disabled = false;
    document.getElementById("btnCancel").disabled = false;
}