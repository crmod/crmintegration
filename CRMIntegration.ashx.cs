﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using System.Web.Script.Serialization;

namespace CRMIntegration
{
    /// <summary>
    /// Summary description for CRMIntegration
    /// </summary>
    public class CRMIntegration : IHttpHandler
    {

        /*
        EXPECTED PARAMETERS
        t       : method name { 0=getcontactdetails; 1 = getcontactcompletedtrainings; 2 = getcontactprojectdesignrequests, 3 = getPartnerWebContactUserProfile, 4 = uploadApprovedPartnerWebMembership, 5= uploadUserProfileUpdates}
        mid     : equivalent CRM PartnerWeb Membership Id  
        */

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            ResponseObject ro = new ResponseObject();
            OnDemandSession mSession = new OnDemandSession();

            System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            ////this will display the text: Hello Worldsss. Testing Only
            //context.Response.Write("Hello Worldsss. Testing Only");

            try
            {
                String method = context.Request["t"];

                if (method.Trim().ToString() == "0")
                {
                    String pMembershipId = context.Request["mid"];

                    if (!String.IsNullOrEmpty(pMembershipId))
                    {
                        mSession  = EnterLoginCredentials();
                        if (mSession != null)
                        {
                            ro.ResponseItem = this.getContactDetails(mSession,pMembershipId);
                        }
                    }
                    else
                    {
                        throw new Exception("Membership Id is null.");
                    }
                }

                if (method.Trim().ToString() == "1")
                {
                    String pMembershipId = context.Request["mid"];

                    if (!String.IsNullOrEmpty(pMembershipId))
                    {
                        mSession = EnterLoginCredentials();

                        if (mSession != null)
                        {
                            ro.ResponseItem = this.getContactCompletedTrainings(mSession,pMembershipId);
                        }
                        else
                        {
                            ro.ErrorMessage = "Please retry reconnecting to CRM";
                        }
                    }
                    else
                    {
                        throw new Exception("Membership Id is null.");
                    }
                }

                if (method.Trim().ToString() == "2")
                {
                    String pMembershipId = context.Request["mid"];

                    if (!String.IsNullOrEmpty(pMembershipId))
                    {
                         mSession = EnterLoginCredentials();

                         if (mSession != null)
                         {
                             ro.ResponseItem = this.getContactProjectDesignRequests(mSession,pMembershipId);
                         }
                         else
                         {
                             ro.ErrorMessage = "Please retry reconnecting to CRM";
                         }
                    }
                    else
                    {
                        throw new Exception("Membership Id is null.");
                    }
                }

                if (method.Trim().ToString() == "3")
                {
                    String pMembershipId = context.Request["mid"];

                    if (!String.IsNullOrEmpty(pMembershipId))
                    {
                        mSession = EnterLoginCredentials();

                        if (mSession != null)
                        {
                            ro.ResponseItem = this.getPartnerWebContactUserProfile(mSession, pMembershipId);
                        }
                        else
                        {
                            ro.ErrorMessage = "Please retry reconnecting to CRM";
                        }
                    }
                    else
                    {
                        throw new Exception("Membership Id is null.");
                    }
                }

                if (method.Trim().ToString() == "4")
                {
                    String pMembershipId = context.Request["mid"];
                    String pFirstName = context.Request["fn"];
                    String pLastName = context.Request["ln"];
                    String pMiddleName = context.Request["mn"];
                    String pEmailAddress = context.Request["ea"];
                    String pContactCellularPhone = context.Request["cp"];
                    String pContactCountry = context.Request["cc"];
                    String pAccountName = context.Request["an"];
                    String pAccountType = context.Request["at"];
                    String pAccountAddress = context.Request["aa"];
                    String pAccountCity = context.Request["ac"];
                    String pAccountStateProvince = context.Request["as"]; ;
                    String pAccountCountry = context.Request["aco"];
                    String pAccountPostalCode = context.Request["az"];
                    String pAccountOption = context.Request["ao"];
                    String pAccountId = context.Request["aid"];
                    String pContactId = context.Request["cid"];
                    String pIsOptIn = context.Request["notify"];

                    //20151006 - added since this was forgotten to be included on the web service
                    String pDateOfReg = context.Request["td"];

                    //20160118 - added to include P+ (User) Account Type and P+ Roles By Country
                    String pPUserType = context.Request["puser"];

                    if (!String.IsNullOrEmpty(pAccountName))
                    {
                        if (!String.IsNullOrEmpty(pAccountAddress))
                        {
                            if (!String.IsNullOrEmpty(pAccountOption))
                            {
                                if ((!String.IsNullOrEmpty(pFirstName)) || (!String.IsNullOrEmpty(pLastName)))
                                {
                                    if (!String.IsNullOrEmpty(pMembershipId))
                                    {
                                        mSession = EnterLoginCredentials();

                                        if (mSession != null)
                                        {
                                            // pAccountOption: 1 = Create New Account, 2 = Move to Another Account, 3= Retain Existing Account Association
                                            ro.ResponseItem = this.uploadApprovedPartnerWebMembership(mSession, pMembershipId, pFirstName, pLastName, pMiddleName, pEmailAddress, pContactCellularPhone, pContactCountry, pAccountName, pAccountType, pAccountAddress, pAccountCity, pAccountStateProvince, pAccountCountry, pAccountPostalCode, pAccountOption, pAccountId, pContactId, pIsOptIn,pDateOfReg,pPUserType,pAccountType);
                                        }
                                        else
                                        {
                                            ro.ErrorMessage = "Please retry reconnecting to CRM";
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Membership Id is null.");
                                    }
                                }
                                else
                                {
                                    throw new Exception("User full name is incomplete.");
                                }
                            }
                            else
                            {
                                throw new Exception("Account Association Option is null.");
                            }
                        }
                        else
                        {
                            throw new Exception("Account Location is null.");
                        }
                    }
                    else
                    {
                        throw new Exception("Account Name is null.");
                    }
                }


                //String pCPartnerwebComments, String pCIsOptOut)
                if (method.Trim().ToString() == "5")
                {
                    String pMembershipId = context.Request["mid"];
                    String pFirstName = context.Request["fn"];
                    String pLastName = context.Request["ln"];
                    String pMiddleName = context.Request["mn"];
                    String pEmailAddress = context.Request["ea"];
                    String pContactCellularPhone = context.Request["cp"];
                    String pContactCountry = context.Request["cc"];
                    String pAccountName = context.Request["an"];
                    String pAccountType = context.Request["at"];
                    String pAccountAddress = context.Request["aa"];
                    String pAccountCity = context.Request["ac"];
                    String pAccountStateProvince = context.Request["as"]; ;
                    String pAccountCountry = context.Request["aco"];
                    String pAccountPostalCode = context.Request["az"];
                    //String pAccountOption = context.Request["ao"];
                    
                    ////20160613 - RDVillamor, commented since Account level will temporarily be skipped from being updated. Account fields which were previously updated has been reconfigured on Contact level
                    //String pAccountId = context.Request["aid"];

                    String pContactId = context.Request["cid"];
                    String pContactJobTitle = context.Request["ct"];
                    String pContactYearsHVAC = context.Request["cy"];
                    String pIsCompressor = context.Request["iscompressor"];
                    String pIsCondensing = context.Request["iscondensing"];
                    String pIsControllers = context.Request["iscontroller"];
                    String pIsLineComponents = context.Request["iscomponents"];
                    String pIsEmersonBrand=context.Request["isemerson"];
                    String pOthers=context.Request["others"];
                    String pScopeOfWork = context.Request["scope"];
                    String pBusinessIndustry = context.Request["business"];
                    String pMarket = context.Request["market"];
                    String pServicesOffered = context.Request["services"];
                    String pPermissionMarketingTarget = context.Request["interests"];
                    String pPartnerwebComments = context.Request["comments"];
                    String pIsOptIn = context.Request["notify"];
                    String pReferredBy = context.Request["rb"];

                    //added 20150416 - missed to be included on the web service
                    String pAccountMainPhone = context.Request["ap"];

                    //added 20160617 - added Key Interest Others field to flow-in to CRM


                    if (!String.IsNullOrEmpty(pAccountName))
                    {
                        if (!String.IsNullOrEmpty(pAccountAddress))
                        {
                            //if (!String.IsNullOrEmpty(pAccountOption))
                            //{
                                if ((!String.IsNullOrEmpty(pFirstName)) || (!String.IsNullOrEmpty(pLastName)))
                                {
                                    if (!String.IsNullOrEmpty(pMembershipId))
                                    {
                                        mSession = EnterLoginCredentials();

                                        if (mSession != null)
                                        {
                                            ro.ResponseItem = this.updatePartnerplusContactUserProfile(mSession, pMembershipId, pAccountName, pContactId, pAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellularPhone, pAccountAddress, pAccountCity, pAccountStateProvince, pAccountCountry, pAccountPostalCode, pContactJobTitle, pContactYearsHVAC,pIsCompressor,pIsCondensing,pIsControllers,pIsLineComponents,pIsEmersonBrand,pOthers,pScopeOfWork,pBusinessIndustry,pMarket,pServicesOffered,pPermissionMarketingTarget,pPartnerwebComments,pIsOptIn,pReferredBy,pAccountMainPhone);
                                        }
                                        else
                                        {
                                            ro.ErrorMessage = "Please retry reconnecting to CRM";
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Membership Id is null.");
                                    }
                                }
                                else
                                {
                                    throw new Exception("User full name is incomplete.");
                                }
                            //}
                            //else
                            //{
                            //    throw new Exception("Account Association Option is null.");
                            //}
                        }
                        else
                        {
                            throw new Exception("Account Location is null.");
                        }
                    }
                    else
                    {
                        throw new Exception("Account Name is null.");
                    }
                }


                // method to save new lead records from event registration
                if (method.Trim().ToString() == "6")
                {
                    String pFirstName = HttpUtility.HtmlDecode(context.Request["fn"]);
                    String pLastName = HttpUtility.HtmlDecode(context.Request["ln"]);
                    String pCompanyName = HttpUtility.HtmlDecode(context.Request["cm"]);
                    String pEmailAddress = HttpUtility.HtmlDecode(context.Request["ea"]);
                    String pContactCountry = HttpUtility.HtmlDecode(context.Request["cc"]);
                    String pBusinessType = HttpUtility.HtmlDecode(context.Request["bt"]);
                    String pIndustry = HttpUtility.HtmlDecode(context.Request["ind"]);
                    String pRole = HttpUtility.HtmlDecode(context.Request["jr"]);
                    String pBusinessGroup = HttpUtility.HtmlDecode(context.Request["bg"]);
                    String pIsOptOut = HttpUtility.HtmlDecode(context.Request["io"]);
                    String pEventName = HttpUtility.HtmlDecode(context.Request["en"]);
                    String pNextStep = "For Qualification and Conversion to a Contact: " + HttpUtility.HtmlDecode(context.Request["er"]);

                    mSession = EnterLoginCredentials();

                    if (mSession != null)
                    {
                        ro.ResponseItem = this.genericSaveNewLeadFromAnEventRegistration(mSession, pFirstName, pLastName, pEmailAddress, pCompanyName, pContactCountry, pBusinessType, pIndustry, pRole, pBusinessGroup, pIsOptOut, pNextStep, pEventName);
                    }
                    else
                    {
                        ro.ErrorMessage = "Please retry reconnecting to CRM";
                    }
                }

                if (method.Trim().ToString() == "7")
                {
                    String pFirstName = HttpUtility.HtmlDecode(context.Request["fn"]);
                    String pLastName = HttpUtility.HtmlDecode(context.Request["ln"]);
                    String pCompanyName = HttpUtility.HtmlDecode(context.Request["cm"]);
                    String pEmailAddress = HttpUtility.HtmlDecode(context.Request["ea"]);
                    String pContactCountry = HttpUtility.HtmlDecode(context.Request["cc"]);
                    String pBusinessType = HttpUtility.HtmlDecode(context.Request["bt"]);
                    String pIndustry = HttpUtility.HtmlDecode(context.Request["ind"]);
                    String pRole = HttpUtility.HtmlDecode(context.Request["jr"]);
                    String pBusinessGroup = HttpUtility.HtmlDecode(context.Request["bg"]);
                    String pIsOptOut = HttpUtility.HtmlDecode(context.Request["io"]);
                    String pEventName = HttpUtility.HtmlDecode(context.Request["en"]);
                    String pNextStep = "For qualification, conversion to a contact and association on AHR 2016 Event:" + HttpUtility.HtmlDecode(context.Request["er"]);
                    String pReferredByContactId = HttpUtility.HtmlDecode(context.Request["rid"]);

                    mSession = EnterLoginCredentials();

                    if (mSession != null)
                    {
                       ro.ResponseItem = this.genericSaveNewLeadFromAHRRegistration(mSession, pFirstName, pLastName, pEmailAddress, pCompanyName, pContactCountry, pBusinessType, pIndustry, pRole, pBusinessGroup, pIsOptOut, pNextStep, pEventName, pReferredByContactId);
                    }
                    else
                    {
                        ro.ErrorMessage = "Please retry reconnecting to CRM";
                    }
 
                }

                // method to save new lead records from registration on page
                if (method.Trim().ToString() == "8")
                {
                    String pFirstName = HttpUtility.HtmlDecode(context.Request["fn"]);
                    String pLastName = HttpUtility.HtmlDecode(context.Request["ln"]);
                    String pCompanyName = HttpUtility.HtmlDecode(context.Request["cm"]);
                    String pEmailAddress = HttpUtility.HtmlDecode(context.Request["ea"]);
                    String pContactCountry = HttpUtility.HtmlDecode(context.Request["cc"]);
                    String pBusinessType = HttpUtility.HtmlDecode(context.Request["bt"]);
                    String pIndustry = HttpUtility.HtmlDecode(context.Request["ind"]);
                    String pRole = HttpUtility.HtmlDecode(context.Request["jr"]);
                    String pBusinessGroup = HttpUtility.HtmlDecode(context.Request["bg"]);
                    String pIsOptOut = HttpUtility.HtmlDecode(context.Request["io"]);
                    String pLeadSourceName = HttpUtility.HtmlDecode(context.Request["sn"]);
                    String pNextStep = "For processing of request: " + HttpUtility.HtmlDecode(context.Request["er"]);

                    mSession = EnterLoginCredentials();

                    if (mSession != null)
                    {
                        ro.ResponseItem = this.genericSaveNewLeadFromPageRegistration(mSession, pFirstName, pLastName, pEmailAddress, pCompanyName, pContactCountry, pBusinessType, pIndustry, pRole, pBusinessGroup, pIsOptOut, pNextStep, pLeadSourceName);
                    }
                    else
                    {
                        ro.ErrorMessage = "Please retry reconnecting to CRM";
                    }
                }

                // method to save new lead records from Connect to an Expert page of ETS
                if (method.Trim().ToString() == "9")
                {
                    String pFirstName = HttpUtility.HtmlDecode(context.Request["fn"]);
                    String pLastName = HttpUtility.HtmlDecode(context.Request["ln"]);
                    String pCompanyName = HttpUtility.HtmlDecode(context.Request["cm"]);
                    String pEmailAddress = HttpUtility.HtmlDecode(context.Request["ea"]);
                    String pContactCountry = HttpUtility.HtmlDecode(context.Request["cc"]);
                    String pBusinessType = HttpUtility.HtmlDecode(context.Request["bt"]);
                    String pRole = HttpUtility.HtmlDecode(context.Request["jr"]);
                    String pBusinessGroup = HttpUtility.HtmlDecode(context.Request["bg"]);
                    String pIsOptOut = HttpUtility.HtmlDecode(context.Request["io"]);
                    String pLeadSourceName = HttpUtility.HtmlDecode(context.Request["sn"]);
                    String pComments = HttpUtility.HtmlDecode(context.Request["com"]);
                    String pPhoneNum = HttpUtility.HtmlDecode(context.Request["pn"]);

                    //er = Communicate with user to get more information
                    String pNextStep = "For processing of request: Inquiry from ETS web page visitor";

                    mSession = EnterLoginCredentials();

                    if (mSession != null)
                    {
                        ro.ResponseItem = this.genericSaveNewLeadFromETSConnectToExpertPage(mSession, pFirstName, pLastName, pEmailAddress, pCompanyName, pContactCountry, pBusinessType, pRole, pBusinessGroup, pIsOptOut, pNextStep, pLeadSourceName, pPhoneNum, pComments);
                    }
                    else
                    {
                        ro.ErrorMessage = "Please retry reconnecting to CRM";
                    }
                }

                /*Created on/by: 20160321/RDVillamor
                 * copy of t=4, recreated for the use of the same logic of uploading of approval of Partner+ MEA
                 * 
                */
                if (method.Trim().ToString() == "10")
                {
                    String pMembershipId = context.Request["mid"];
                    String pFirstName = context.Request["fn"];
                    String pLastName = context.Request["ln"];
                    String pMiddleName = context.Request["mn"];
                    String pEmailAddress = context.Request["ea"];
                    String pContactCellularPhone = context.Request["cp"];
                    String pContactCountry = context.Request["cc"];
                    String pAccountName = context.Request["an"];
                    String pAccountType = context.Request["at"];
                    String pAccountAddress = context.Request["aa"];
                    String pAccountCity = context.Request["ac"];
                    String pAccountStateProvince = context.Request["as"]; ;
                    String pAccountCountry = context.Request["aco"];
                    String pAccountPostalCode = context.Request["az"];
                    String pAccountOption = context.Request["ao"];
                    String pAccountId = context.Request["aid"];
                    String pContactId = context.Request["cid"];

                    String pIsOptIn = context.Request["notify"];

                    //20151006 - added since this was forgotten to be included on the web service
                    String pDateOfReg = context.Request["td"];

                    //20160118 - added to include P+ (User) Account Type and P+ Roles By Country
                    String pPUserType = context.Request["puser"];

                    if (!String.IsNullOrEmpty(pAccountName))
                    {
                        if (!String.IsNullOrEmpty(pAccountAddress))
                        {
                            if (!String.IsNullOrEmpty(pAccountOption))
                            {
                                if ((!String.IsNullOrEmpty(pFirstName)) || (!String.IsNullOrEmpty(pLastName)))
                                {
                                    if (!String.IsNullOrEmpty(pMembershipId))
                                    {
                                        mSession = EnterLoginCredentials();

                                        if (mSession != null)
                                        {
                                            // pAccountOption: 1 = Create New Account, 2 = Move to Another Account, 3= Retain Existing Account Association
                                            ro.ResponseItem = this.uploadApprovedPartnerWebMembership_MEA(mSession, pMembershipId, pFirstName, pLastName, pMiddleName, pEmailAddress, pContactCellularPhone, pContactCountry, pAccountName, pAccountType, pAccountAddress, pAccountCity, pAccountStateProvince, pAccountCountry, pAccountPostalCode, pAccountOption, pAccountId, pContactId, pIsOptIn, pDateOfReg, pPUserType, pAccountType);

                                            mSession.Destroy();
                                        }
                                        else
                                        {
                                            ro.ErrorMessage = "Please retry reconnecting to CRM";
                                        }
                                    }
                                    else
                                    {
                                        throw new Exception("Membership Id is null.");
                                    }
                                }
                                else
                                {
                                    throw new Exception("User full name is incomplete.");
                                }
                            }
                            else
                            {
                                throw new Exception("Account Association Option is null.");
                            }
                        }
                        else
                        {
                            throw new Exception("Account Location is null.");
                        }
                    }
                    else
                    {
                        throw new Exception("Account Name is null.");
                    }
                }

                //20160318 - Created by RDVillamor for POC purposes of Eloqua-CRM integration for Events-invitee
                if (method.Trim().ToString() == "elo")
                {
                    String contactRowId = HttpUtility.HtmlDecode(context.Request["cid"]);
                    String eventRowId = HttpUtility.HtmlDecode(context.Request["eid"]);
                    String eventInviteeStatus = HttpUtility.HtmlDecode(context.Request["istat"]);
                    String eventInviteeSource = HttpUtility.HtmlDecode(context.Request["isource"]);
                    String email = HttpUtility.HtmlDecode(context.Request["email"]);

                    mSession = EnterLoginCredentials();

                    if (mSession != null)
                    {
                        ro.ResponseItem = this.postInviteeStatus(mSession,contactRowId, eventRowId, eventInviteeStatus, eventInviteeSource, email);
                    }
                    else
                    {
                        ro.ErrorMessage = "Please retry reconnecting to CRM";
                    }


                }

                /*
                 * RDVillamor / 20160512
                 * Created for the Phase 2 implementation of P+ Member Registration
                 */
                if (method.Trim().ToString() == "11")
                {
                    String pMembershipId = context.Request["mid"];
                    String pFirstName = context.Request["fn"];
                    String pLastName = context.Request["ln"];
                    String pEmail = context.Request["ea"];
                    String pMobileNum = context.Request["cp"];
                    String pBusinessName = context.Request["bn"];
                    String pAddress = context.Request["ca"];
                    String pContactCountry = context.Request["cc"]; 
                    String pIsOptIn = context.Request["notify"];

                    if (!String.IsNullOrEmpty(pBusinessName))
                    {
                        if (!String.IsNullOrEmpty(pAddress))
                        {
                            if ((!String.IsNullOrEmpty(pFirstName)) || (!String.IsNullOrEmpty(pLastName)))
                            {
                                if (!String.IsNullOrEmpty(pMembershipId))
                                {
                                    mSession = EnterLoginCredentials();

                                    if (mSession != null)
                                    {
                                        ro.ResponseItem = this.processPartnerplusSignUpDetails(mSession, pFirstName, pLastName, pEmail, pMobileNum, pAddress, pContactCountry, pBusinessName, pMembershipId, pIsOptIn);

                                        mSession.Destroy();
                                    }
                                    else
                                    {
                                        ro.ErrorMessage = "Please retry reconnecting to CRM";
                                    }
                                }
                                else
                                {
                                    throw new Exception("Membership Id is null.");
                                }
                            }
                            else
                            {
                                throw new Exception("User full name is incomplete.");
                            }
                        }
                        else
                        {
                            throw new Exception("Address is null.");
                        }
                    }
                    else
                    {
                        throw new Exception("Business Name is null.");
                    }
                }

                /* 20160512 - RDVillamor
                 * This method is developed to flow-in to CRM the processed approvals from the enhanced User Management page
                 */
                if (method.Trim().ToString() == "12")
                {
                    String pMiddleName = context.Request["mn"];
                    String pLastName = context.Request["ln"];
                    String pEmail = context.Request["ea"];
                    String pMobileNum = context.Request["cp"];
                    String pBusinessName = context.Request["bn"];
                    String pAddress = context.Request["ca"];
                    String pCity = context.Request["ci"];
                    String pStateProvince = context.Request["sp"];
                    String pCountry = context.Request["cc"];
                    String pRoles = context.Request["ro"];
                    String pBusinessType = context.Request["bt"];
                    String pPartnerplusUserType = context.Request["ut"];
                    String pAccountId = context.Request["aid"];
                    String pContactId = context.Request["cid"];

                    ////20160617, added by RDVillamor
                    //String pFirstName = context.Request["fn"];

                    if (!String.IsNullOrEmpty(pBusinessName))
                    {
                        if (!String.IsNullOrEmpty(pAddress))
                        {
                            mSession = EnterLoginCredentials();

                            if (mSession != null)
                            {
                                ro.ResponseItem = this.processPartnerplusUserManagementApproval_Enhanced(mSession, pBusinessName, pAccountId, pContactId, pMiddleName, pMobileNum, pRoles, pBusinessType, pAddress, pCity, pStateProvince, pCountry, pPartnerplusUserType);

                                mSession.Destroy();
                            }
                        }
                        else
                        {
                            throw new Exception("Address is null");
                        }
                    }
                    else
                    {
                        throw new Exception("Business Name is null.");
                    }
                }

                /* 20160513 / RDVillamor
                 * This function extracts the list of accounts associated to the Contact
                 */
                //public ArrayList getCRMContactAccounts(OnDemandSession pSession,String pContactId)
                if (method.Trim().ToString() == "13")
                {
                    String pContactId = context.Request["cid"];

                    if (!String.IsNullOrEmpty(pContactId))
                    {
                        mSession = EnterLoginCredentials();
                        
                        if (mSession != null)
                        {
                            ro.ResponseItem = this.getCRMContactAccounts(mSession, pContactId);

                            mSession.Destroy();
                        }
                    }
                    else
                    {
                        throw new Exception("CRM Contact Row Id is null.");
                    }
                }


                /**/
                if (method.Trim().ToString() == "14")
                {
                    String pBusinessName = context.Request["bn"];
                    String pLocation = context.Request["bc"];

                    if (!String.IsNullOrEmpty(pBusinessName))
                    {
                        mSession = EnterLoginCredentials();

                        if (mSession != null)
                        {
                            ro.ResponseItem = this.getCRMAccountsByBusinessName(mSession, pBusinessName, pLocation);

                            mSession.Destroy();
                        }
                    }
                    else
                    {
                        throw new Exception("Business Name is null.");
                    }
                }

                /*
                 * RDVillamor / 20160708 - RDVillamor
                 * Created for the Phase 2 implementation of P+ Member Registration of MEA
                 */
                if (method.Trim().ToString() == "15")
                {
                    String pMembershipId = context.Request["mid"];
                    String pFirstName = context.Request["fn"];
                    String pLastName = context.Request["ln"];
                    String pEmail = context.Request["ea"];
                    String pMobileNum = context.Request["cp"];
                    String pBusinessName = context.Request["bn"];
                    String pAddress = context.Request["ca"];
                    String pContactCountry = context.Request["cc"];
                    String pIsOptIn = context.Request["notify"];

                    if (!String.IsNullOrEmpty(pBusinessName))
                    {
                        if (!String.IsNullOrEmpty(pAddress))
                        {
                            if ((!String.IsNullOrEmpty(pFirstName)) || (!String.IsNullOrEmpty(pLastName)))
                            {
                                if (!String.IsNullOrEmpty(pMembershipId))
                                {
                                    mSession = EnterLoginCredentials();

                                    if (mSession != null)
                                    {
                                        ro.ResponseItem = this.processPartnerplusSignUpDetails_MEA(mSession, pFirstName, pLastName, pEmail, pMobileNum, pAddress, pContactCountry, pBusinessName, pMembershipId, pIsOptIn);

                                        mSession.Destroy();
                                    }
                                    else
                                    {
                                        ro.ErrorMessage = "Please retry reconnecting to CRM";
                                    }
                                }
                                else
                                {
                                    throw new Exception("Membership Id is null.");
                                }
                            }
                            else
                            {
                                throw new Exception("User full name is incomplete.");
                            }
                        }
                        else
                        {
                            throw new Exception("Address is null.");
                        }
                    }
                    else
                    {
                        throw new Exception("Business Name is null.");
                    }
                }

                /* 20160708 - RDVillamor
                 * This method is developed to flow-in to CRM the processed approvals from the enhanced User Management page of Partner+ MEA
                 */
                if (method.Trim().ToString() == "16")
                {
                    String pMiddleName = context.Request["mn"];
                    String pLastName = context.Request["ln"];
                    String pEmail = context.Request["ea"];
                    String pMobileNum = context.Request["cp"];
                    String pBusinessName = context.Request["bn"];
                    String pAddress = context.Request["ca"];
                    String pCity = context.Request["ci"];
                    String pStateProvince = context.Request["sp"];
                    String pCountry = context.Request["cc"];
                    String pRoles = context.Request["ro"];
                    String pBusinessType = context.Request["bt"];
                    String pPartnerplusUserType = context.Request["ut"];
                    String pAccountId = context.Request["aid"];
                    String pContactId = context.Request["cid"];

                    ////20160617, added by RDVillamor
                    //String pFirstName = context.Request["fn"];

                    if (!String.IsNullOrEmpty(pBusinessName))
                    {
                        if (!String.IsNullOrEmpty(pAddress))
                        {
                            mSession = EnterLoginCredentials();

                            if (mSession != null)
                            {
                                ro.ResponseItem = this.processPartnerplusUserManagementApproval_Enhanced_MEA(mSession, pBusinessName, pAccountId, pContactId, pMiddleName, pMobileNum, pRoles, pBusinessType, pAddress, pCity, pStateProvince, pCountry, pPartnerplusUserType);

                                mSession.Destroy();
                            }
                        }
                        else
                        {
                            throw new Exception("Address is null");
                        }
                    }
                    else
                    {
                        throw new Exception("Business Name is null.");
                    }
                }

                /*20160719 - RDVillamor
                 * This is to return the values for the new profile builder feature of Partner+
                 * 
                 */
                if (method.Trim().ToString() == "17")
                {
                    String pMembershipId = context.Request["mid"];

                    mSession = EnterLoginCredentials();

                    if (mSession != null)
                    {
                        ro.ResponseItem = this.getPartnerWebContactUserProfile_Enhanced_for_Profile_Builder(mSession, pMembershipId);

                        mSession.Destroy();
                    }
                }


                /* 20160721 - RDVillamor
                 * This is to safe the data from the new My Profile page
                 * 
                 */
                if (method.Trim().ToString() == "18")
                {
                    String pMembershipId = context.Request["mid"];
                    String pContactId = context.Request["cid"];
                    String pAccountId = context.Request["aid"];
                    String pAddress = context.Request["cad"];
                    String pCity = context.Request["city"];
                    String pJobTitle = context.Request["jt"];
                    String pJobFocus = context.Request["jf"];
                    String pContactType = context.Request["bt"];
                    String pCJobSites = context.Request["js"];
                    //String pACustomerIndustries = context.Request["aci"];
                    String pCMajorGroup = context.Request["cmg"];
                    String pCJobSpecific = context.Request["cjs"];
                    String pAServicesOffered = context.Request["as"];
                    String pCYears = context.Request["cyx"];
                    String pCLang = context.Request["cl"];
                    String pCMobileType = context.Request["cmt"];
                    String pCHasMobileData = context.Request["chd"];
                    String pCKeyAresToImprove = context.Request["cka"];

                    mSession = EnterLoginCredentials();

                    if (mSession != null)
                    {
                        ro.ResponseItem = this.updatePartnerplusContactUserProfile_Enhanced(mSession, pMembershipId, pContactId, pAccountId, pContactType, pAddress, pCity, pJobTitle, pCYears, pAServicesOffered, pCKeyAresToImprove, pCJobSites, pJobFocus, pCLang, pCMobileType, pCHasMobileData, pCMajorGroup, pCJobSpecific);

                        mSession.Destroy();
                    }
                }

                /*
                 * RDVillamor / 20160512
                 * Created for the Phase 3 implementation of P+ Member Registration, flexible to be used by both MEA and Asia
                 */
                if (method.Trim().ToString() == "19")
                {
                    String pMembershipId = context.Request["mid"];
                    String pFirstName = context.Request["fn"];
                    String pLastName = context.Request["ln"];
                    String pEmail = context.Request["ea"];
                    String pMobileNum = context.Request["cp"];
                    String pBusinessName = context.Request["bn"];
                    String pAddress = context.Request["ca"];
                    String pContactCountry = context.Request["cc"];
                    String pIsOptIn = context.Request["notify"];
                    String pGroup = context.Request["grp"];

                    if (!String.IsNullOrEmpty(pBusinessName))
                    {
                        if (!String.IsNullOrEmpty(pAddress))
                        {
                            if ((!String.IsNullOrEmpty(pFirstName)) || (!String.IsNullOrEmpty(pLastName)))
                            {
                                if (!String.IsNullOrEmpty(pMembershipId))
                                {
                                    mSession = EnterLoginCredentials();

                                    if (mSession != null)
                                    {
                                        ro.ResponseItem = this.processPartnerplusSignUpDetails_Phase3_Enhanced(mSession, pFirstName, pLastName, pEmail, pMobileNum, pAddress, pContactCountry, pBusinessName, pMembershipId, pIsOptIn,pGroup);

                                        mSession.Destroy();
                                    }
                                    else
                                    {
                                        ro.ErrorMessage = "Please retry reconnecting to CRM";
                                    }
                                }
                                else
                                {
                                    throw new Exception("Membership Id is null.");
                                }
                            }
                            else
                            {
                                throw new Exception("User full name is incomplete.");
                            }
                        }
                        else
                        {
                            throw new Exception("Address is null.");
                        }
                    }
                    else
                    {
                        throw new Exception("Business Name is null.");
                    }
                }

            }
            catch (Exception ex)
            {
                ro.ErrorMessage = ex.Message;
            }

            String sJSON = oSerializer.Serialize(ro);
            context.Response.Write(sJSON);

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public static String getPartnerWebMemberCRMId(OnDemandSession pSession, String pMembershipId)
        {
            String sContactId = "";
            Int64 intCtrContactResults = 0;

            bool hasRecords = true;

            WSOD_Contact_v2.Contact srvcContact = new WSOD_Contact_v2.Contact();
            WSOD_Contact_v2.ContactQueryPage_Input inputContact = new WSOD_Contact_v2.ContactQueryPage_Input();
            WSOD_Contact_v2.ContactQueryPage_Output outputContact = new WSOD_Contact_v2.ContactQueryPage_Output();

            try
            {
                while (hasRecords == true)
                {
                    inputContact.ListOfContact = new WSOD_Contact_v2.ListOfContactQuery();
                    inputContact.ListOfContact.Contact = new WSOD_Contact_v2.ContactQuery();

                    inputContact.ListOfContact.pagesize = "1";
                    inputContact.ListOfContact.startrownum = "0";

                    inputContact.ListOfContact.Contact.ContactFirstName = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ContactLastName = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.Id = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ContactFullName = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ContactEmail = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.Owner = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.OwnerId = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.IndexedShortText1 = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.searchspec = "[IndexedShortText1]='" + pMembershipId + "'";

                    srvcContact.Url = pSession.GetURL();
                    srvcContact.CookieContainer = pSession.GetCookieContainer();
                    srvcContact.Timeout = 120000;

                    outputContact = srvcContact.ContactQueryPage(inputContact);

                    if (outputContact.ListOfContact.Contact != null)
                    {
                        intCtrContactResults = outputContact.ListOfContact.Contact.Length;

                        if (intCtrContactResults == 1)
                        {
                            sContactId = outputContact.ListOfContact.Contact[0].Id;
                        }
                    }

                    hasRecords = false;
                }

            }
            catch (Exception ex)
            {
                sContactId = null;
            }

            return sContactId;
        }

        public ArrayList getContactCompletedTrainings(OnDemandSession pSession, String pMembershipId)
        {
            ArrayList arrContactCompletedTrainings = new ArrayList();
            bool hasRecords = true;
            Int64 intCtrTrainingsPageCtr = 0;
            Int64 intEventOutput = 0;
            String pContactId = "";

            WSOD_Event_v1.MedEd srvcEvent = new WSOD_Event_v1.MedEd();
            WSOD_Event_v1.MedEdWS_MedEdQueryPage_Input inputEvent = new WSOD_Event_v1.MedEdWS_MedEdQueryPage_Input();
            WSOD_Event_v1.MedEdWS_MedEdQueryPage_Output outputEvent = new WSOD_Event_v1.MedEdWS_MedEdQueryPage_Output();
           
            try
            {
                

                pContactId = getPartnerWebMemberCRMId(pSession, pMembershipId);

                if (pContactId != "")
                {
                    while (hasRecords == true)
                    {
                        inputEvent.PageSize = "100";
                        inputEvent.StartRowNum = ((intCtrTrainingsPageCtr)).ToString();
                        inputEvent.UseChildAnd = "true";

                        inputEvent.ListOfMedEd = new WSOD_Event_v1.MedEd1[1];
                        inputEvent.ListOfMedEd[0] = new WSOD_Event_v1.MedEd1();
                        inputEvent.ListOfMedEd[0].ListOfInvitee = new WSOD_Event_v1.Invitee[1];
                        inputEvent.ListOfMedEd[0].ListOfInvitee[0] = new WSOD_Event_v1.Invitee();

                        inputEvent.ListOfMedEd[0].MedEdId = "";
                        inputEvent.ListOfMedEd[0].Name = "";
                        inputEvent.ListOfMedEd[0].IndexedLongText0 = "";
                        inputEvent.ListOfMedEd[0].Type = "='Training'";
                        //inputEvent.ListOfMedEd[0].StartDate = "(&gt;='" + DateTime.Today.AddYears(-1).ToString("MM/dd/yyyy HH:mm:ss") +"') AND (&lt;='" + DateTime.Now.AddDays(+1).ToString("MM/dd/yyyy HH:mm:ss") + "')";
                        inputEvent.ListOfMedEd[0].StartDate = "(>='" + DateTime.Today.AddYears(-1).ToString("MM/dd/yyyy HH:mm:ss") + "') AND (<='" + DateTime.Now.AddDays(+1).ToString("MM/dd/yyyy HH:mm:ss") + "')";
                        inputEvent.ListOfMedEd[0].EndDate = "";
                        inputEvent.ListOfMedEd[0].plBusiness_Group = "";
                        inputEvent.ListOfMedEd[0].Objective = "";
                        inputEvent.ListOfMedEd[0].Owner = "";
                        inputEvent.ListOfMedEd[0].Status = "(='Active') OR (='Completed')";

                        inputEvent.ListOfMedEd[0].ListOfInvitee[0].ContactId = "='" + pContactId + "'";
                        inputEvent.ListOfMedEd[0].ListOfInvitee[0].Status = "='Attended'";
                        inputEvent.ListOfMedEd[0].ListOfInvitee[0].ContactFirstName = "";
                        inputEvent.ListOfMedEd[0].ListOfInvitee[0].ContactLastName = "";
                        inputEvent.ListOfMedEd[0].ListOfInvitee[0].ContactFullName = "";

                        srvcEvent.Url = pSession.GetURL();
                        srvcEvent.CookieContainer = pSession.GetCookieContainer();
                        srvcEvent.Timeout = 120000;

                        outputEvent = srvcEvent.MedEdQueryPage(inputEvent);

                        intEventOutput = outputEvent.ListOfMedEd.Length;

                        if (intEventOutput > 0)
                        {
                            for (Int64 intIdxEvent = 0; intIdxEvent < intEventOutput; intIdxEvent++)
                            {
                                EventV1 csEvent = new EventV1();

                                csEvent.EventName = outputEvent.ListOfMedEd[intIdxEvent].Name;
                                csEvent.EventAlternateName = outputEvent.ListOfMedEd[intIdxEvent].IndexedLongText0;
                                csEvent.EventStartDate = outputEvent.ListOfMedEd[intIdxEvent].StartDate.ToString();
                                csEvent.EventEndDate = outputEvent.ListOfMedEd[intIdxEvent].EndDate.ToString();
                                csEvent.EventType = outputEvent.ListOfMedEd[intIdxEvent].Type;

                                arrContactCompletedTrainings.Add(csEvent);
                            }

                            if (intEventOutput >= 100)
                            {
                                hasRecords = true;
                                intCtrTrainingsPageCtr = intCtrTrainingsPageCtr + 100;
                            }
                            else
                            {
                                hasRecords = false;
                            }

                        }
                        else
                        {
                            arrContactCompletedTrainings = null;
                            hasRecords = false;
                        }

                    }
                }
                else
                {
                    arrContactCompletedTrainings = null;
                }

            }
            catch(Exception ex)
            {
                arrContactCompletedTrainings = null;
            }

            return arrContactCompletedTrainings;
        }

        public ArrayList getContactCompletedTrainings_2(OnDemandSession pSession,String pMembershipId)
        {
            ArrayList arrContactCompletedTrainings = new ArrayList();
            bool hasRecords = true;
            Int64 intCtrTrainingsPageCtr = 0;
            Int64 intEventOutput = 0;
            String pContactId = ""; 

            WSOD_Event_v2.MedEd srvcEvent = new WSOD_Event_v2.MedEd();
            WSOD_Event_v2.MedEdQueryPage_Input inputEvent = new WSOD_Event_v2.MedEdQueryPage_Input();
            WSOD_Event_v2.MedEdQueryPage_Output outputEvent = new WSOD_Event_v2.MedEdQueryPage_Output();

            try
            {
                EventV1 csEvent = new EventV1();

                pContactId = getPartnerWebMemberCRMId(pSession,pMembershipId);

                while (hasRecords == true)
                {
                    //Event: Event Name, Event Id, Type=Training, Invitee Status = Attended, StartDate/EndDate within current FY Year

                    inputEvent.ListOfMedEd = new WSOD_Event_v2.ListOfMedEdQuery();
                    inputEvent.ListOfMedEd.MedEd = new WSOD_Event_v2.MedEdQuery();
                    inputEvent.ListOfMedEd.MedEd.ListOfInvitee = new WSOD_Event_v2.ListOfInviteeQuery();
                    inputEvent.ListOfMedEd.MedEd.ListOfInvitee.Invitee = new WSOD_Event_v2.InviteeQuery();

                    inputEvent.ListOfMedEd.pagesize = "100";
                    inputEvent.ListOfMedEd.startrownum = ((intCtrTrainingsPageCtr)).ToString();

                    inputEvent.ListOfMedEd.pagesize = "100";
                    inputEvent.ListOfMedEd.startrownum = "0";

                    inputEvent.ListOfMedEd.MedEd.Id = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.Name = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.IndexedLongText0 = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.Type = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.StartDate = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.EndDate = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.plBusiness_Group = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.Objective = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.Owner = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.Status = new WSOD_Event_v2.queryType();
                    
                    inputEvent.ListOfMedEd.MedEd.ListOfInvitee.Invitee.ContactId = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.ListOfInvitee.Invitee.Status = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.ListOfInvitee.Invitee.ContactFirstName = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.ListOfInvitee.Invitee.ContactLastName = new WSOD_Event_v2.queryType();
                    inputEvent.ListOfMedEd.MedEd.ListOfInvitee.Invitee.ContactFullName = new WSOD_Event_v2.queryType();

                    inputEvent.ListOfMedEd.MedEd.searchspec = "[Status]<>'Cancelled' AND [Type]='Training' AND ([StartDate]>='" + DateTime.Today.AddYears(-1).ToString("MM/dd/yyyy HH:mm:ss") + "' AND [StartDate]<='" + DateTime.Now.AddDays(+1).ToString("MM/dd/yyyy HH:mm:ss") + "')";
                    inputEvent.ListOfMedEd.MedEd.StartDate.sortorder = "DESC";

                    inputEvent.ListOfMedEd.MedEd.ListOfInvitee.Invitee.searchspec = "[Status]= 'Attended' AND [ContactId]='" + pContactId +"'";

                    srvcEvent.Url = pSession.GetURL();
                    srvcEvent.CookieContainer = pSession.GetCookieContainer();
                    srvcEvent.Timeout = 120000;

                    outputEvent = srvcEvent.MedEdQueryPage(inputEvent);

                    if (outputEvent.ListOfMedEd.MedEd != null)
                    {
                        intEventOutput = outputEvent.ListOfMedEd.MedEd.Length;

                        if (intEventOutput > 0)
                        {
                            for (Int64 intIdxEvent = 0; intIdxEvent < intEventOutput; intIdxEvent++)
                            {
                                csEvent.EventName = outputEvent.ListOfMedEd.MedEd[intIdxEvent].Name;
                                csEvent.EventAlternateName = outputEvent.ListOfMedEd.MedEd[intIdxEvent].IndexedLongText0;
                                csEvent.EventStartDate = outputEvent.ListOfMedEd.MedEd[intIdxEvent].StartDate.ToString();
                                csEvent.EventEndDate = outputEvent.ListOfMedEd.MedEd[intIdxEvent].EndDate.ToString();
                                csEvent.EventType = outputEvent.ListOfMedEd.MedEd[intIdxEvent].Type;

                                arrContactCompletedTrainings.Add(csEvent);
                            }

                            if (intEventOutput >= 100)
                            {
                                hasRecords = true;
                                intCtrTrainingsPageCtr = intCtrTrainingsPageCtr + 100;
                            }
                            else
                            {
                                hasRecords = false;
                            }
                        }
                    }
                    else
                    {
                        arrContactCompletedTrainings = null;
                    }
                }
            }
            catch (Exception ex)
            {
                arrContactCompletedTrainings = null;
            }


            return arrContactCompletedTrainings;
        }

        public ArrayList getContactProjectDesignRequests(OnDemandSession pSession, String pMembershipId)
        {
            ArrayList arrContactProjectDesignRequestsDetails = new ArrayList();

            Int64 intServiceRequestPageCount = 0;
            Int64 intCtrSRResults = 0;
            String pContactId="";
            bool hasRecords = true;

            WSOD_ServiceRequest_v1.ServiceRequest srvcService = new WSOD_ServiceRequest_v1.ServiceRequest();
            WSOD_ServiceRequest_v1.ServiceRequestWS_ServiceRequestQueryPage_Input inputService = new WSOD_ServiceRequest_v1.ServiceRequestWS_ServiceRequestQueryPage_Input();
            WSOD_ServiceRequest_v1.ServiceRequestWS_ServiceRequestQueryPage_Output outputService = new WSOD_ServiceRequest_v1.ServiceRequestWS_ServiceRequestQueryPage_Output();

            try
            {
                

                pContactId = getPartnerWebMemberCRMId(pSession, pMembershipId);

                if (pContactId != "")
                {
                    while (hasRecords == true)
                    {
                        inputService.PageSize = "100";
                        inputService.StartRowNum = ((intServiceRequestPageCount)).ToString();
                        inputService.UseChildAnd = "true";

                        inputService.ListOfServiceRequest = new WSOD_ServiceRequest_v1.ServiceRequest1[1];
                        inputService.ListOfServiceRequest[0] = new WSOD_ServiceRequest_v1.ServiceRequest1();

                        inputService.ListOfServiceRequest[0].ContactId = "='" + pContactId + "'";
                        inputService.ListOfServiceRequest[0].SRNumber = "";
                        inputService.ListOfServiceRequest[0].ServiceRequestId = "";
                        inputService.ListOfServiceRequest[0].Status = "";

                        //20150715: Revised inputService.ListOfServiceRequest[0].Type = "='Project Design'" to include new fields on the criteria
                        inputService.ListOfServiceRequest[0].Type = "='Technical'";
                        inputService.ListOfServiceRequest[0].Area = "='Project Design'";
                        
                        inputService.ListOfServiceRequest[0].Subject = "";
                        inputService.ListOfServiceRequest[0].OpenedTime = "(>='" + DateTime.Today.AddYears(-1).ToString("MM/dd/yyyy HH:mm:ss") + "') AND (<='" + DateTime.Now.AddDays(+1).ToString("MM/dd/yyyy HH:mm:ss") + "')";

                        srvcService.Url = pSession.GetURL();
                        srvcService.CookieContainer = pSession.GetCookieContainer();
                        srvcService.Timeout = 120000;

                        outputService = srvcService.ServiceRequestQueryPage(inputService);

                        intCtrSRResults = outputService.ListOfServiceRequest.Length;

                        if (intCtrSRResults > 0)
                        {
                            for (Int64 intIdxSR = 0; intIdxSR < intCtrSRResults; intIdxSR++)
                            {
                                ServiceRequest csServiceRequest = new ServiceRequest();

                                csServiceRequest.ServiceRequestContactId = outputService.ListOfServiceRequest[intIdxSR].ContactId;  //outputService.ListOfContact.Contact[0].Id;
                                csServiceRequest.ServiceRequestContactMembershipId = pMembershipId.ToString();
                                csServiceRequest.ServiceRequestId = outputService.ListOfServiceRequest[intIdxSR].ServiceRequestId;
                                csServiceRequest.ServiceRequestNumber = outputService.ListOfServiceRequest[intIdxSR].SRNumber;
                                csServiceRequest.ServiceRequestOpenedTime = outputService.ListOfServiceRequest[intIdxSR].OpenedTime.ToString();
                                csServiceRequest.ServiceRequestStatus = outputService.ListOfServiceRequest[intIdxSR].Status;
                                csServiceRequest.ServiceRequestSubject = outputService.ListOfServiceRequest[intIdxSR].Subject;
                                csServiceRequest.ServiceRequestType = outputService.ListOfServiceRequest[intIdxSR].Type;
                                csServiceRequest.ServiceRequestArea = outputService.ListOfServiceRequest[intIdxSR].Area;

                                arrContactProjectDesignRequestsDetails.Add(csServiceRequest);
                            }

                            if (intCtrSRResults >= 100)
                            {
                                intServiceRequestPageCount = intServiceRequestPageCount + 100;
                                hasRecords = true;
                            }
                            else
                            {
                                hasRecords = false;
                            }
                        }
                        else
                        {
                            arrContactProjectDesignRequestsDetails = null;
                            hasRecords = false;
                        }

                    }
                }
                else
                {
                    arrContactProjectDesignRequestsDetails = null;
                    hasRecords = false;
                }
            }
            catch (Exception ex)
            {
                arrContactProjectDesignRequestsDetails = null;
            }

            return arrContactProjectDesignRequestsDetails;
        }


        public ArrayList getContactProjectDesignRequests_v1(OnDemandSession pSession, String pMembershipId)
        {
            ArrayList arrContactProjectDesignRequestsDetails = new ArrayList();

            Int64 intCtrContactResults = 0;
            Int64 intCtrContactSRResults = 0;
            bool hasRecords = true;

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactQueryPage_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactQueryPage_Input();
            WSOD_Contact_v1.ContactWS_ContactQueryPage_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactQueryPage_Output();

            try
            {
                ServiceRequest csServiceRequest = new ServiceRequest();

                while (hasRecords == true)
                {
                    inputContact.PageSize = "100";
                    inputContact.StartRowNum = "0";
                    inputContact.UseChildAnd = "true";

                    //Build the list
                    inputContact.ListOfContact = new WSOD_Contact_v1.Contact1[1];
                    inputContact.ListOfContact[0] = new WSOD_Contact_v1.Contact1();
                    inputContact.ListOfContact[0].ListOfServiceRequest = new WSOD_Contact_v1.ServiceRequest[1];
                    inputContact.ListOfContact[0].ListOfServiceRequest[0] = new WSOD_Contact_v1.ServiceRequest();


                    //Populate the list
                    inputContact.ListOfContact[0].ContactFirstName = "";
                    inputContact.ListOfContact[0].ContactLastName = "";
                    inputContact.ListOfContact[0].ContactId = "";
                    inputContact.ListOfContact[0].ContactFullName = "";
                    inputContact.ListOfContact[0].ContactEmail = "";
                    inputContact.ListOfContact[0].Owner = "";
                    inputContact.ListOfContact[0].OwnerId = "";
                    inputContact.ListOfContact[0].OwnerEmailAddress = "";
                    inputContact.ListOfContact[0].IndexedNumber0 = "";
                    inputContact.ListOfContact[0].IndexedShortText1 = "='" + pMembershipId + "'";
                    inputContact.ListOfContact[0].AccountId = "";
                    inputContact.ListOfContact[0].AccountLocation = "";
                    inputContact.ListOfContact[0].AccountName = "";
                    inputContact.ListOfContact[0].AlternateCountry = "";

                    inputContact.ListOfContact[0].ListOfServiceRequest[0].ServiceRequestId = "";
                    inputContact.ListOfContact[0].ListOfServiceRequest[0].SRNumber = "";
                    inputContact.ListOfContact[0].ListOfServiceRequest[0].Status = "";
                    inputContact.ListOfContact[0].ListOfServiceRequest[0].Type = "='Project Design'";
                    inputContact.ListOfContact[0].ListOfServiceRequest[0].OpenedTime = "(>='" + DateTime.Today.AddYears(-1).ToString("MM/dd/yyyy HH:mm:ss") + "') AND (<='" + DateTime.Now.AddDays(+1).ToString("MM/dd/yyyy HH:mm:ss") + "')";
                    inputContact.ListOfContact[0].ListOfServiceRequest[0].Subject = "";

                    srvcContact.Url = pSession.GetURL();
                    srvcContact.CookieContainer = pSession.GetCookieContainer();
                    srvcContact.Timeout = 120000;

                    outputContact = srvcContact.ContactQueryPage(inputContact);

                    intCtrContactResults = outputContact.ListOfContact.Length;

                    if (intCtrContactResults == 1)
                    {
                        intCtrContactSRResults = outputContact.ListOfContact[0].ListOfServiceRequest.Length;

                        if (intCtrContactSRResults > 0)
                        {
                            for (Int64 intIdxContactSRCtr = 0; intIdxContactSRCtr < intCtrContactSRResults; intIdxContactSRCtr++)
                            {
                                csServiceRequest.ServiceRequestContactId = outputContact.ListOfContact[0].ContactId;
                                csServiceRequest.ServiceRequestContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                                csServiceRequest.ServiceRequestId = outputContact.ListOfContact[0].ListOfServiceRequest[intIdxContactSRCtr].ServiceRequestId;
                                csServiceRequest.ServiceRequestNumber = outputContact.ListOfContact[0].ListOfServiceRequest[intIdxContactSRCtr].SRNumber;
                                csServiceRequest.ServiceRequestOpenedTime = outputContact.ListOfContact[0].ListOfServiceRequest[intIdxContactSRCtr].OpenedTime.ToString();
                                csServiceRequest.ServiceRequestStatus = outputContact.ListOfContact[0].ListOfServiceRequest[intIdxContactSRCtr].Status;
                                csServiceRequest.ServiceRequestSubject = outputContact.ListOfContact[0].ListOfServiceRequest[intIdxContactSRCtr].Subject;
                                csServiceRequest.ServiceRequestType = outputContact.ListOfContact[0].ListOfServiceRequest[intIdxContactSRCtr].Type;

                                arrContactProjectDesignRequestsDetails.Add(csServiceRequest);
                            }

                            hasRecords = false;
                        }
                        else
                        {
                            arrContactProjectDesignRequestsDetails = null;
                            hasRecords = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                arrContactProjectDesignRequestsDetails = null;
            }

            return arrContactProjectDesignRequestsDetails;
        }


        public ArrayList getContactProjectDesignRequests_2(OnDemandSession pSession,String pMembershipId)
        {
            ArrayList arrContactProjectDesignRequestsDetails = new ArrayList();
            Int64 intCtrContactResults = 0;
            Int64 intContactSRPageCount = 0;

            bool hasRecords = true;

            WSOD_Contact_v2.Contact srvcContact = new WSOD_Contact_v2.Contact();
            WSOD_Contact_v2.ContactQueryPage_Input inputContact = new WSOD_Contact_v2.ContactQueryPage_Input();
            WSOD_Contact_v2.ContactQueryPage_Output outputContact = new WSOD_Contact_v2.ContactQueryPage_Output();

            try
            {
                ServiceRequest csServiceRequest = new ServiceRequest();

                while (hasRecords == true)
                {
                    inputContact.ListOfContact = new WSOD_Contact_v2.ListOfContactQuery();
                    inputContact.ListOfContact.Contact = new WSOD_Contact_v2.ContactQuery();
                    inputContact.ListOfContact.Contact.ListOfServiceRequest = new WSOD_Contact_v2.ListOfServiceRequestQuery();
                    inputContact.ListOfContact.Contact.ListOfServiceRequest.ServiceRequest = new WSOD_Contact_v2.ServiceRequestQuery();

                    inputContact.ListOfContact.pagesize = "1";
                    inputContact.ListOfContact.startrownum = "0";

                    inputContact.ListOfContact.Contact.ListOfServiceRequest.pagesize = "100";
                    inputContact.ListOfContact.Contact.ListOfServiceRequest.startrownum = ((intContactSRPageCount)).ToString();

                    inputContact.ListOfContact.Contact.ContactFirstName = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ContactLastName = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.Id = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ContactFullName = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ContactEmail = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.Owner = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.OwnerId = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.IndexedNumber0 = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.IndexedShortText1 = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.AccountId = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.AccountName = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.AccountLocation = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.AlternateCountry = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.searchspec = "[IndexedShortText1]='" + pMembershipId + "'";

                    inputContact.ListOfContact.Contact.ListOfServiceRequest.ServiceRequest.Id = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ListOfServiceRequest.ServiceRequest.SRNumber = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ListOfServiceRequest.ServiceRequest.Status = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ListOfServiceRequest.ServiceRequest.Type = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ListOfServiceRequest.ServiceRequest.OpenedTime = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ListOfServiceRequest.ServiceRequest.Subject = new WSOD_Contact_v2.queryType();
                    inputContact.ListOfContact.Contact.ListOfServiceRequest.ServiceRequest.searchspec = "[Type] ='Project Design' AND ([OpenedTime]>='" + DateTime.Today.AddYears(-1).ToString("MM/dd/yyyy HH:mm:ss") + "' AND [OpenedTime]<='" + DateTime.Now.AddDays(+1).ToString("MM/dd/yyyy HH:mm:ss") + "')";
                    inputContact.ListOfContact.Contact.ListOfServiceRequest.ServiceRequest.OpenedTime.sortorder = "DESC";

                    
                    srvcContact.Url = pSession.GetURL();
                    srvcContact.CookieContainer = pSession.GetCookieContainer();
                    srvcContact.Timeout = 120000;

                    outputContact = srvcContact.ContactQueryPage(inputContact);

                    if (outputContact.ListOfContact.Contact != null)
                    {
                        intCtrContactResults = outputContact.ListOfContact.Contact.Length;

                        if (intCtrContactResults == 1)
                        {
                            if (outputContact.ListOfContact.Contact[0].ListOfServiceRequest.ServiceRequest != null)
                            {
                                intContactSRPageCount = outputContact.ListOfContact.Contact[0].ListOfServiceRequest.ServiceRequest.Length;

                                if (intContactSRPageCount > 0)
                                {
                                    for (Int64 intIdxCtrSR = 0; intIdxCtrSR < intContactSRPageCount; intIdxCtrSR++)
                                    {
                                        csServiceRequest.ServiceRequestContactId = outputContact.ListOfContact.Contact[0].Id;
                                        csServiceRequest.ServiceRequestContactMembershipId = outputContact.ListOfContact.Contact[0].IndexedShortText1;
                                        csServiceRequest.ServiceRequestId = outputContact.ListOfContact.Contact[0].ListOfServiceRequest.ServiceRequest[intIdxCtrSR].Id;
                                        csServiceRequest.ServiceRequestNumber = outputContact.ListOfContact.Contact[0].ListOfServiceRequest.ServiceRequest[intIdxCtrSR].SRNumber;
                                        csServiceRequest.ServiceRequestOpenedTime = outputContact.ListOfContact.Contact[0].ListOfServiceRequest.ServiceRequest[intIdxCtrSR].OpenedTime.ToString();
                                        csServiceRequest.ServiceRequestStatus = outputContact.ListOfContact.Contact[0].ListOfServiceRequest.ServiceRequest[intIdxCtrSR].Status;
                                        csServiceRequest.ServiceRequestSubject = outputContact.ListOfContact.Contact[0].ListOfServiceRequest.ServiceRequest[intIdxCtrSR].Subject;
                                        csServiceRequest.ServiceRequestType = outputContact.ListOfContact.Contact[0].ListOfServiceRequest.ServiceRequest[intIdxCtrSR].Type;

                                        arrContactProjectDesignRequestsDetails.Add(csServiceRequest);
                                    }

                                    if (intContactSRPageCount >= 100)
                                    {
                                        hasRecords = true;
                                        intContactSRPageCount = intContactSRPageCount + 100;
                                    }
                                    else
                                    {
                                        hasRecords = false;
                                    }
                                }
                                else
                                {
                                    arrContactProjectDesignRequestsDetails = null;
                                    hasRecords = false;
                                }

                            }
                            else
                            {
                                arrContactProjectDesignRequestsDetails = null;
                                hasRecords = false;
                            }
                        }
                        else
                        {
                            arrContactProjectDesignRequestsDetails = null;
                            hasRecords = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                arrContactProjectDesignRequestsDetails = null;
            }

            return arrContactProjectDesignRequestsDetails;
        }

        /*
         * 20160516 - RDVillamor
         * 1. Added the following fields to be displayed on this method: PartnerPlus Business Name, Roles by Country, User Type, Middle Name, Email address, Contact Type, Partnerplus User Type,and Address fields such as Address1, Address2, City, Province.
         * 
         * 20160519 - RDVillamor
         * 1. Added Date of Registration,CellularNumber, ContactEmail and ContactCountry on the return set
         * 
         * 20160524 - RDVillamor
         * 1. Added Middle Name on the return set
         * 
         * 20160609 - RDVillamor
         * 1. Enhance the display of Business name, to use the existing Account Name as default value if no value is found on Business Name
         */
        public ArrayList getContactDetails(OnDemandSession pSession,String pMembershipId)
        {
            ArrayList arrPartnerWebContactDetails = new ArrayList();
            Int64 intCtrContactResults = 0;
            String pAccountCustID = "";

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactQueryPage_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactQueryPage_Input();
            WSOD_Contact_v1.ContactWS_ContactQueryPage_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactQueryPage_Output();

            try
            {
                ContactV1 csContact = new ContactV1();

                inputContact.PageSize = "100";
                inputContact.StartRowNum = "0";
                inputContact.UseChildAnd = "true";

                //Build the list
                inputContact.ListOfContact = new WSOD_Contact_v1.Contact1[1];
                inputContact.ListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                inputContact.ListOfContact[0].ContactFirstName = "";
                inputContact.ListOfContact[0].ContactLastName = "";
                inputContact.ListOfContact[0].ContactId = "";
                inputContact.ListOfContact[0].ContactFullName = "";
                inputContact.ListOfContact[0].ContactEmail = "";
                inputContact.ListOfContact[0].Owner = "";
                inputContact.ListOfContact[0].OwnerId = "";
                inputContact.ListOfContact[0].OwnerEmailAddress = "";
                inputContact.ListOfContact[0].IndexedNumber0 = "";
                inputContact.ListOfContact[0].IndexedShortText1 = "='" + pMembershipId + "'";
                inputContact.ListOfContact[0].AccountId = "";
                inputContact.ListOfContact[0].AccountLocation = "";
                inputContact.ListOfContact[0].AccountName = "";
                inputContact.ListOfContact[0].AlternateCountry = "";

                //20160516, refer on item#1
                inputContact.ListOfContact[0].MiddleName = "";
                //inputContact.ListOfContact[0].stPartnerPlus_Business_Name = "";
                inputContact.ListOfContact[0].ltPartnerPlus_Business_Name = "";
                inputContact.ListOfContact[0].stPartnerPlus_Roles_By_Country = "";
                inputContact.ListOfContact[0].plPartnerPlus_User_Type = "";
                inputContact.ListOfContact[0].ContactType = "";
                inputContact.ListOfContact[0].AccountId = "";
                inputContact.ListOfContact[0].AccountName = "";
                inputContact.ListOfContact[0].AccountLocation = "";
                inputContact.ListOfContact[0].AlternateAddress1 = "";
                inputContact.ListOfContact[0].AlternateAddress2 = "";
                inputContact.ListOfContact[0].AlternateAddress3 = "";
                inputContact.ListOfContact[0].AlternateCity = "";
                inputContact.ListOfContact[0].AlternateProvince = "";

                //20160519, refer on item#1
                inputContact.ListOfContact[0].CellularPhone="";
                inputContact.ListOfContact[0].dMembership_Validity = "";
                inputContact.ListOfContact[0].plMembership_Status = "";
                inputContact.ListOfContact[0].ltPartnerPlus_Business_Name = "";

                if (pSession == null)
                {
                    pSession = EnterLoginCredentials();
                }

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();
                srvcContact.Timeout = 120000;

                outputContact = srvcContact.ContactQueryPage(inputContact);

                intCtrContactResults = outputContact.ListOfContact.Length;

                if (intCtrContactResults > 0)
                {
                    /*20160517 - RDVillamor*/
                    /*this has been commented since wrong field was being assigned on the contact Membership Id file*/
                    //csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedNumber0;

                    /*20160517 - RDVillamor*/
                    /*to correct the error, IndexedShortText1 has been included on the return set*/
                    csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                    csContact.ContactId = outputContact.ListOfContact[0].ContactId;
                    csContact.ContactFullName = outputContact.ListOfContact[0].ContactFullName;
                    csContact.ContactFirstName = outputContact.ListOfContact[0].ContactFirstName;
                    csContact.ContactLastName = outputContact.ListOfContact[0].ContactLastName;
                    csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                    csContact.ContactAccountId = outputContact.ListOfContact[0].AccountId;
                    csContact.ContactAccountLocation = outputContact.ListOfContact[0].AccountLocation;
                    csContact.ContactAccountName = outputContact.ListOfContact[0].AccountName;

                    pAccountCustID = getAccountCustID(pSession, outputContact.ListOfContact[0].AccountId.ToString());

                    csContact.ContactAccountNumber = pAccountCustID.ToString();

                    //enhanced on 20160516, refer on item#1
                    //enhanced on 20160609, refer to item#1
                    if ((outputContact.ListOfContact[0].ltPartnerPlus_Business_Name == "") || (outputContact.ListOfContact[0].ltPartnerPlus_Business_Name == null))
                    {
                        csContact.ContactAccountName = outputContact.ListOfContact[0].AccountName;
                        
                    }
                    else
                    {
                        csContact.ContactAccountName = outputContact.ListOfContact[0].ltPartnerPlus_Business_Name;
                    }

                    csContact.BusinessName = outputContact.ListOfContact[0].ltPartnerPlus_Business_Name;
                    csContact.ContactMiddleName = outputContact.ListOfContact[0].MiddleName;
                    csContact.PartnerplusRole = outputContact.ListOfContact[0].stPartnerPlus_Roles_By_Country;
                    csContact.PartnerplusUserType = outputContact.ListOfContact[0].plPartnerPlus_User_Type;
                    csContact.ContactType = outputContact.ListOfContact[0].ContactType;
                    csContact.ContactAccountId = outputContact.ListOfContact[0].AccountId;
                    csContact.ContactAccountName = outputContact.ListOfContact[0].AccountName;
                    csContact.ContactAccountLocation = outputContact.ListOfContact[0].AccountLocation;

                    csContact.ContactAddress1 = outputContact.ListOfContact[0].AlternateAddress1;
                    csContact.ContactAddress2 = outputContact.ListOfContact[0].AlternateAddress2;
                    csContact.ContactAddress3 = outputContact.ListOfContact[0].AlternateAddress3;
                    csContact.ContactCity = outputContact.ListOfContact[0].AlternateCity;
                    csContact.ContactProvince = outputContact.ListOfContact[0].AlternateProvince;

                    //20160519, refer on item#1
                    csContact.ContactEmailAddress = outputContact.ListOfContact[0].ContactEmail;
                    csContact.ContactCountry = outputContact.ListOfContact[0].AlternateCountry;
                    csContact.ContactCellularPhone = outputContact.ListOfContact[0].CellularPhone;
                    csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;

                    //20160524, refer on item#1
                    csContact.ContactMiddleName = outputContact.ListOfContact[0].MiddleName;
                    csContact.PartnerplusMembershipStatus = outputContact.ListOfContact[0].plMembership_Status;

                    arrPartnerWebContactDetails.Add(csContact);
                }
            }
            catch (Exception ex)
            {
                ContactV1 csContact = new ContactV1();
                csContact.ErrorMessage = ex.Message;
                csContact.ContactMembershipId = pMembershipId;

                arrPartnerWebContactDetails.Add(csContact);
            }

            return arrPartnerWebContactDetails;
        }

        //20150316 - Added for PartnerWeb User Profile 
        public ArrayList getPartnerWebContactUserProfile(OnDemandSession pSession, String pMembershipId)
        {
            ArrayList arrPartnerWebContactDetails = new ArrayList();
            Int64 intCtrContactResults = 0;
            Int64 intCtrContactAccounts = 0;

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactQueryPage_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactQueryPage_Input();
            WSOD_Contact_v1.ContactWS_ContactQueryPage_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactQueryPage_Output();

            try
            {
                inputContact.PageSize = "100";
                inputContact.StartRowNum = "0";
                inputContact.UseChildAnd = "true";

                //Build the list
                inputContact.ListOfContact = new WSOD_Contact_v1.Contact1[1];
                inputContact.ListOfContact[0] = new WSOD_Contact_v1.Contact1();
                inputContact.ListOfContact[0].ListOfAccount = new WSOD_Contact_v1.Account[1];
                inputContact.ListOfContact[0].ListOfAccount[0] = new WSOD_Contact_v1.Account();

                //Populate the list
                inputContact.ListOfContact[0].ContactFirstName = "";
                inputContact.ListOfContact[0].ContactLastName = "";
                inputContact.ListOfContact[0].MiddleName = "";
                inputContact.ListOfContact[0].ContactId = "";
                inputContact.ListOfContact[0].ContactFullName = "";
                inputContact.ListOfContact[0].ContactEmail = "";
                inputContact.ListOfContact[0].ContactType = "";
                inputContact.ListOfContact[0].Owner = "";
                inputContact.ListOfContact[0].OwnerId = "";
                inputContact.ListOfContact[0].OwnerEmailAddress = "";
                inputContact.ListOfContact[0].IndexedNumber0 = "";
                inputContact.ListOfContact[0].IndexedShortText1 = "='" + pMembershipId + "'";
                inputContact.ListOfContact[0].AccountId = "";
                inputContact.ListOfContact[0].AccountLocation = "";
                inputContact.ListOfContact[0].AccountName = "";
                inputContact.ListOfContact[0].AlternateCountry = "";
                inputContact.ListOfContact[0].dMembership_Validity = "";
                inputContact.ListOfContact[0].bNever_Phone = "";
                inputContact.ListOfContact[0].bNever_SMS = "";
                inputContact.ListOfContact[0].NeverEmail = "";
                inputContact.ListOfContact[0].MiddleName = "";
                inputContact.ListOfContact[0].ContactEmail = "";
                inputContact.ListOfContact[0].CellularPhone = "";
                inputContact.ListOfContact[0].ContactType = "";
                inputContact.ListOfContact[0].iYears_of_Experience_in_HVACR_industry = "";
                inputContact.ListOfContact[0].JobTitle = "";
                inputContact.ListOfContact[0].msplPermission_Marketing_Target = "";
                inputContact.ListOfContact[0].ltMarket = "";
                inputContact.ListOfContact[0].ltPartnerWeb_Contact_Comments = "";
                inputContact.ListOfContact[0].ltScope_of_Work = "";
                inputContact.ListOfContact[0].bCompressor = "";
                inputContact.ListOfContact[0].bCondensing_Units = "";
                inputContact.ListOfContact[0].bControllers = "";
                inputContact.ListOfContact[0].bLine_Components = "";
                inputContact.ListOfContact[0].ltScope_of_Work = "";
                inputContact.ListOfContact[0].ltReferred_by_Company = "";

                inputContact.ListOfContact[0].AlternateAddress1 = "";
                inputContact.ListOfContact[0].AlternateAddress2 = "";
                inputContact.ListOfContact[0].AlternateAddress3 = "";
                inputContact.ListOfContact[0].AlternateCity = "";
                inputContact.ListOfContact[0].AlternateCountry = "";
                inputContact.ListOfContact[0].AlternateStateProvince = "";
                inputContact.ListOfContact[0].AlternateProvince = "";
                inputContact.ListOfContact[0].AlternateZipCode = "";

                //start: 20160613 - RDVillamor, added to display the Business Name provided by the member during sign-up
                //inputContact.ListOfContact[0].stPartnerPlus_Business_Name = "";
                inputContact.ListOfContact[0].ltPartnerPlus_Business_Name = "";
                inputContact.ListOfContact[0].stPartnerPlus_Current_Brand_Usage_Others = "";
                inputContact.ListOfContact[0].ltPartnerPlus_Business_Industry = "";
                inputContact.ListOfContact[0].ltPartnerPlus_Services_Offered = "";
                inputContact.ListOfContact[0].bPartnerPlus_Current_Brand_Used_Emerson = "";
                inputContact.ListOfContact[0].WorkPhone = "";
                //end: 20160613 - RDVillamor, added to display the Business Name provided by the member during sign-up

                //start: 20160617: added to display the values on Key Interest
                inputContact.ListOfContact[0].ltPartnerPlus_Key_Interests = "";
                //end: 20160617: added to display the values on Key Interest

                inputContact.ListOfContact[0].ListOfAccount[0].AccountId = "";
                inputContact.ListOfContact[0].ListOfAccount[0].AccountName = "";

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();
                srvcContact.Timeout = 120000;

                outputContact = srvcContact.ContactQueryPage(inputContact);

                intCtrContactResults = outputContact.ListOfContact.Length;

                if (intCtrContactResults > 0)
                {
                    ContactV1 csContact = new ContactV1();

                    csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedNumber0;
                    csContact.ContactId = outputContact.ListOfContact[0].ContactId;
                    csContact.ContactFullName = outputContact.ListOfContact[0].ContactFullName;
                    csContact.ContactFirstName = outputContact.ListOfContact[0].ContactFirstName;
                    csContact.ContactMiddleName = outputContact.ListOfContact[0].MiddleName;
                    csContact.ContactLastName = outputContact.ListOfContact[0].ContactLastName;
                    csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                    csContact.ContactEmailAddress = outputContact.ListOfContact[0].ContactEmail;
                    csContact.ContactCellularPhone = outputContact.ListOfContact[0].CellularPhone;

                    //20150908 - Condition Added by RDVillamor to handle Korea as Equivalent to South Korea
                    if (outputContact.ListOfContact[0].AlternateCountry == "Korea")
                    {
                        csContact.ContactCountry = "South Korea";
                    }
                    else if (outputContact.ListOfContact[0].AlternateCountry == "Congo, Democratic Republic of")
                    {
                        csContact.ContactCountry = "Democratic Republic of the Congo";
                    }
                    else if (outputContact.ListOfContact[0].AlternateCountry == "Libyan Arab Jamahiriya")
                    {
                        csContact.ContactCountry = "Libya";
                    }
                    else if (outputContact.ListOfContact[0].AlternateCountry == "Tanzania, United Republic of")
                    {
                        csContact.ContactCountry = "Tanzania";
                    }
                    else
                    {
                        csContact.ContactCountry = outputContact.ListOfContact[0].AlternateCountry;
                    }

                    csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;
                    csContact.ContactYearsOfHVACExp = outputContact.ListOfContact[0].iYears_of_Experience_in_HVACR_industry;
                    csContact.ContactJobTitle = outputContact.ListOfContact[0].JobTitle;
                    csContact.ContactScopeOfWork = outputContact.ListOfContact[0].ltScope_of_Work;
                    csContact.IsCompressorContact = outputContact.ListOfContact[0].bCompressor;
                    csContact.IsCondensingUnitsContact = outputContact.ListOfContact[0].bCondensing_Units;
                    csContact.IsControllersContact = outputContact.ListOfContact[0].bControllers;
                    csContact.IsLineComponentsContact = outputContact.ListOfContact[0].bLine_Components;
                    csContact.ContactPartnerWebComments = outputContact.ListOfContact[0].ltPartnerWeb_Contact_Comments;
                    
                    ////20160617 - RDVillamor, commented to replace the field being mapped
                    //csContact.ContactInterests = outputContact.ListOfContact[0].msplPermission_Marketing_Target;
                    ////end

                    csContact.ContactInterests = outputContact.ListOfContact[0].ltPartnerPlus_Key_Interests;
                   
                    csContact.ContactMarket = outputContact.ListOfContact[0].ltMarket;
                    csContact.ContactNeverEmail = outputContact.ListOfContact[0].NeverEmail;
                    csContact.ContactNeverPhone = outputContact.ListOfContact[0].bNever_Phone;
                    csContact.ContactNeverSMS = outputContact.ListOfContact[0].bNever_SMS;

                    if (outputContact.ListOfContact[0].ContactType == "Internal")
                    {
                        csContact.ContactType = "Emerson Employee";
                    }
                    else
                    {
                        csContact.ContactType = outputContact.ListOfContact[0].ContactType;
                    }

                    csContact.ContactReferredBy = outputContact.ListOfContact[0].ltReferred_by_Company;

                    csContact.ContactAddress1 = outputContact.ListOfContact[0].AlternateAddress1;
                    csContact.ContactAddress2 = outputContact.ListOfContact[0].AlternateAddress2;
                    csContact.ContactCity = outputContact.ListOfContact[0].AlternateCity;
                    csContact.ContactProvince = outputContact.ListOfContact[0].AlternateProvince;
                    csContact.ContactState = outputContact.ListOfContact[0].AlternateStateProvince;
                    csContact.ContactZIPCode = outputContact.ListOfContact[0].AlternateZipCode;

                    csContact.ContactAccountId = outputContact.ListOfContact[0].AccountId;
                    csContact.ContactAccountLocation = outputContact.ListOfContact[0].AccountLocation;

                    if ((outputContact.ListOfContact[0].ltPartnerPlus_Business_Name != null) || (outputContact.ListOfContact[0].ltPartnerPlus_Business_Name==""))
                    {
                        csContact.ContactAccountName = outputContact.ListOfContact[0].ltPartnerPlus_Business_Name;
                    }
                    else
                    {
                        csContact.ContactAccountName = outputContact.ListOfContact[0].AccountName;
                    }

                    //start: 20160613 - RDVillamor, added to skip the update on Account level
                    csContact.ContactListOfAccountIfOthersBrandUseEmerson = outputContact.ListOfContact[0].stPartnerPlus_Current_Brand_Usage_Others;
                    csContact.ContactListOfAccountIsBrandUseEmerson = outputContact.ListOfContact[0].bPartnerPlus_Current_Brand_Used_Emerson;
                    csContact.ContactListOfAccountServicesOffered = outputContact.ListOfContact[0].ltPartnerPlus_Services_Offered;
                    csContact.ContactListOfAccountBusinessIndustry = outputContact.ListOfContact[0].ltPartnerPlus_Business_Industry;
                    csContact.ContactListOfAccountMainPhone = outputContact.ListOfContact[0].WorkPhone;

                    //end: 20160613 - RDVillamor, added to skip the update on Account level

                    //Display values from List of Account Fields
                    ////20160613 - RDVillamor, commented since this is no longer needed
                    //csContact.ContactListOfAccountId = outputContactAccountDetails.ListOfAccount[0].AccountId;

                    //if (csContact.ContactAccountId.ToString() == csContact.ContactListOfAccountId.ToString())
                    //{
                    //    csContact.ContactListOfAccountIsPrimary = "Y";
                    //}
                    //else
                    //{
                    //    csContact.ContactListOfAccountIsPrimary = "N";
                    //}

                    //csContact.ContactListOfAccountName = outputContactAccountDetails.ListOfAccount[0].AccountName;
                    //csContact.ContactListOfAccountType = outputContactAccountDetails.ListOfAccount[0].AccountType;
                    //csContact.ContactListOfAccountBusinessIndustry = outputContactAccountDetails.ListOfAccount[0].ltBusiness_Industry;
                    //csContact.ContactListOfAccountServicesOffered = outputContactAccountDetails.ListOfAccount[0].ltServices_Offered;
                    //csContact.ContactListOfAccountIsBrandUseEmerson = outputContactAccountDetails.ListOfAccount[0].bCurrent_Brand_Used_Emerson;
                    //csContact.ContactListOfAccountIfOthersBrandUseEmerson = outputContactAccountDetails.ListOfAccount[0].stCurrent_Brand_Others;

                    //csContact.ContactListOfAccountAddress = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToStreetAddress + " " + outputContactAccountDetails.ListOfAccount[0].PrimaryShipToStreetAddress2 + " " + outputContactAccountDetails.ListOfAccount[0].PrimaryShipToStreetAddress3;
                    //csContact.ContactListOfAccountCity = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToCity;
                    //csContact.ContactListOfAccountProvince = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToProvince;
                    //csContact.ContactListOfAccountState = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToState;

                    ////20150908 - Condition Added by RDVillamor to handle Korea as Equivalent to South Korea
                    //if (outputContactAccountDetails.ListOfAccount[0].PrimaryShipToCountry == "Korea")
                    //{
                    //    csContact.ContactListOfAccountCountry = "South Korea";
                    //}
                    //else
                    //{
                    //    csContact.ContactListOfAccountCountry = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToCountry;
                    //}

                    //csContact.ContactListOfAccountZIP = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToPostalCode;
                    //csContact.ContactListOfAccountMainPhone = outputContactAccountDetails.ListOfAccount[0].MainPhone;

                    arrPartnerWebContactDetails.Add(csContact);
 
                }


                //if (intCtrContactResults > 0)
                //{
                //   intCtrContactAccounts = outputContact.ListOfContact[0].ListOfAccount.Length;

                //    if (intCtrContactAccounts > 0)
                //    {
                //        for (Int64 intIdxContactAcct = 0; intIdxContactAcct < intCtrContactAccounts; intIdxContactAcct++)
                //        {
                //            WSOD_Account_v1.AccountWS_AccountQueryPage_Output outputContactAccountDetails = new WSOD_Account_v1.AccountWS_AccountQueryPage_Output();
                //            ContactV1 csContact = new ContactV1();

                //            outputContactAccountDetails =  getAccountDetails(pSession, outputContact.ListOfContact[0].ListOfAccount[intIdxContactAcct].AccountId.ToString());

                //            if (outputContactAccountDetails != null)
                //            {
                //                csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedNumber0;
                //                csContact.ContactId = outputContact.ListOfContact[0].ContactId;
                //                csContact.ContactFullName = outputContact.ListOfContact[0].ContactFullName;
                //                csContact.ContactFirstName = outputContact.ListOfContact[0].ContactFirstName;
                //                csContact.ContactMiddleName = outputContact.ListOfContact[0].MiddleName;
                //                csContact.ContactLastName = outputContact.ListOfContact[0].ContactLastName;
                //                csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                //                csContact.ContactEmailAddress = outputContact.ListOfContact[0].ContactEmail;
                //                csContact.ContactCellularPhone = outputContact.ListOfContact[0].CellularPhone;
                                
                //                //20150908 - Condition Added by RDVillamor to handle Korea as Equivalent to South Korea
                //                if (outputContact.ListOfContact[0].AlternateCountry == "Korea")
                //                {
                //                    csContact.ContactCountry = "South Korea";
                //                }
                //                else
                //                {
                //                    csContact.ContactCountry = outputContact.ListOfContact[0].AlternateCountry;
                //                }

                //                csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;
                //                csContact.ContactYearsOfHVACExp = outputContact.ListOfContact[0].iYears_of_Experience_in_HVACR_industry;
                //                csContact.ContactJobTitle = outputContact.ListOfContact[0].JobTitle;
                //                csContact.ContactScopeOfWork = outputContact.ListOfContact[0].ltScope_of_Work;
                //                csContact.IsCompressorContact = outputContact.ListOfContact[0].bCompressor;
                //                csContact.IsCondensingUnitsContact = outputContact.ListOfContact[0].bCondensing_Units;
                //                csContact.IsControllersContact = outputContact.ListOfContact[0].bControllers;
                //                csContact.IsLineComponentsContact = outputContact.ListOfContact[0].bLine_Components;
                //                csContact.ContactPartnerWebComments = outputContact.ListOfContact[0].ltPartnerWeb_Contact_Comments;
                //                csContact.ContactInterests = outputContact.ListOfContact[0].msplPermission_Marketing_Target;
                //                csContact.ContactMarket = outputContact.ListOfContact[0].ltMarket;
                //                csContact.ContactNeverEmail = outputContact.ListOfContact[0].NeverEmail;
                //                csContact.ContactNeverPhone = outputContact.ListOfContact[0].bNever_Phone;
                //                csContact.ContactNeverSMS = outputContact.ListOfContact[0].bNever_SMS;
                //                csContact.ContactType = outputContact.ListOfContact[0].ContactType;
                //                csContact.ContactReferredBy = outputContact.ListOfContact[0].ltReferred_by_Company;

                //                csContact.ContactAddress1 = outputContact.ListOfContact[0].AlternateAddress1;
                //                csContact.ContactAddress2 = outputContact.ListOfContact[0].AlternateAddress2;
                //                csContact.ContactCity = outputContact.ListOfContact[0].AlternateCity;
                //                csContact.ContactCountry = outputContact.ListOfContact[0].AlternateCountry;
                //                csContact.ContactProvince = outputContact.ListOfContact[0].AlternateProvince;
                //                csContact.ContactState = outputContact.ListOfContact[0].AlternateStateProvince;
                //                csContact.ContactZIPCode = outputContact.ListOfContact[0].AlternateZipCode;

                //                csContact.ContactAccountId = outputContact.ListOfContact[0].AccountId;
                //                csContact.ContactAccountLocation = outputContact.ListOfContact[0].AccountLocation;
                //                csContact.ContactAccountName = outputContact.ListOfContact[0].AccountName;

                //                //Display values from List of Account Fields

                //                csContact.ContactListOfAccountId = outputContactAccountDetails.ListOfAccount[0].AccountId;

                //                if (csContact.ContactAccountId.ToString() == csContact.ContactListOfAccountId.ToString())
                //                {
                //                    csContact.ContactListOfAccountIsPrimary = "Y";
                //                }
                //                else
                //                {
                //                    csContact.ContactListOfAccountIsPrimary = "N";
                //                }

                //                csContact.ContactListOfAccountName = outputContactAccountDetails.ListOfAccount[0].AccountName;
                //                csContact.ContactListOfAccountType = outputContactAccountDetails.ListOfAccount[0].AccountType;
                //                csContact.ContactListOfAccountBusinessIndustry = outputContactAccountDetails.ListOfAccount[0].ltBusiness_Industry;
                //                csContact.ContactListOfAccountServicesOffered = outputContactAccountDetails.ListOfAccount[0].ltServices_Offered;
                //                csContact.ContactListOfAccountIsBrandUseEmerson = outputContactAccountDetails.ListOfAccount[0].bCurrent_Brand_Used_Emerson;
                //                csContact.ContactListOfAccountIfOthersBrandUseEmerson = outputContactAccountDetails.ListOfAccount[0].stCurrent_Brand_Others;

                //                csContact.ContactListOfAccountAddress = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToStreetAddress + " " + outputContactAccountDetails.ListOfAccount[0].PrimaryShipToStreetAddress2 + " " + outputContactAccountDetails.ListOfAccount[0].PrimaryShipToStreetAddress3;
                //                csContact.ContactListOfAccountCity = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToCity;
                //                csContact.ContactListOfAccountProvince = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToProvince;
                //                csContact.ContactListOfAccountState = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToState;

                //                //20150908 - Condition Added by RDVillamor to handle Korea as Equivalent to South Korea
                //                if (outputContactAccountDetails.ListOfAccount[0].PrimaryShipToCountry == "Korea")
                //                {
                //                    csContact.ContactListOfAccountCountry = "South Korea";
                //                }
                //                else
                //                {
                //                    csContact.ContactListOfAccountCountry = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToCountry;
                //                }

                //                csContact.ContactListOfAccountZIP = outputContactAccountDetails.ListOfAccount[0].PrimaryShipToPostalCode;
                //                csContact.ContactListOfAccountMainPhone = outputContactAccountDetails.ListOfAccount[0].MainPhone;

                //                arrPartnerWebContactDetails.Add(csContact);
                //            }

                //        }

                //    }

                //}
            }
            catch (Exception ex)
            {
                arrPartnerWebContactDetails = null;
            }

            return arrPartnerWebContactDetails;
        }

        //20160719 - added by RDVillamor
        /*
         * Changes:
         * 20160809 / RDVillamor
         * 1. Hide the display of values on ContactListOfCustomerIndustry
         * 2. Activate the display of values on ContactListOfCustomerIndustry, hide the values on ContactListOfJobSiteType
         * 
         * 20160810 / RDVillamor
         * 1. Enhanced the query to use "page size" on display of child records objects. By default, only 10 records can be displayed on each child record type
         * 2. Enhanced the query to use "ViewMode" then set it to "Broadest" to be able to query using the highest level of access.
         */
        public ArrayList getPartnerWebContactUserProfile_Enhanced_for_Profile_Builder(OnDemandSession pSession, String pMembershipId)
        {
            ArrayList arrPartnerplusContactDetails = new ArrayList();
            
            String vJobSiteType="";
            String vKeyAreasToImprove="";
            String vPrimaryJobFocus = "";
            String vAccountId = "";


            WSOD_Contact_v2.Contact srvcContact = new WSOD_Contact_v2.Contact();
            WSOD_Contact_v2.ContactQueryPage_Input inputContactQuery = new WSOD_Contact_v2.ContactQueryPage_Input();
            WSOD_Contact_v2.ContactQueryPage_Output outputContactQuery = new WSOD_Contact_v2.ContactQueryPage_Output();

            try
            {
                inputContactQuery.ListOfContact = new WSOD_Contact_v2.ListOfContactQuery();
                inputContactQuery.ListOfContact.Contact = new WSOD_Contact_v2.ContactQuery();

                // Account Extension - Custom Object 10
                inputContactQuery.ListOfContact.Contact.ListOfCustomObject10 = new WSOD_Contact_v2.ListOfCustomObject10Query();
                
                // Contact Extension - Custom Object 11
                inputContactQuery.ListOfContact.Contact.ListOfCustomObject11 = new WSOD_Contact_v2.ListOfCustomObject11Query();

                inputContactQuery.ListOfContact.pagesize = "1";
                inputContactQuery.ListOfContact.startrownum = "0";
                inputContactQuery.ViewMode = "Broadest";

                inputContactQuery.ListOfContact.Contact.Id = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.IndexedShortText1 = new WSOD_Contact_v2.queryType();

                inputContactQuery.ListOfContact.Contact.AccountId = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AccountLocation = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AccountName = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.wCRM_Contact_Link = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.bNever_SMS = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactFirstName = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactLastName = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactEmail = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.WorkPhone = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.CellularPhone = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.HomePhone = new WSOD_Contact_v2.queryType();

                inputContactQuery.ListOfContact.Contact.JobTitle = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.stSecond_Email = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactType = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.OwnerId = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.NeverEmail = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ltPartnerPlus_Business_Name = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.stPartnerPlus_Roles_By_Country = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.plMembership_Status = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.plPartnerPlus_Years_of_Experience_in_HVACR_Industry = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.plSpoken_Language = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.plMobile_Device = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.plPartnerPlus_Years_of_Experience_in_HVACR_Industry = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.stHas_Mobile_Data_Connection_Outside_Office = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.IndexedPick1 = new WSOD_Contact_v2.queryType(); //Major Group (Job Function)
                inputContactQuery.ListOfContact.Contact.IndexedPick2 = new WSOD_Contact_v2.queryType(); // Job Specific

                inputContactQuery.ListOfContact.Contact.AlternateAddress1 = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateAddress2 = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateAddress3 = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateCity = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateCountry = new WSOD_Contact_v2.queryType();
                
                ////Account Extension - Custom Object 10
                //inputContactQuery.ListOfContact.Contact.ListOfCustomObject10.CustomObject10 = new WSOD_Contact_v2.CustomObject10Query();
                //inputContactQuery.ListOfContact.Contact.ListOfCustomObject10.CustomObject10.Type = new WSOD_Contact_v2.queryType();
                //inputContactQuery.ListOfContact.Contact.ListOfCustomObject10.CustomObject10.Name = new WSOD_Contact_v2.queryType();
                //inputContactQuery.ListOfContact.Contact.ListOfCustomObject10.CustomObject10.Id = new WSOD_Contact_v2.queryType();

                ////Contact Extension = Custom Object 11
                inputContactQuery.ListOfContact.Contact.ListOfCustomObject11.pagesize = "15";
                inputContactQuery.ListOfContact.Contact.ListOfCustomObject11.startrownum = "0";
                inputContactQuery.ListOfContact.Contact.ListOfCustomObject11.CustomObject11 = new WSOD_Contact_v2.CustomObject11Query();
                inputContactQuery.ListOfContact.Contact.ListOfCustomObject11.CustomObject11.Type = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ListOfCustomObject11.CustomObject11.Name = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ListOfCustomObject11.CustomObject11.Id = new WSOD_Contact_v2.queryType();

                inputContactQuery.ListOfContact.Contact.searchspec = "[IndexedShortText1] ='" + pMembershipId + "'";

                if (pSession == null)
                {
                    pSession = EnterLoginCredentials();
                }

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();
                outputContactQuery = srvcContact.ContactQueryPage(inputContactQuery);

                if (outputContactQuery != null)
                {
                    if (outputContactQuery.ListOfContact.Contact != null)
                    {
                        if (outputContactQuery.ListOfContact.Contact.Length > 0)
                        {
                            ContactV1 csContact = new ContactV1();

                            csContact.ContactId = outputContactQuery.ListOfContact.Contact[0].Id;
                            csContact.ContactFirstName = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].ContactFirstName);
                            csContact.ContactLastName = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].ContactLastName);
                            csContact.ContactEmailAddress = outputContactQuery.ListOfContact.Contact[0].ContactEmail;
                            csContact.ContactMembershipId = outputContactQuery.ListOfContact.Contact[0].IndexedShortText1;
                            csContact.ContactLanguage = outputContactQuery.ListOfContact.Contact[0].plSpoken_Language;
                            csContact.ContactMobileDevice = outputContactQuery.ListOfContact.Contact[0].plMobile_Device;
                            csContact.ContactHasMobileAccessOutsideOffice = outputContactQuery.ListOfContact.Contact[0].stHas_Mobile_Data_Connection_Outside_Office;
                            csContact.BusinessName = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].ltPartnerPlus_Business_Name);
                            csContact.ContactYearsOfHVACExp = outputContactQuery.ListOfContact.Contact[0].plPartnerPlus_Years_of_Experience_in_HVACR_Industry;
                            csContact.ContactJobTitle = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].JobTitle);
                            csContact.ContactCellularPhone = outputContactQuery.ListOfContact.Contact[0].CellularPhone;
                            csContact.ContactType = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].ContactType);

                            if (outputContactQuery.ListOfContact.Contact[0].AlternateCountry == "Korea")
                            {
                                csContact.ContactCountry = "South Korea";
                            }
                            else if (outputContactQuery.ListOfContact.Contact[0].AlternateCountry == "Congo, Democratic Republic of")
                            {
                                csContact.ContactCountry = "Democratic Republic of the Congo";
                            }
                            else if (outputContactQuery.ListOfContact.Contact[0].AlternateCountry == "Libyan Arab Jamahiriya")
                            {
                                csContact.ContactCountry = "Libya";
                            }
                            else if (outputContactQuery.ListOfContact.Contact[0].AlternateCountry == "Tanzania, United Republic of")
                            {
                                csContact.ContactCountry = "Tanzania";
                            }
                            else
                            {
                                csContact.ContactCountry = outputContactQuery.ListOfContact.Contact[0].AlternateCountry;
                            }

                            csContact.ContactAccountId = outputContactQuery.ListOfContact.Contact[0].AccountId;

                            if ((outputContactQuery.ListOfContact.Contact[0].ltPartnerPlus_Business_Name != null) || (outputContactQuery.ListOfContact.Contact[0].ltPartnerPlus_Business_Name == ""))
                            {
                                csContact.ContactAccountName = outputContactQuery.ListOfContact.Contact[0].ltPartnerPlus_Business_Name;
                            }
                            else
                            {
                                csContact.ContactAccountName = outputContactQuery.ListOfContact.Contact[0].AccountName;
                            }

                            csContact.ContactAccountLocation = outputContactQuery.ListOfContact.Contact[0].AccountLocation;

                            csContact.ContactAddress1 = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].AlternateAddress1);
                            csContact.ContactAddress2 = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].AlternateAddress2);
                            csContact.ContactAddress3 = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].AlternateAddress3);
                            csContact.ContactCity = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].AlternateCity);

                            csContact.JobFunction = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].IndexedPick1);
                            csContact.JobSpecificRole = transformSpecialCharacters(outputContactQuery.ListOfContact.Contact[0].IndexedPick2);

                            ////Account Extension - Custom Object 10
                            //if (outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject10 != null)
                            //{
                            //    if (outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject10.CustomObject10.Length > 0)
                            //    {
                            //        for (int idxAcctExt = 0; idxAcctExt < outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject10.CustomObject10.Length; idxAcctExt++)
                            //        {
                            //            if (outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject10.CustomObject10[idxAcctExt].Type == "PartnerPlus Customer Industry")
                            //            {
                            //                vCustomerIndustry = vCustomerIndustry + outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject10.CustomObject10[idxAcctExt].Name + ",";
                            //            }

                            //            if (outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject10.CustomObject10[idxAcctExt].Type == "PartnerPlus Company Service")
                            //            {
                            //                vCompanyService = vCompanyService + outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject10.CustomObject10[idxAcctExt].Name + ",";
                            //            }

                                        
                            //        }
                            //    }
                            //    else
                            //    {
                            //        vCustomerIndustry="";
                            //        vCompanyService="";
                            //    }
                            //}

                            //csContact.ContactListOfAccountBusinessIndustry = vCustomerIndustry;
                            //csContact.ContactListOfAccountServicesOffered = vCompanyService;
                   
                            ////Contact Extension - Custom Object 11
                            if (outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject11 != null)
                            {
                                if (outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject11.CustomObject11 != null)
                                {
                                    if (outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject11.CustomObject11.Length > 0)
                                    {
                                        for (int idxContExt = 0; idxContExt < outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject11.CustomObject11.Length; idxContExt++)
                                        {
                                            if (outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject11.CustomObject11[idxContExt].Type == "PartnerPlus Job Site Type")
                                            {
                                                vJobSiteType = vJobSiteType + outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject11.CustomObject11[idxContExt].Name + ",";
                                            }

                                            if (outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject11.CustomObject11[idxContExt].Type == "PartnerPlus Areas to Improve")
                                            {
                                                vKeyAreasToImprove = vKeyAreasToImprove + outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject11.CustomObject11[idxContExt].Name + ",";
                                            }

                                            if (outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject11.CustomObject11[idxContExt].Type == "PartnerPlus Primary Job Focus")
                                            {
                                                vPrimaryJobFocus = outputContactQuery.ListOfContact.Contact[0].ListOfCustomObject11.CustomObject11[idxContExt].Name;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        vJobSiteType = "";
                                        vKeyAreasToImprove = "";
                                        vPrimaryJobFocus = "";
                                    }
                                }
                                else
                                {
                                    vJobSiteType = "";
                                    vKeyAreasToImprove = "";
                                    vPrimaryJobFocus = "";
                                }
                                
                            }

                            //csContact.ContactListOfJobSiteType = transformSpecialCharacters(vJobSiteType);
                            csContact.ContactListOfKeyAreasToImprove = transformSpecialCharacters(vKeyAreasToImprove);
                            csContact.ContactPrimaryJobFocus = transformSpecialCharacters(vPrimaryJobFocus);

                            if (outputContactQuery.ListOfContact.Contact[0].AccountId == "" || outputContactQuery.ListOfContact.Contact[0].AccountId == null || outputContactQuery.ListOfContact.Contact[0].AccountId.IndexOf("No Match") >= 0)
                            {
                                vAccountId = "";
                            }
                            else
                            {
                                vAccountId = outputContactQuery.ListOfContact.Contact[0].AccountId;
                            }

                            csContact.ContactAccountId = vAccountId;

                            string[] sPartnerplusAccountExtensionDetails = getAccountExtensionDetails(pSession, outputContactQuery.ListOfContact.Contact[0].Id, vAccountId).Split('|');

                            if(sPartnerplusAccountExtensionDetails.Length>0)
                            {
                                if(sPartnerplusAccountExtensionDetails[0]!="")
                                {
                                    csContact.ContactListOfCompanyService = transformSpecialCharacters(sPartnerplusAccountExtensionDetails[0]);
                                }
                                else
                                {
                                    csContact.ContactListOfCompanyService="";
                                }

                                if (sPartnerplusAccountExtensionDetails[1] != "")
                                {
                                    csContact.ContactListOfCustomerIndustry = transformSpecialCharacters(sPartnerplusAccountExtensionDetails[1]);
                                }
                                else
                                {
                                    csContact.ContactListOfCustomerIndustry = "";
                                }
                            }
                            else
                            {
                                csContact.ContactListOfCustomerIndustry = "";
                                csContact.ContactListOfCompanyService="";
                            }
                           
                            arrPartnerplusContactDetails.Add(csContact);

                        }
                    }
 
                }

            }
            catch (Exception ex)
            {


                ContactV1 csContact = new ContactV1();
                csContact.ContactMembershipId = pMembershipId;
                csContact.ErrorMessage = ex.Message;

                arrPartnerplusContactDetails.Add(csContact);

                LogToFile("Error on getPartnerWebContactUserProfile_Enhanced_for_Profile_Builder (" + pMembershipId +") :" + ex.Message);

            }

            return arrPartnerplusContactDetails;
        }

        /* 20160725 - RDVillamor
         * Get Account Extension Details associated to the Contact
         */
        public String getAccountExtensionDetails(OnDemandSession pSession, String pContactId, String pAccountId)
        {
            String result = "";
            String vCompanyService="";
            String vCustomerIndustry="";

            WSOD_AccountExtension_v2.CustomObject10 srvcAccountExt = new WSOD_AccountExtension_v2.CustomObject10();
            WSOD_AccountExtension_v2.CustomObject10QueryPage_Input inputQuery = new WSOD_AccountExtension_v2.CustomObject10QueryPage_Input();
            WSOD_AccountExtension_v2.CustomObject10QueryPage_Output outputQuery = new WSOD_AccountExtension_v2.CustomObject10QueryPage_Output();

            try
            {
                inputQuery.ListOfCustomObject10 = new WSOD_AccountExtension_v2.ListOfCustomObject10Query();
                inputQuery.ListOfCustomObject10.CustomObject10 = new WSOD_AccountExtension_v2.CustomObject10Query();

                inputQuery.ListOfCustomObject10.pagesize = "30";
                inputQuery.ListOfCustomObject10.startrownum = "0";
                inputQuery.ViewMode = "Broadest";

                inputQuery.ListOfCustomObject10.CustomObject10.Id = new WSOD_AccountExtension_v2.queryType();
                inputQuery.ListOfCustomObject10.CustomObject10.Type = new WSOD_AccountExtension_v2.queryType();
                inputQuery.ListOfCustomObject10.CustomObject10.Name = new WSOD_AccountExtension_v2.queryType();
                inputQuery.ListOfCustomObject10.CustomObject10.ContactId = new WSOD_AccountExtension_v2.queryType();
                inputQuery.ListOfCustomObject10.CustomObject10.stoptimizedAttribute_01_Contact_Id = new WSOD_AccountExtension_v2.queryType();
                inputQuery.ListOfCustomObject10.CustomObject10.AccountId = new WSOD_AccountExtension_v2.queryType();

                if (pAccountId == "")
                {
                    inputQuery.ListOfCustomObject10.CustomObject10.searchspec = "[stoptimizedAttribute_01_Contact_Id]='" + pContactId + "' AND ([Type]='PartnerPlus Company Service' OR [Type]='PartnerPlus Customer Industry')";
                }
                else
                {
                    inputQuery.ListOfCustomObject10.CustomObject10.searchspec = "[AccountId]='" + pAccountId + "' AND ([Type]='PartnerPlus Company Service' OR [Type]='PartnerPlus Customer Industry')";
                }

                if (pSession == null)
                {
                    pSession = EnterLoginCredentials();
                }

                srvcAccountExt.Url = pSession.GetURL();
                srvcAccountExt.CookieContainer = pSession.GetCookieContainer();
                outputQuery = srvcAccountExt.CustomObject10QueryPage(inputQuery);

                if (outputQuery != null)
                {
                    if (outputQuery.ListOfCustomObject10.CustomObject10 != null)
                    {
                        if (outputQuery.ListOfCustomObject10.CustomObject10.Length > 0)
                        {
                            for (int idxAcctExt = 0; idxAcctExt < outputQuery.ListOfCustomObject10.CustomObject10.Length; idxAcctExt++)
                            {
                                if (outputQuery.ListOfCustomObject10.CustomObject10[idxAcctExt].Type == "PartnerPlus Company Service")
                                {
                                    vCompanyService = vCompanyService + outputQuery.ListOfCustomObject10.CustomObject10[idxAcctExt].Name + ",";
                                }

                                if (outputQuery.ListOfCustomObject10.CustomObject10[idxAcctExt].Type == "PartnerPlus Customer Industry")
                                {
                                    vCustomerIndustry = vCustomerIndustry + outputQuery.ListOfCustomObject10.CustomObject10[idxAcctExt].Name + ",";
                                }
                            }
                        }
                        else
                        {
                            vCompanyService = "";
                            vCustomerIndustry = "";
                        }

                    }
                    else
                    {
                        vCompanyService = "";
                        vCustomerIndustry = "";
                    }
                }
                else
                {
                    vCompanyService = "";
                    vCustomerIndustry = "";
                }

                result = vCompanyService + "|" + vCustomerIndustry;

            }
            catch (Exception ex)
            {
                LogToFile("Error on getAccountExtensionDetails: " + pContactId + ": " + ex.Message.ToString());
                result = "Error on getAccountExtensionDetails: " + pContactId + ": " + ex.Message.ToString();
            }

            return result;
        }

        public WSOD_Account_v1.AccountWS_AccountQueryPage_Output getAccountDetails(OnDemandSession pSession, String pAccountId)
        {
            ArrayList arrPartnerWebContactAccountDetails = new ArrayList();

            WSOD_Account_v1.Account srvcAccount = new WSOD_Account_v1.Account();
            WSOD_Account_v1.AccountWS_AccountQueryPage_Input inputAccount = new WSOD_Account_v1.AccountWS_AccountQueryPage_Input();
            WSOD_Account_v1.AccountWS_AccountQueryPage_Output outputAccount = new WSOD_Account_v1.AccountWS_AccountQueryPage_Output();

            try
            {
                inputAccount.PageSize = "1";
                inputAccount.StartRowNum = "0";
                inputAccount.UseChildAnd = "true";

                inputAccount.ListOfAccount = new WSOD_Account_v1.Account1[1];
                inputAccount.ListOfAccount[0] = new WSOD_Account_v1.Account1();

                inputAccount.ListOfAccount[0].AccountId = "='" + pAccountId + "'";
                inputAccount.ListOfAccount[0].AccountName = "";
                inputAccount.ListOfAccount[0].Owner = "";
                inputAccount.ListOfAccount[0].Location = "";
                inputAccount.ListOfAccount[0].AccountType = "";
                inputAccount.ListOfAccount[0].MainPhone = "";
                inputAccount.ListOfAccount[0].PrimaryShipToStreetAddress = "";
                inputAccount.ListOfAccount[0].PrimaryShipToStreetAddress2 = "";
                inputAccount.ListOfAccount[0].PrimaryShipToStreetAddress3 = "";
                inputAccount.ListOfAccount[0].PrimaryShipToCity = "";
                inputAccount.ListOfAccount[0].PrimaryShipToProvince = "";
                inputAccount.ListOfAccount[0].PrimaryShipToState = "";
                inputAccount.ListOfAccount[0].PrimaryShipToCountry = "";
                inputAccount.ListOfAccount[0].PrimaryShipToPostalCode="";
                inputAccount.ListOfAccount[0].bCurrent_Brand_Used_Emerson = "";
                inputAccount.ListOfAccount[0].stCurrent_Brand_Others = "";
                inputAccount.ListOfAccount[0].msplMarket = "";
                inputAccount.ListOfAccount[0].ltBusiness_Industry = "";
                inputAccount.ListOfAccount[0].ltServices_Offered = "";

                srvcAccount.Url = pSession.GetURL();
                srvcAccount.CookieContainer = pSession.GetCookieContainer();
                srvcAccount.Timeout = 120000;

                outputAccount = srvcAccount.AccountQueryPage(inputAccount);

            }
            catch(Exception ex)
            {
                outputAccount=null;
            }

            return outputAccount;
        }

        public String getAccountCustID(OnDemandSession pSession,String pAccountId)
        {
            String accountCustID = "";

            Int64 intCtrAccountResults = 0;

            WSOD_Account_v1.Account srvcAccount = new WSOD_Account_v1.Account();
            WSOD_Account_v1.AccountWS_AccountQueryPage_Input inputAccount = new WSOD_Account_v1.AccountWS_AccountQueryPage_Input();
            WSOD_Account_v1.AccountWS_AccountQueryPage_Output outputAccount = new WSOD_Account_v1.AccountWS_AccountQueryPage_Output();

            try
            {
                inputAccount.PageSize = "1";
                inputAccount.StartRowNum = "0";
                inputAccount.UseChildAnd = "true";

                inputAccount.ListOfAccount = new WSOD_Account_v1.Account1[1];
                inputAccount.ListOfAccount[0] = new WSOD_Account_v1.Account1();

                inputAccount.ListOfAccount[0].AccountId = "='" + pAccountId + "'";
                inputAccount.ListOfAccount[0].AccountName = "";
                inputAccount.ListOfAccount[0].Owner = "";
                
                //this field is equivalent to Account.CustID
                inputAccount.ListOfAccount[0].CustomObject2Name = "";

                srvcAccount.Url = pSession.GetURL();
                srvcAccount.CookieContainer = pSession.GetCookieContainer();
                srvcAccount.Timeout = 120000;

                outputAccount = srvcAccount.AccountQueryPage(inputAccount);

                intCtrAccountResults = outputAccount.ListOfAccount.Length;

                if (intCtrAccountResults ==1)
                {
                    accountCustID = outputAccount.ListOfAccount[0].CustomObject2Name;
                }
            }
            catch (Exception ex)
            {
                accountCustID = null;
                LogToFile("Error on getAccountCustID:" + pAccountId +":" + ex.Message);
            }
            
            return accountCustID;
        }


        public ArrayList uploadApprovedPartnerWebMembership(OnDemandSession pSession, String pMembershipId, String pFirstName, String pLastName, String pMiddleName, String pEmailAddress, String pContactCellular, String pContactCountry, String pAccountName, String pAccountType,String pAccountAddress, String pAccountCity, String pAccountStateProvince, String pAccountCountry, String pAccountZIP,String pAccountOption, String pAccountId, String pContactId, String pOptIn, String pDateOfReg, String pPUserAccountType, String pPRolesByCountry)
        {
            ArrayList arrPartnerWebContactDetails = new ArrayList();

            String vAccountId = "";
            String uploadingResult = "";
            String vAccountType = "";

            //20150908 - Added to handle South Korea as equivalent to 
            String vCountry = "";

            //20160321 - Added to derive the Group Member
            String vGroupMember = "";
            String vSourceOfUpdate = "Partner+ Asia";

            try
            {
                try
                {
                    ////20160118: commented since the way Account Type is used is already different. Will only use Contractor or Wholesaler as its values
                    //if (pAccountType == "End-User")
                    //{
                    //    vAccountType = "End User";
                    //}
                    //else
                    //{
                    //    vAccountType = pAccountType;
                    //}

                    if (pAccountType.IndexOf("Contractor") >= 0)
                    {
                        vAccountType = "Contractor";
                    }
                    else if (pAccountType.IndexOf("Wholesaler") >= 0)
                    {
                        vAccountType = "Wholesaler";
                    }

                    if ((pContactCountry == "South Korea") || (pAccountCountry == "South Korea"))
                    {
                        vCountry = "Korea";
                    }
                    else
                    {
                        vCountry = pContactCountry;
                    }

                    ////20160414: hidden on the assumption that MEA will use a different method already
                    //if ((vCountry == "United Arab Emirates") || (vCountry == "Egypt"))
                    //{
                    //    vGroupMember = "MEA";
                    //}
                    //else
                    //{
                    //    vGroupMember = "Asia";
                    //}

                    vGroupMember = "Asia";

                    if (pAccountOption == "2")
                    {
                        //Move to Another Account
                        if (pAccountId == "")
                        {
                            vAccountId = createNewPartnerplusAccount(pSession, pAccountName, vAccountType, pAccountAddress, pAccountStateProvince, pAccountCity, vCountry, pAccountZIP,vGroupMember);
                        }
                        else
                        {
                            vAccountId = pAccountId;
                        }

                        if (pContactId == "")
                        {
                            uploadingResult = createNewPartnerplusContact(pSession, pMembershipId, vAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellular, pAccountAddress, pAccountCity, pAccountStateProvince, vCountry, pAccountZIP, vAccountId, pOptIn,pDateOfReg,pPUserAccountType,pPRolesByCountry,vGroupMember);
                        }
                        else
                        {
                            uploadingResult = updateExistingPartnerplusContact(pSession, pMembershipId, vAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellular, pAccountAddress, pAccountCity, pAccountStateProvince, vCountry, pAccountZIP, vAccountId, pContactId, pOptIn, pDateOfReg, pPUserAccountType, pPRolesByCountry,vSourceOfUpdate);
                        }
                    }
                    else if (pAccountOption == "1")
                    {
                        ////20160118: commented since the way Account Type is used is already different. Will only use Contractor or Wholesaler as its values
                        //if (pAccountType == "End-User")
                        //{
                        //    vAccountType = "End User";
                        //}
                        //else
                        //{
                        //    vAccountType = pAccountType;
                        //}

                        if (pAccountType.IndexOf("Contractor") >= 0)
                        {
                            vAccountType = "Contractor";
                        }
                        else if (pAccountType.IndexOf("Wholesaler") >= 0)
                        {
                            vAccountType = "Wholesaler";
                        }

                        //Create New Account Association
                        if (pAccountId == "")
                        {
                            vAccountId = createNewPartnerplusAccount(pSession, pAccountName, vAccountType, pAccountAddress, pAccountStateProvince, pAccountCity, vCountry, pAccountZIP,vGroupMember);

                        }
                        else
                        {
                            vAccountId = pAccountId;
                        }

                        if(pContactId=="")
                        {
                            uploadingResult = createNewPartnerplusContact(pSession, pMembershipId, vAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellular, pAccountAddress, pAccountCity, pAccountStateProvince, vCountry, pAccountZIP, vAccountId, pOptIn, pDateOfReg,pPUserAccountType,pPRolesByCountry,vGroupMember);
                        }
                        else
                        {
                            //only add the ass
                            uploadingResult = updatePartnerplusContactCreateNewAccountAssociation(pSession, pMembershipId, vAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellular, pAccountAddress, pAccountCity, pAccountStateProvince, vCountry, pAccountZIP, vAccountId, pContactId, pOptIn, pDateOfReg, pPUserAccountType, pPRolesByCountry, vSourceOfUpdate);
                        }
                        
                    }
                    else if (pAccountOption == "3")
                    {
                        ////20160118: commented since the way Account Type is used is already different. Will only use Contractor or Wholesaler as its values
                        //if (pAccountType == "End-User")
                        //{
                        //    vAccountType = "End User";
                        //}
                        //else
                        //{
                        //    vAccountType = pAccountType;
                        //}

                        if (pAccountType.IndexOf("Contractor") >= 0)
                        {
                            vAccountType = "Contractor";
                        }
                        else if (pAccountType.IndexOf("Wholesaler") >= 0)
                        {
                            vAccountType = "Wholesaler";
                        }

                        vAccountId = pAccountId;
                        uploadingResult = updatePartnerplusContactRetainExistingAccountAssociation(pSession, pMembershipId, vAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellular, pAccountAddress, pAccountCity, pAccountStateProvince, vCountry, pAccountZIP, pContactId, pOptIn,pDateOfReg,pPUserAccountType,pPRolesByCountry, vSourceOfUpdate);
                    }

                   

                }
                catch (Exception ex)
                {
                    vAccountId = "";
                    LogToFile("Error on uploadApprovedPartnerWebMembership:" + pMembershipId + ":" + ex.Message);
                }


                if (uploadingResult != "")
                {
                    ContactV1 csContact = new ContactV1();

                    csContact.ContactId = uploadingResult.ToString();

                    arrPartnerWebContactDetails.Add(csContact);
                }

            }
            catch (Exception ex)
            {
                arrPartnerWebContactDetails = null;
                LogToFile("Error on uploadApprovedPartnerWebMembership:" + pMembershipId + ":" + ex.Message);
            }

            return arrPartnerWebContactDetails;
        }

        /*
         * Created on/by: 20160708/Rochelle Villamor
         * 
         * to handle registration process of MEA
         */
        public ArrayList uploadApprovedPartnerWebMembership_MEA(OnDemandSession pSession, String pMembershipId, String pFirstName, String pLastName, String pMiddleName, String pEmailAddress, String pContactCellular, String pContactCountry, String pAccountName, String pAccountType, String pAccountAddress, String pAccountCity, String pAccountStateProvince, String pAccountCountry, String pAccountZIP, String pAccountOption, String pAccountId, String pContactId, String pOptIn, String pDateOfReg, String pPUserAccountType, String pPRolesByCountry)
        {
            ArrayList arrPartnerWebContactDetails = new ArrayList();

            String vAccountId = "";
            String uploadingResult = "";
            String vAccountType = "";

            //20150908 - Added to handle South Korea as equivalent to 
            String vCountry = "";

            //20160321 - Added to derive the Group Member
            String vGroupMember = "";
            String vSourceOfUpdate = "Partner+ MEA";

            try
            {
                try
                {
                    ////20160118: commented since the way Account Type is used is already different. Will only use Contractor or Wholesaler as its values
                    //if (pAccountType == "End-User")
                    //{
                    //    vAccountType = "End User";
                    //}
                    //else
                    //{
                    //    vAccountType = pAccountType;
                    //}

                    if (pAccountType.IndexOf("Contractor") >= 0)
                    {
                        vAccountType = "Contractor";
                    }
                    else if (pAccountType.IndexOf("Wholesaler") >= 0)
                    {
                        vAccountType = "Wholesaler";
                    }

                    if ((pAccountCountry == "Guinea-Bissau") || (pContactCountry== "Guinea-Bissau"))
                    {
                        vCountry = "Guinea-Bissau";
                    }
                    else if ((pAccountCountry == "Democratic Republic of the Congo") || (pContactCountry == "Democratic Republic of the Congo"))
                    {
                        vCountry = "Congo, Democratic Republic of";
                    }
                    else if ((pAccountCountry == "Libya") || (pContactCountry == "Libya"))
                    {
                        vCountry = "Libyan Arab Jamahiriya";
                    }
                    else if ((pAccountCountry == "Tanzania") || (pContactCountry == "Tanzania"))
                    {
                        vCountry = "Tanzania, United Republic of";
                    }
                    else
                    {
                        vCountry = pContactCountry;
                    }

                    ////20160414: commented since this method is expected to be used by MEA group only
                    //if ((vCountry == "United Arab Emirates") || (vCountry == "Egypt"))
                    //{
                    //    vGroupMember = "MEA";
                    //}
                    //else
                    //{
                    //    vGroupMember = "Asia";
                    //}

                    vGroupMember = "MEA";

                    if (pAccountOption == "2")
                    {
                        //Move to Another Account
                        if (pAccountId == "")
                        {
                            vAccountId = createNewPartnerplusAccount(pSession, pAccountName, vAccountType, pAccountAddress, pAccountStateProvince, pAccountCity, vCountry, pAccountZIP,vGroupMember);
                        }
                        else
                        {
                            vAccountId = pAccountId;
                        }

                        if (pContactId == "")
                        {
                            uploadingResult = createNewPartnerplusContact(pSession, pMembershipId, vAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellular, pAccountAddress, pAccountCity, pAccountStateProvince, vCountry, pAccountZIP, vAccountId, pOptIn, pDateOfReg, pPUserAccountType, pPRolesByCountry,vGroupMember);
                        }
                        else
                        {
                            uploadingResult = updateExistingPartnerplusContact(pSession, pMembershipId, vAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellular, pAccountAddress, pAccountCity, pAccountStateProvince, vCountry, pAccountZIP, vAccountId, pContactId, pOptIn, pDateOfReg, pPUserAccountType, pPRolesByCountry,vSourceOfUpdate);
                        }
                    }
                    else if (pAccountOption == "1")
                    {
                        ////20160118: commented since the way Account Type is used is already different. Will only use Contractor or Wholesaler as its values
                        //if (pAccountType == "End-User")
                        //{
                        //    vAccountType = "End User";
                        //}
                        //else
                        //{
                        //    vAccountType = pAccountType;
                        //}

                        if (pAccountType.IndexOf("Contractor") >= 0)
                        {
                            vAccountType = "Contractor";
                        }
                        else if (pAccountType.IndexOf("Wholesaler") >= 0)
                        {
                            vAccountType = "Wholesaler";
                        }

                        //Create New Account Association
                        if (pAccountId == "")
                        {
                            vAccountId = createNewPartnerplusAccount(pSession, pAccountName, vAccountType, pAccountAddress, pAccountStateProvince, pAccountCity, vCountry, pAccountZIP,vGroupMember);
                        }
                        else
                        {
                            vAccountId = pAccountId;
                        }

                        if (pContactId == "")
                        {
                            uploadingResult = createNewPartnerplusContact(pSession, pMembershipId, vAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellular, pAccountAddress, pAccountCity, pAccountStateProvince, vCountry, pAccountZIP, vAccountId, pOptIn, pDateOfReg, pPUserAccountType, pPRolesByCountry,vGroupMember);
                        }
                        else
                        {
                            //only add the ass
                            uploadingResult = updatePartnerplusContactCreateNewAccountAssociation(pSession, pMembershipId, vAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellular, pAccountAddress, pAccountCity, pAccountStateProvince, vCountry, pAccountZIP, vAccountId, pContactId, pOptIn, pDateOfReg, pPUserAccountType, pPRolesByCountry, vSourceOfUpdate);
                        }

                    }
                    else if (pAccountOption == "3")
                    {
                        ////20160118: commented since the way Account Type is used is already different. Will only use Contractor or Wholesaler as its values
                        //if (pAccountType == "End-User")
                        //{
                        //    vAccountType = "End User";
                        //}
                        //else
                        //{
                        //    vAccountType = pAccountType;
                        //}

                        if (pAccountType.IndexOf("Contractor") >= 0)
                        {
                            vAccountType = "Contractor";
                        }
                        else if (pAccountType.IndexOf("Wholesaler") >= 0)
                        {
                            vAccountType = "Wholesaler";
                        }

                        vAccountId = pAccountId;
                        uploadingResult = updatePartnerplusContactRetainExistingAccountAssociation(pSession, pMembershipId, vAccountType, pFirstName, pMiddleName, pLastName, pEmailAddress, pContactCellular, pAccountAddress, pAccountCity, pAccountStateProvince, vCountry, pAccountZIP, pContactId, pOptIn, pDateOfReg, pPUserAccountType, pPRolesByCountry, vSourceOfUpdate);
                    }



                }
                catch (Exception ex)
                {
                    vAccountId = "";
                    LogToFile("Error on uploadApprovedPartnerWebMembership_MEA:" + pMembershipId + ":" + ex.Message);
                }


                if (uploadingResult != "")
                {
                    ContactV1 csContact = new ContactV1();

                    csContact.ContactId = uploadingResult.ToString();

                    arrPartnerWebContactDetails.Add(csContact);
                }

            }
            catch (Exception ex)
            {
                arrPartnerWebContactDetails = null;
                LogToFile("Error on uploadApprovedPartnerWebMembership_MEA:" + pMembershipId + ":" + ex.Message);
            }

            return arrPartnerWebContactDetails;
        }

        public String createNewPartnerplusAccount(OnDemandSession pSession,String pAccountName,String pAccountType,String pAccountLocation, String pAStateProvince, String pACity, String pACountry, String pAPostalCode, String pGroupMember)
        {
            String accountId ="";
            
            String accountType;
            String accountOwner = ConfigurationManager.AppSettings["defaultOwner"].ToString();
            
            WSOD_Account_v1.Account srvcAccount = new WSOD_Account_v1.Account();
            WSOD_Account_v1.AccountWS_AccountInsert_Input inputAccount = new WSOD_Account_v1.AccountWS_AccountInsert_Input();
            WSOD_Account_v1.AccountWS_AccountInsert_Output outputAccount = new WSOD_Account_v1.AccountWS_AccountInsert_Output();

            try
            {

                if (pAccountType == "Wholesaler")
                {
                    accountType = "Wholesaler";
                }
                else
                {
                    accountType = "Contractor";
                }

                WSOD_Account_v1.Account1[] objListOfAccount = new WSOD_Account_v1.Account1[1];
                objListOfAccount[0] = new WSOD_Account_v1.Account1();

                objListOfAccount[0].AccountName = pAccountName;
                objListOfAccount[0].AccountType = accountType;
                objListOfAccount[0].Location = pAccountLocation;
                
                //20160321: RDVillamor, use pGroupMember to pass the Group Member field
                objListOfAccount[0].IndexedPick0 = pGroupMember; //"Asia";
                objListOfAccount[0].OwnerFullName = accountOwner;
                objListOfAccount[0].IndexedBoolean0 = "Y";

                objListOfAccount[0].PrimaryShipToStreetAddress = pAccountLocation;
                objListOfAccount[0].PrimaryShipToCity = pACity;
                objListOfAccount[0].PrimaryShipToProvince = pAStateProvince;
                objListOfAccount[0].PrimaryShipToCountry = pACountry;
                objListOfAccount[0].PrimaryShipToPostalCode = pAPostalCode;

                objListOfAccount[0].PrimaryBillToStreetAddress = pAccountLocation;
                objListOfAccount[0].PrimaryBillToCity = pACity;
                objListOfAccount[0].PrimaryBillToProvince = pAStateProvince;
                objListOfAccount[0].PrimaryBillToCountry = pACountry;
                objListOfAccount[0].PrimaryBillToPostalCode = pAPostalCode;


                inputAccount.ListOfAccount = objListOfAccount;

                srvcAccount.Url = pSession.GetURL();
                outputAccount = srvcAccount.AccountInsert(inputAccount);

                accountId = outputAccount.ListOfAccount[0].AccountId;
            }
            catch (Exception ex)
            {
                accountId = null;
                LogToFile("Error on createNewPartnerplusAccount:" + pAccountName + ":" + ex.Message);
            }

            return accountId;
        }

        public String createNewPartnerplusContact(OnDemandSession pSession, String pMembershipId, String pContactType, String pFirstName, String pMiddleName, String pLastName, String pEmailAddress, String pCellularPhone, String pAddress, String pCity, String pStateProvince, String pCountry, String pPostalCode, String pAccountId, String pCIsOptIn, String pDateOfReg, String pUserAccountType, String pRolesByCountry, String pGroupMember)
        {
            String contactResult = "";
            String vContactType = "";
            String isOptOut = "";
            String contactOwner = ConfigurationManager.AppSettings["defaultOwner"].ToString();

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactInsert_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactInsert_Input();
            WSOD_Contact_v1.ContactWS_ContactInsert_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactInsert_Output();

            try
            {

                if (pCIsOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                if (pContactType == "End-User")
                {
                    vContactType = "End User";
                }
                else
                {
                    vContactType = pContactType;
                }


                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactFirstName = pFirstName;
                objListOfContact[0].ContactLastName = pLastName;
                objListOfContact[0].MiddleName = pMiddleName;
                objListOfContact[0].ContactEmail = pEmailAddress;
                objListOfContact[0].ContactFullName = pFirstName + " " + pLastName;
                objListOfContact[0].ContactType = vContactType;

                //20160321: RDVillamor, use pGroupMember to pass the Group Member field
                objListOfContact[0].IndexedPick0 = pGroupMember; //"Asia";

                objListOfContact[0].IndexedShortText1 = pMembershipId;
                objListOfContact[0].OwnerFullName = contactOwner;
                objListOfContact[0].AccountId = pAccountId;
                objListOfContact[0].CellularPhone = pCellularPhone;
                objListOfContact[0].bNever_SMS = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].stPartner_Campaign_Opt_Out_YN = isOptOut;
                
                //insert address of account
                objListOfContact[0].AlternateAddress1 = pAddress;
                objListOfContact[0].AlternateCity = pCity;
                objListOfContact[0].AlternateProvince = pStateProvince;
                objListOfContact[0].AlternateCountry = pCountry;
                objListOfContact[0].AlternateZipCode = pPostalCode;

                objListOfContact[0].plMembership_Status = "Member";

                //String pDateOfReg
                objListOfContact[0].dMembership_Validity = pDateOfReg.ToString();

                //20160118: added to save additional fields: CRM.PartnerPlusUserAccountType = UserApproval.AccountType, CRM.ContactType=UserApproval.RolesByCountry
                objListOfContact[0].plPartnerPlus_User_Type = pUserAccountType;
                objListOfContact[0].stPartnerPlus_Roles_By_Country = pRolesByCountry;

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactInsert(inputContact);

                contactResult = outputContact.ListOfContact[0].ContactId;

            }
            catch (Exception ex)
            {
                contactResult="Error";
                LogToFile("Error on createNewPartnerplusContact:" + pMembershipId + ":" + ex.Message);
            }

            return contactResult;
        }

        public String updateExistingPartnerplusContact(OnDemandSession pSession, String pMembershipId, String pContactType, String pFirstName, String pMiddleName, String pLastName, String pEmailAddress, String pCellularPhone, String pAddress, String pCity, String pStateProvince, String pCountry, String pPostalCode, String pAccountId, String pContactId, String pCIsOptIn, String pDateOfReg, String pUserAccountType, String pRolesByCountry, String pSourceOfUpdate)
        {
            String contactResult = "";
            String vContactType = "";
            String isOptOut = "";

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Output();

            try
            {
                if (pCIsOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                if (pContactType == "End-User")
                {
                    vContactType = "End User";
                }
                else
                {
                    vContactType = pContactType;
                }


                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactFirstName = pFirstName;
                objListOfContact[0].ContactLastName = pLastName;
                objListOfContact[0].MiddleName = pMiddleName;
                objListOfContact[0].ContactEmail = pEmailAddress;
                objListOfContact[0].ContactFullName = pFirstName + " " + pLastName;
                objListOfContact[0].ContactType = vContactType;
                objListOfContact[0].IndexedShortText1 = pMembershipId;
                objListOfContact[0].AccountId = pAccountId;
                objListOfContact[0].CellularPhone = pCellularPhone;
                objListOfContact[0].ContactId = pContactId;

                //insert address of account
                objListOfContact[0].AlternateAddress1 = pAddress;
                objListOfContact[0].AlternateCity = pCity;
                objListOfContact[0].AlternateProvince = pStateProvince;
                objListOfContact[0].AlternateCountry = pCountry;
                objListOfContact[0].AlternateZipCode = pPostalCode;
                objListOfContact[0].bNever_SMS = isOptOut;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                objListOfContact[0].stPartner_Campaign_Opt_Out_YN = isOptOut;

                //20151006 - added to fix the bug that date of registration was not included;
                objListOfContact[0].dMembership_Validity = pDateOfReg.ToString();

                //20160118: added to save additional fields: CRM.PartnerPlusUserAccountType = UserApproval.AccountType, CRM.ContactType=UserApproval.RolesByCountry
                objListOfContact[0].plPartnerPlus_User_Type = pUserAccountType;
                objListOfContact[0].stPartnerPlus_Roles_By_Country = pRolesByCountry;

                //20160422: added to include the source of update: parameter=pSourceOfUpdate, expected values:Partner+ Asia, Partner+ MEA 
                objListOfContact[0].stSource_of_Update = pSourceOfUpdate;

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactUpdate(inputContact);

                contactResult = outputContact.ListOfContact[0].ContactId;

            }
            catch (Exception ex)
            {
                contactResult = "Error";
                LogToFile("Error on updateExistingPartnerplusContact:" + pMembershipId + ":" + ex.Message);
            }


            return contactResult;
        }

        public String updatePartnerplusContactRetainExistingAccountAssociation(OnDemandSession pSession, String pMembershipId, String pContactType, String pFirstName, String pMiddleName, String pLastName, String pEmailAddress, String pCellularPhone, String pAddress, String pCity, String pStateProvince, String pCountry, String pPostalCode, String pContactId, String pCIsOptIn, String pDateOfReg, String pUserAccountType, String pRolesByCountry, String pSourceOfUpdate)
        {
            String contactResult = "";
            String vContactType = "";
            String isOptOut = "";
            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Output();



            try
            {
                if (pCIsOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                if (pContactType == "End-User")
                {
                    vContactType = "End User";
                }
                else
                {
                    vContactType = pContactType;
                }


                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactFirstName = pFirstName;
                objListOfContact[0].ContactLastName = pLastName;
                objListOfContact[0].MiddleName = pMiddleName;
                objListOfContact[0].ContactEmail = pEmailAddress;
                objListOfContact[0].ContactFullName = pFirstName + " " + pLastName;
                objListOfContact[0].ContactType = vContactType;
                objListOfContact[0].IndexedShortText1 = pMembershipId;
                objListOfContact[0].CellularPhone = pCellularPhone;
                objListOfContact[0].ContactId = pContactId;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].bNever_SMS = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                objListOfContact[0].stPartner_Campaign_Opt_Out_YN = isOptOut;

                //insert address of account
                objListOfContact[0].AlternateAddress1 = pAddress;
                objListOfContact[0].AlternateCity = pCity;
                objListOfContact[0].AlternateProvince = pStateProvince;
                objListOfContact[0].AlternateCountry = pCountry;
                objListOfContact[0].AlternateZipCode = pPostalCode;

                objListOfContact[0].plMembership_Status = "Member";

                //20151006 - added to fix the bug that date of registration was not included;
                objListOfContact[0].dMembership_Validity = Convert.ToDateTime(pDateOfReg).ToString("MM/dd/yyyy");

                //20160118: added to save additional fields: CRM.PartnerPlusUserAccountType = UserApproval.AccountType, CRM.ContactType=UserApproval.RolesByCountry
                objListOfContact[0].plPartnerPlus_User_Type = pUserAccountType;
                objListOfContact[0].stPartnerPlus_Roles_By_Country = pRolesByCountry;

                //20161422: added to log the source of update page
                objListOfContact[0].stSource_of_Update = pSourceOfUpdate;

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactUpdate(inputContact);

                contactResult = outputContact.ListOfContact[0].ContactId;


            }
            catch (Exception ex)
            {
                contactResult = "Error";
                LogToFile("Error on updatePartnerplusContactRetainExistingAccountAssociation:" + pMembershipId + ":" + ex.Message);
            }


            return contactResult;
        }


        public String updatePartnerplusContactCreateNewAccountAssociation(OnDemandSession pSession, String pMembershipId, String pContactType, String pFirstName, String pMiddleName, String pLastName, String pEmailAddress, String pCellularPhone, String pAddress, String pCity, String pStateProvince, String pCountry, String pPostalCode, String pAccountId, String pContactId, String pCIsOptIn, String pDateOfReg, String pUserAccountType, String pRolesByCountry, String pSourceofUpdate)
        {
            String contactResult = "";
            String vContactType = "";
            String isOptOut = "";

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Output();

            WSOD_Contact_v1.ContactWS_ContactInsertChild_Input inputContactChild = new WSOD_Contact_v1.ContactWS_ContactInsertChild_Input();
            WSOD_Contact_v1.ContactWS_ContactInsertChild_Output outputContactChild = new WSOD_Contact_v1.ContactWS_ContactInsertChild_Output();

            try
            {
                if (pCIsOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                if (pContactType == "End-User")
                {
                    vContactType = "End User";
                }
                else
                {
                    vContactType = pContactType;
                }

                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactFirstName = pFirstName;
                objListOfContact[0].ContactLastName = pLastName;
                objListOfContact[0].MiddleName = pMiddleName;
                objListOfContact[0].ContactEmail = pEmailAddress;
                objListOfContact[0].ContactFullName = pFirstName + " " + pLastName;
                objListOfContact[0].ContactType = vContactType;
                objListOfContact[0].IndexedShortText1 = pMembershipId;
                objListOfContact[0].CellularPhone = pCellularPhone;
                objListOfContact[0].ContactId = pContactId;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                objListOfContact[0].bNever_SMS = isOptOut;
                objListOfContact[0].stPartner_Campaign_Opt_Out_YN = isOptOut;

                //insert address of account
                objListOfContact[0].AlternateAddress1 = pAddress;
                objListOfContact[0].AlternateCity = pCity;
                objListOfContact[0].AlternateProvince = pStateProvince;
                objListOfContact[0].AlternateCountry = pCountry;
                objListOfContact[0].AlternateZipCode = pPostalCode;

                objListOfContact[0].plMembership_Status = "Member";

                //20151006 - added to fix the bug that date of registration was not included;
                objListOfContact[0].dMembership_Validity = pDateOfReg.ToString();

                //20160118: added to save additional fields: CRM.PartnerPlusUserAccountType = UserApproval.AccountType, CRM.ContactType=UserApproval.RolesByCountry
                objListOfContact[0].plPartnerPlus_User_Type = pUserAccountType;
                objListOfContact[0].stPartnerPlus_Roles_By_Country = pRolesByCountry;

                ////20160422: added to replace the primary account with the new once
               
                //objListOfContact[0].AccountId = pAccountId;

                ////20160422: added to indicate that change was triggered by update on a the page (parameter: pSourceofUpdate / expected values: Partner+ MEA, Partner+ Asia
                objListOfContact[0].stSource_of_Update = pSourceofUpdate;

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactUpdate(inputContact);

                contactResult = outputContact.ListOfContact[0].ContactId;

                if (contactResult != null)
                {
                    WSOD_Contact_v1.Contact1[] objListOfContactAccount = new WSOD_Contact_v1.Contact1[1];
                    objListOfContactAccount[0] = new WSOD_Contact_v1.Contact1();
                    objListOfContactAccount[0].ListOfAccount = new WSOD_Contact_v1.Account[1];
                    objListOfContactAccount[0].ListOfAccount[0] = new WSOD_Contact_v1.Account();

                    objListOfContactAccount[0].ContactId = pContactId;

                    objListOfContactAccount[0].ListOfAccount[0].AccountId = pAccountId;

                    inputContactChild.ListOfContact = objListOfContactAccount;

                    srvcContact.Url = pSession.GetURL();
                    
                    //20160422: added this missing item on the code. This is a bug
                    srvcContact.CookieContainer = pSession.GetCookieContainer();

                    outputContactChild = srvcContact.ContactInsertChild(inputContactChild);


                }


            }
            catch (Exception ex)
            {
                contactResult = "Error";
                LogToFile("Error on updatePartnerplusContactCreateNewAccountAssociation:" + pMembershipId + ":" + ex.Message);
            }


            return contactResult;
        }

        //20160613 - RDVillamor, removed String pAccountId between pMembershipId and pAccountName
        /*
         * 20160617, added Key Interest - Others field on the values flowed-in to CRM
         */
        public String updatePartnerplusContactUserProfile(OnDemandSession pSession,String pMembershipId,String pAccountName,String pContactId,String pAccountType,String pFirstName, String pMiddleName, String pLastName, String pEmailAddress, String pCellularPhone, String pAddress, String pCity, String pStateProvince, String pCountry, String pPostalCode, String pJobTitle, String pCYearsOfHVAC, String pCIsCompressor, String pCIsCondensingUnits, String pCIsElectronicControllers, String pCIsLineComponents, String pACurrentBrandUsageIsEmerson, String pACurrentBrandUsageIsOthers, String pCScopeOfWork, String pABusinessIndustry, String pCMarket, String pAServicesOffered, String pCPermissionMarketingTarget, String pCPartnerwebComments, String pCIsOptIn, String pReferredBy, String pAMainPhone)
        {
            String contactResult = "";
            String vContactType = "";
            String isOptOut = "";
            String vCountry = "";

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            
            WSOD_Contact_v1.ContactWS_ContactUpdate_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Output();

            WSOD_Contact_v1.ContactWS_ContactUpdateChild_Input inputContactChild = new WSOD_Contact_v1.ContactWS_ContactUpdateChild_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdateChild_Output outputContactChild = new WSOD_Contact_v1.ContactWS_ContactUpdateChild_Output();

            //20160613 - RdVillamor, added to count the length of address
            int iAddressLen = 0;

            try
            {
                if (pCIsOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                if (pAccountType == "End-User")
                {
                    vContactType = "End User";
                }
                    //start:20160615 - RDVillamor, added to handle issue on saving contact type field
                else if (pAccountType == "0")
                {
                    vContactType = "";
                }
                else if (pAccountType == "Emerson Employee")
                {
                    vContactType = "Internal";
                }
                 //end:20160615 - RDVillamor, added to handle issue on saving contact type field
                else
                {
                    vContactType = pAccountType;
                }

                //20160708: RDVillamor ,Added More Transformed Country Names aside from Korea
                if (pCountry == "South Korea")
                {
                    vCountry = "Korea";
                }
                else if (pCountry == "Democratic Republic of the Congo")
                {
                    vCountry = "Congo, Democratic Republic of";
                }
                else if (pCountry == "Libya")
                {
                    vCountry = "Libyan Arab Jamahiriya";
                }
                else if (pCountry == "Tanzania")
                {
                    vCountry = "Tanzania, United Republic of";
                }
                else
                {
                    vCountry = pCountry;
                }
                
                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactId = pContactId;
                objListOfContact[0].ContactFirstName = pFirstName;
                objListOfContact[0].ContactLastName = pLastName;
                objListOfContact[0].MiddleName = pMiddleName;
                objListOfContact[0].ContactEmail = pEmailAddress;
                objListOfContact[0].ContactFullName = pFirstName + " " + pLastName;
                objListOfContact[0].ContactType = vContactType;
                objListOfContact[0].IndexedShortText1 = pMembershipId;
                objListOfContact[0].CellularPhone = "+" + pCellularPhone.Trim(); //pCellularPhone;

                //insert address of account
                iAddressLen = pAddress.Length;

                //start: 20160613 - RDVillamor, added to parse the characters of Address fields
                if (iAddressLen <= 40)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress;
                    objListOfContact[0].AlternateAddress2 = "";
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 40) && (iAddressLen <= 80))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40);
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 80) && (iAddressLen < 121))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80);
                }
                else if (iAddressLen > 120)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80, 40);
                }
                //end: 20160613 - RDVillamor, added to parse the characters of Address fields


                ////20160613 - RDVillamor, commeneted since this has been corrected
                //objListOfContact[0].AlternateAddress1 = pAddress;

                objListOfContact[0].AlternateCity = pCity;
                objListOfContact[0].AlternateProvince = pStateProvince;
                objListOfContact[0].AlternateCountry = vCountry;
                objListOfContact[0].AlternateZipCode = pPostalCode;

                objListOfContact[0].JobTitle = pJobTitle;
                objListOfContact[0].iYears_of_Experience_in_HVACR_industry = pCYearsOfHVAC;
                
                objListOfContact[0].bCompressor = pCIsCompressor;
                objListOfContact[0].bCondensing_Units = pCIsCondensingUnits;
                objListOfContact[0].bControllers = pCIsElectronicControllers;
                objListOfContact[0].bLine_Components = pCIsLineComponents;

                ////20160617 - RDVillamor, commented to just save the values on a ltPartnerPlus_Key_Interests field
                //objListOfContact[0].msplPermission_Marketing_Target = pCPermissionMarketingTarget;
                
                objListOfContact[0].ltPartnerWeb_Contact_Comments = pCPartnerwebComments;

                objListOfContact[0].stPartner_Campaign_Opt_Out_YN = isOptOut;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                objListOfContact[0].bNever_SMS = isOptOut;

                objListOfContact[0].ltReferred_by_Company = pReferredBy;
                objListOfContact[0].ltMarket = pCMarket;

                objListOfContact[0].ltScope_of_Work = pCScopeOfWork;

                ////start: 20160613 - RDVillamor, added to log all the answers given by the user on My Profile page on the Contact level. For now, all the details will be saved here and no value will flow-in to account.
                objListOfContact[0].bPartnerPlus_Current_Brand_Used_Emerson = pACurrentBrandUsageIsEmerson;
                objListOfContact[0].stPartnerPlus_Current_Brand_Usage_Others = pACurrentBrandUsageIsOthers;
                objListOfContact[0].ltPartnerPlus_Services_Offered = pAServicesOffered;
                objListOfContact[0].ltPartnerPlus_Business_Industry = pABusinessIndustry;
                objListOfContact[0].ltPartnerPlus_Business_Name = pAccountName;
                objListOfContact[0].WorkPhone = "+" + pAMainPhone.Trim();
                ////end: 20160613 - RDVillamor, added to log all the answers given by the user on My Profile page on the Contact level. For now, all the details will be saved here and no value will flow-in to account.

                //start: 20160617 - RDVillamor, added to just save the values on a ltPartnerPlus_Key_Interests field
                objListOfContact[0].ltPartnerPlus_Key_Interests = pCPermissionMarketingTarget;
                //objListOfContact[0].stPartnerPlus_Key_Interest_Others = pKeyInterestOthers;
                //end: 20160617 - RDVillamor, added to just save the values on a ltPartnerPlus_Key_Interests field

                inputContact.ListOfContact = objListOfContact;

                ////20160613 - RDVillamor, added to allow reconnection
                if (pSession == null)
                {
                   pSession = EnterLoginCredentials();
                }

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactUpdate(inputContact);

                ////20160613 - RDVillamor, commented to skip the update on the Account level. Instead, log the answers on the My Profile questions on the Contact Level fields
                //if (outputContact != null)
                //{
                //    contactResult = updateAccountDetailsInitiatedOnUserProfile(pSession, pAccountId, pAccountName, pAccountType, pCellularPhone, pACurrentBrandUsageIsEmerson, pACurrentBrandUsageIsOthers, pABusinessIndustry, pAServicesOffered, pAddress, pCity, pStateProvince, vCountry, pPostalCode, pAMainPhone);
                //}

                contactResult = outputContact.ListOfContact[0].ContactId;

            }
            catch (Exception ex)
            {
                contactResult = "Error on updatePartnerplusContactUserProfile:" + pMembershipId + ":" + ex.Message;
                LogToFile("Error on updatePartnerplusContactUserProfile:" + pMembershipId + ":" + ex.Message);
            }


            return contactResult;
        }

        /*
         * 20160720 - RDVillamor
         * Developed for the Profile Builder enhancement of Partner+
         * 
         * Changes:
         * 1. 20160809 - remove pABusinessIndustry as this field is already covered by pCJobSiteType
         */
        public String updatePartnerplusContactUserProfile_Enhanced(OnDemandSession pSession, String pMembershipId, String pContactId, String pAccountId,String pAccountType,String pAddress,String pCity, String pJobTitle, String pCYearsOfHVAC, String pAServicesOffered, String pCKeyAreasToImprove, String pCJobSiteType,String pCPrimaryJobFocus, String pLang, String pMobileDeviceType, String pHasMobileData, String pCMajorGroup, String pCJobSpecific)
        {
            String contactResult = "";
            String vContactType = "";
            String vAccountId = "";

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();

            WSOD_Contact_v1.ContactWS_ContactUpdate_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Output();

            WSOD_Contact_v1.ContactWS_ContactUpdateChild_Input inputContactChild = new WSOD_Contact_v1.ContactWS_ContactUpdateChild_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdateChild_Output outputContactChild = new WSOD_Contact_v1.ContactWS_ContactUpdateChild_Output();

            int iAddressLen = 0;

            try
            {

                if (pAccountType == "End-User")
                {
                    vContactType = "End User";
                }
                else if (pAccountType == "0")
                {
                    vContactType = "";
                }
                else if (pAccountType.IndexOf("Dealer") >= 0)
                {
                    vContactType = "Dealer";
                }
                else if (pAccountType.IndexOf("Wholesaler") >= 0)
                {
                    vContactType = "Wholesaler";
                }
                else if (pAccountType.IndexOf("OEM") >= 0)
                {
                    vContactType = "OEM";
                }
                else
                {
                    vContactType = pAccountType;
                }

                if (pAccountId == "No Match Row Id" || pAccountId == "" || pAccountId==null)
                {
                    vAccountId = "";
                }
                else
                {
                    vAccountId = pAccountId;
                }

                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactId = pContactId;
                //objListOfContact[0].ContactFirstName = pFirstName;
                //objListOfContact[0].ContactLastName = pLastName;
                //objListOfContact[0].ContactFullName = pFirstName + " " + pLastName;
                objListOfContact[0].ContactType = vContactType;
                objListOfContact[0].IndexedShortText1 = pMembershipId;
                objListOfContact[0].AlternateCity = transformSpecialCharacters(pCity);
                objListOfContact[0].JobTitle = transformSpecialCharacters(pJobTitle);
                objListOfContact[0].plPartnerPlus_Years_of_Experience_in_HVACR_Industry = pCYearsOfHVAC;
                objListOfContact[0].IndexedPick1 = transformSpecialCharacters(pCMajorGroup);
                objListOfContact[0].IndexedPick2 = transformSpecialCharacters(pCJobSpecific);
                objListOfContact[0].plSpoken_Language = pLang;
                
                //objListOfContact[0].ltPartnerPlus_Services_Offered = pAServicesOffered;
                //objListOfContact[0].ltPartnerPlus_Business_Industry = pABusinessIndustry;

                objListOfContact[0].plMobile_Device = pMobileDeviceType;
                objListOfContact[0].stHas_Mobile_Data_Connection_Outside_Office = pHasMobileData;

                iAddressLen = pAddress.Length;

                if (iAddressLen <= 40)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress;
                    objListOfContact[0].AlternateAddress2 = "";
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 40) && (iAddressLen <= 80))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40);
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 80) && (iAddressLen < 121))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80);
                }
                else if (iAddressLen > 120)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80, 40);
                }

                inputContact.ListOfContact = objListOfContact;

                if (pSession == null)
                {
                    pSession = EnterLoginCredentials();
                }

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactUpdate(inputContact);


                if (outputContact != null)
                {
                    // delete existing records associated on this contact on Contact Extensions table
                    processDeleteOfExtistingContactExtensionDetails(pSession, pContactId);

                    // add child record on Contact Extensions
                    if (pCKeyAreasToImprove != "" || pCKeyAreasToImprove != null)
                    {
                        processCreationOfContactExtensionDetails(pSession, pContactId, pCKeyAreasToImprove,pCPrimaryJobFocus,vAccountId);
                    }

                    // delete existing records associated on this contact on Account Extensions table, Contact Id will be saved on Attribute 01: Contact Id field
                    processDeleteOfExtistingAccountExtensionDetails(pSession,pContactId,pAccountId);

                    // add child record on Account Extensions,  Contact Id will be saved on Attribute 01: Contact Id field
                    if ((pAServicesOffered != "" || pAServicesOffered != null)|| (pCJobSiteType != "" || pCJobSiteType != null))
                    {
                        processCreationOfAccountExtensionDetails(pSession, pContactId, pAccountId, pAServicesOffered,pCJobSiteType);
                    }
                }

                contactResult = outputContact.ListOfContact[0].ContactId;

            }
            catch (Exception ex)
            {
                contactResult = "Error on updatePartnerplusContactUserProfile_Enhanced:" + pMembershipId + ":" + ex.Message;

                LogToFile("Error on updatePartnerplusContactUserProfile_Enhanced:" + pMembershipId + ":" + ex.Message);
            }


            return contactResult;
        }

        //20150319 - Added to handle update of User Profile initiated by the contractor/wholesaler
        public String updateAccountDetailsInitiatedOnUserProfile(OnDemandSession pSession, String pAccountId,String pAccountName, String pAccountType, String pCellularPhone,String pIsEmersonBrandUser, String pOtherBrands, String pABusinessIndustry, String pAServicesOffered, String pAddress, String pCity, String pStateProvince, String pCountry, String pPostalCode, String pAWorkPhone) 
        {
            String accountResult;
            String vAccountType="";

            WSOD_Account_v1.Account srvcAccount = new WSOD_Account_v1.Account();
            WSOD_Account_v1.AccountWS_AccountUpdate_Input inputAccount = new WSOD_Account_v1.AccountWS_AccountUpdate_Input();
            WSOD_Account_v1.AccountWS_AccountUpdate_Output outputAccount = new WSOD_Account_v1.AccountWS_AccountUpdate_Output();

            try
            {
                if (pAccountType == "Wholesaler")
                {
                    vAccountType = "Wholesaler";
                }
                else
                {
                    vAccountType = "Contractor";
                }

                WSOD_Account_v1.Account1[] objListOfAccount = new WSOD_Account_v1.Account1[1];
                objListOfAccount[0] = new WSOD_Account_v1.Account1();

                objListOfAccount[0].AccountId = pAccountId;
                //objListOfAccount[0].AccountName = pAccountName;
                //objListOfAccount[0].AccountType = accountType;
                //objListOfAccount[0].Location = pAddress;
                objListOfAccount[0].MainPhone = pAWorkPhone;
                //objListOfAccount[0].IndexedBoolean0 = "Y";

                //objListOfAccount[0].PrimaryShipToStreetAddress = pAddress;
                //objListOfAccount[0].PrimaryShipToCity = pCity;
                //objListOfAccount[0].PrimaryShipToProvince = pStateProvince;
                //objListOfAccount[0].PrimaryShipToCountry = pCountry;
                //objListOfAccount[0].PrimaryShipToPostalCode = pPostalCode;

                //objListOfAccount[0].PrimaryBillToStreetAddress = pAddress;
                //objListOfAccount[0].PrimaryBillToCity = pCity;
                //objListOfAccount[0].PrimaryBillToProvince = pStateProvince;
                //objListOfAccount[0].PrimaryBillToCountry = pCountry;
                //objListOfAccount[0].PrimaryBillToPostalCode = pPostalCode;

                objListOfAccount[0].bCurrent_Brand_Used_Emerson = pIsEmersonBrandUser;
                objListOfAccount[0].stCurrent_Brand_Others = pOtherBrands;
                objListOfAccount[0].ltServices_Offered = pAServicesOffered;
                objListOfAccount[0].ltBusiness_Industry = pABusinessIndustry;
                
                inputAccount.ListOfAccount = objListOfAccount;

                srvcAccount.Url = pSession.GetURL();
                srvcAccount.CookieContainer = pSession.GetCookieContainer();

                outputAccount = srvcAccount.AccountUpdate(inputAccount);

                accountResult = outputAccount.ListOfAccount[0].AccountId;
            }
            catch (Exception ex)
            {
                accountResult = "Error on updateAccountDetailsInitiatedOnUserProfile:" + pAccountId + ":" + ex.Message;
                LogToFile("Error on updateAccountDetailsInitiatedOnUserProfile:" + pAccountId + ":" + ex.Message);
            }

            return accountResult;

        }

        public String genericSaveNewLeadFromAnEventRegistration(OnDemandSession pSession,String pFirstName,String pLastName,String pEmail, String pCompanyName, String pCountry, String pBusinessType, String pIndustry, String pJobCategory, String pBusinessGroup, String pIsOptOut, String pNextStep, String pLeadSourceName)
        {
            String result = "";

            WSDL_Lead_v1.Lead srvcLead = new WSDL_Lead_v1.Lead();
            WSDL_Lead_v1.LeadWS_LeadInsert_Input inputNewLead = new WSDL_Lead_v1.LeadWS_LeadInsert_Input();
            WSDL_Lead_v1.LeadWS_LeadInsert_Output outputNewLead = new WSDL_Lead_v1.LeadWS_LeadInsert_Output();

            try
            {
                WSDL_Lead_v1.Lead1[] objListOfLead = new WSDL_Lead_v1.Lead1[1];
                objListOfLead[0] = new WSDL_Lead_v1.Lead1();

                objListOfLead[0].LeadFirstName = pFirstName;
                objListOfLead[0].LeadLastName = pLastName;
                objListOfLead[0].LeadEmail = pEmail;
                objListOfLead[0].Company = pCompanyName;
                objListOfLead[0].Country = pCountry;
                objListOfLead[0].Industry = pIndustry;
                objListOfLead[0].plJob_Category = pJobCategory;
                objListOfLead[0].LeadType = pBusinessType;
                objListOfLead[0].IndexedPick0 = pBusinessGroup;
                objListOfLead[0].Source = "Web Site";
                objListOfLead[0].plLead_Source_Name=pLeadSourceName;
                objListOfLead[0].NextStep= pNextStep;
                objListOfLead[0].NeverEmail = pIsOptOut;
                objListOfLead[0].OwnerId = ConfigurationManager.AppSettings["defaultOwnerId"].ToString();

                if (pCountry == "Canada")
                {
                    objListOfLead[0].stCanada_Campaign_Opt_Out_YN = pIsOptOut;
                    objListOfLead[0].dCASL_Implied_Consent_Expiration_Date = Convert.ToDateTime(DateTime.Now.AddYears(2)).ToString("MM/dd/yyyy");
                    objListOfLead[0].plCASL_Consent_Level = "Implied";
                    objListOfLead[0].dCASL_Consent_Date = Convert.ToDateTime(DateTime.Today).ToString("MM/dd/yyyy");
                }

                inputNewLead.ListOfLead=objListOfLead;

                EnterLoginCredentials();

                srvcLead.Url = pSession.GetURL();
                srvcLead.CookieContainer = pSession.GetCookieContainer();

                outputNewLead = srvcLead.LeadInsert(inputNewLead);
                
                result="Success; CRM Lead Row Id:" + outputNewLead.ListOfLead[0].LeadId;

            }
            catch (Exception ex)
            {
                LogToFile("Error on genericSaveNewLeadFromAnEventRegistration:" + ex.Message);
                result = "Failed; Error:" + ex.Message.ToString();
            }

            return result;
        }

        //20151112 - created by RDVillamor 
        public String genericSaveNewLeadFromPageRegistration(OnDemandSession pSession, String pFirstName, String pLastName, String pEmail, String pCompanyName, String pCountry, String pBusinessType, String pIndustry, String pJobCategory, String pBusinessGroup, String pIsOptOut, String pNextStep, String pLeadSourceName)
        {
            String result = "";

            WSDL_Lead_v1.Lead srvcLead = new WSDL_Lead_v1.Lead();
            WSDL_Lead_v1.LeadWS_LeadInsert_Input inputNewLead = new WSDL_Lead_v1.LeadWS_LeadInsert_Input();
            WSDL_Lead_v1.LeadWS_LeadInsert_Output outputNewLead = new WSDL_Lead_v1.LeadWS_LeadInsert_Output();

            try
            {
                WSDL_Lead_v1.Lead1[] objListOfLead = new WSDL_Lead_v1.Lead1[1];
                objListOfLead[0] = new WSDL_Lead_v1.Lead1();

                objListOfLead[0].LeadFirstName = pFirstName;
                objListOfLead[0].LeadLastName = pLastName;
                objListOfLead[0].LeadEmail = pEmail;
                objListOfLead[0].Company = pCompanyName;
                objListOfLead[0].Country = pCountry;
                objListOfLead[0].Industry = pIndustry;
                objListOfLead[0].plJob_Category = pJobCategory;
                objListOfLead[0].LeadType = pBusinessType;
                objListOfLead[0].IndexedPick0 = pBusinessGroup;
                objListOfLead[0].Source = "Web Site";
                objListOfLead[0].plLead_Source_Name = pLeadSourceName;

                objListOfLead[0].NextStep = pNextStep;
                objListOfLead[0].NeverEmail = pIsOptOut;
                objListOfLead[0].OwnerId = ConfigurationManager.AppSettings["defaultOwnerId"].ToString();

                if (pCountry == "Canada")
                {
                    objListOfLead[0].stCanada_Campaign_Opt_Out_YN = pIsOptOut;
                    objListOfLead[0].dCASL_Implied_Consent_Expiration_Date = Convert.ToDateTime(DateTime.Now.AddYears(2)).ToString("MM/dd/yyyy");
                    objListOfLead[0].plCASL_Consent_Level = "Implied";
                    objListOfLead[0].dCASL_Consent_Date = Convert.ToDateTime(DateTime.Today).ToString("MM/dd/yyyy");
                }

                inputNewLead.ListOfLead = objListOfLead;

                EnterLoginCredentials();

                srvcLead.Url = pSession.GetURL();
                srvcLead.CookieContainer = pSession.GetCookieContainer();

                outputNewLead = srvcLead.LeadInsert(inputNewLead);

                result = "Success; CRM Lead Row Id:" + outputNewLead.ListOfLead[0].LeadId;

            }
            catch (Exception ex)
            {
                LogToFile("Error on genericSaveNewLeadFromAnEventRegistration:" + ex.Message);
                result = "Failed; Error:" + ex.Message.ToString();
            }

            return result;
        }

        //20151216 - created by RDVillamor for the use of ETS Connect to Expert Page
        public String genericSaveNewLeadFromETSConnectToExpertPage(OnDemandSession pSession, String pFirstName, String pLastName, String pEmail, String pCompanyName, String pCountry, String pBusinessType, String pJobCategory, String pBusinessGroup, String pIsOptOut, String pNextStep, String pLeadSourceName, String pPhoneNum, String pComments)
        {
            String result = "";

            WSDL_Lead_v1.Lead srvcLead = new WSDL_Lead_v1.Lead();
            WSDL_Lead_v1.LeadWS_LeadInsert_Input inputNewLead = new WSDL_Lead_v1.LeadWS_LeadInsert_Input();
            WSDL_Lead_v1.LeadWS_LeadInsert_Output outputNewLead = new WSDL_Lead_v1.LeadWS_LeadInsert_Output();

            
            try
            {
                WSDL_Lead_v1.Lead1[] objListOfLead = new WSDL_Lead_v1.Lead1[1];
                objListOfLead[0] = new WSDL_Lead_v1.Lead1();

                objListOfLead[0].LeadFirstName = pFirstName;
                objListOfLead[0].LeadLastName = pLastName;
                objListOfLead[0].LeadEmail = pEmail;
                objListOfLead[0].Company = pCompanyName;
                objListOfLead[0].Country = pCountry;
                objListOfLead[0].plJob_Category = pJobCategory;
                objListOfLead[0].LeadType = pBusinessType;
                objListOfLead[0].IndexedPick0 = pBusinessGroup;
                objListOfLead[0].Source = "Web Site";

                if (pLeadSourceName == "1")
                {
                    objListOfLead[0].plLead_Source_Name = "Connect an ETS Reefer Expert";
                }
                else
                {
                    objListOfLead[0].plLead_Source_Name = pLeadSourceName;
                }

                objListOfLead[0].Description = pComments;
                objListOfLead[0].PrimaryPhone = pPhoneNum;
                objListOfLead[0].NextStep = pNextStep;
                objListOfLead[0].NeverEmail = pIsOptOut;
                objListOfLead[0].OwnerId = ConfigurationManager.AppSettings["defaultOwnerId"].ToString();

                if (pCountry == "Canada")
                {
                    objListOfLead[0].stCanada_Campaign_Opt_Out_YN = pIsOptOut;
                    objListOfLead[0].dCASL_Implied_Consent_Expiration_Date = Convert.ToDateTime(DateTime.Now.AddYears(2)).ToString("MM/dd/yyyy");
                    objListOfLead[0].plCASL_Consent_Level = "Implied";
                    objListOfLead[0].dCASL_Consent_Date = Convert.ToDateTime(DateTime.Today).ToString("MM/dd/yyyy");
                }

                inputNewLead.ListOfLead = objListOfLead;

                EnterLoginCredentials();

                srvcLead.Url = pSession.GetURL();
                srvcLead.CookieContainer = pSession.GetCookieContainer();

                outputNewLead = srvcLead.LeadInsert(inputNewLead);

                result = "Success; CRM Lead Row Id:" + outputNewLead.ListOfLead[0].LeadId;

            }
            catch (Exception ex)
            {
                LogToFile("Error on genericSaveNewLeadFromAnEventRegistration:" + ex.Message);
                result = "Failed; Error:" + ex.Message.ToString();
            }

            return result;
        }



        //20151109 - added by RDVillamor, for the use of AHR Registration on Sharepoint page
        public String genericSaveNewLeadFromAHRRegistration(OnDemandSession pSession, String pFirstName, String pLastName, String pEmail, String pCompanyName, String pCountry, String pBusinessType, String pIndustry, String pJobCategory, String pBusinessGroup, String pIsOptOut, String pNextStep, String pLeadSourceName, String pContactId)
        {
            String result = "";
            String pAHR2016EventId = "AOOA-39U5XQ";

            WSDL_Lead_v1.Lead srvcLead = new WSDL_Lead_v1.Lead();
            WSDL_Lead_v1.LeadWS_LeadInsert_Input inputNewLead = new WSDL_Lead_v1.LeadWS_LeadInsert_Input();
            WSDL_Lead_v1.LeadWS_LeadInsert_Output outputNewLead = new WSDL_Lead_v1.LeadWS_LeadInsert_Output();

            String defaultAHRActivityOwnerId = ConfigurationManager.AppSettings["defaultAHRActivityOwnerId"].ToString();

            try
            {
                WSDL_Lead_v1.Lead1[] objListOfLead = new WSDL_Lead_v1.Lead1[1];
                objListOfLead[0] = new WSDL_Lead_v1.Lead1();

                objListOfLead[0].LeadFirstName = pFirstName;
                objListOfLead[0].LeadLastName = pLastName;
                objListOfLead[0].LeadEmail = pEmail;
                objListOfLead[0].Company = pCompanyName;
                objListOfLead[0].Country = pCountry;
                objListOfLead[0].Industry = pIndustry;
                objListOfLead[0].plJob_Category = pJobCategory;
                objListOfLead[0].LeadType = pBusinessType;

                if (pBusinessGroup == "Contractor-Wholesaler")
                {
                    objListOfLead[0].IndexedPick0 = "Contractor & Wholesaler";
                }
                else
                {
                    objListOfLead[0].IndexedPick0 = pBusinessGroup;
                }

                objListOfLead[0].ContactId = pContactId;
                objListOfLead[0].Source = "Web Site";
                objListOfLead[0].plLead_Source_Name = pLeadSourceName;
                objListOfLead[0].NextStep = pNextStep;
                objListOfLead[0].NeverEmail = pIsOptOut;
                objListOfLead[0].OwnerId = ConfigurationManager.AppSettings["defaultOwnerId"].ToString();

                if (pCountry == "Canada")
                {
                    objListOfLead[0].stCanada_Campaign_Opt_Out_YN = pIsOptOut;
                    objListOfLead[0].dCASL_Implied_Consent_Expiration_Date = Convert.ToDateTime(DateTime.Now.AddYears(2)).ToString("MM/dd/yyyy");
                    objListOfLead[0].plCASL_Consent_Level = "Implied";
                    objListOfLead[0].dCASL_Consent_Date = Convert.ToDateTime(DateTime.Today).ToString("MM/dd/yyyy");
                }

                inputNewLead.ListOfLead = objListOfLead;

                EnterLoginCredentials();

                srvcLead.Url = pSession.GetURL();
                srvcLead.CookieContainer = pSession.GetCookieContainer();

                outputNewLead = srvcLead.LeadInsert(inputNewLead);

                addNewChildActivity(pSession, "Task", "To Do", defaultAHRActivityOwnerId, "For Disposition: Qualification, Conversion to a Contact and Association on AHR 2016 Event", "In Progress", pAHR2016EventId, "", pContactId, "", "", outputNewLead.ListOfLead[0].LeadId, "", pNextStep);

                result = "Success; CRM Lead Row Id:" + outputNewLead.ListOfLead[0].LeadId;

            }
            catch (Exception ex)
            {
                LogToFile("Error on genericSaveNewLeadFromAHRRegistration:" + ex.Message);
                result = "Failed; Error:" + ex.Message.ToString();
            }

            return result;
        }

        //20151118 - Added by RDVillamor
        public String addNewChildActivity(OnDemandSession mSession, String pActivity, String pType, String pOwnerId, String pSubject, String pStatus, String pEventId, String pAccountId, String pContactId, String pCampaignId, String pServiceRequestId, String pLeadId, String pGroupMember, String pDescription)
        {
            String result = "";

            WSDL_Activity_v1.Activity srvcActivity = new WSDL_Activity_v1.Activity();
            WSDL_Activity_v1.ActivityNWS_Activity_Insert_Input inputActivity = new WSDL_Activity_v1.ActivityNWS_Activity_Insert_Input();
            WSDL_Activity_v1.ActivityNWS_Activity_Insert_Output outputActivity = new WSDL_Activity_v1.ActivityNWS_Activity_Insert_Output();

            try
            {
                WSDL_Activity_v1.Activity1[] objListOfActivity = new WSDL_Activity_v1.Activity1[1];
                objListOfActivity[0] = new WSDL_Activity_v1.Activity1();
                objListOfActivity[0].Activity = pActivity;
                objListOfActivity[0].Type = pType;
                objListOfActivity[0].Status = pStatus;
                objListOfActivity[0].Subject = pSubject;
                objListOfActivity[0].IndexedPick0 = pGroupMember;
                objListOfActivity[0].Description = pDescription;
                objListOfActivity[0].OwnerId = pOwnerId;
                objListOfActivity[0].PrimaryContactId = pContactId;
                objListOfActivity[0].MedEdEventId = pEventId;
                objListOfActivity[0].AccountId = pAccountId;
                objListOfActivity[0].CampaignId = pCampaignId;
                objListOfActivity[0].ServiceRequestId = pServiceRequestId;
                objListOfActivity[0].LeadId = pLeadId;

                inputActivity.ListOfActivity = objListOfActivity;

                EnterLoginCredentials();

                srvcActivity.Url = mSession.GetURL();
                srvcActivity.CookieContainer = mSession.GetCookieContainer();
                outputActivity = srvcActivity.Activity_Insert(inputActivity);

                result = "Success";
            }
            catch (Exception ex)
            {
                LogToFile("Error on addNewChildActivity(): --> " + ex.Message);
                result = "Failed; Error Message:" + ex.Message.ToString();
            }

            return result;
        }

        //20160318: RDVillamor, just added to check if ashx can be available in the internet
        public String postInviteeStatus(OnDemandSession mSession, String pContactId, String pEventId, String pInviteeStatus, String pInviteeSource, String pEmail)
        {
            LogToFile("Start processing postInviteeStatus:pContactId" + pContactId + ",pEventId:" + pEventId + ",pInviteeStatus:" + pInviteeStatus + ",pInviteeSource:" + pInviteeSource + ",pEmail:" + pEmail);

            String result = "";
            try
            {
                try
                {
                    WSOD_Event_v1.MedEd srvcEvent = new WSOD_Event_v1.MedEd();
                    WSOD_Event_v1.MedEdWS_MedEdUpdateChild_Input inputEvent = new WSOD_Event_v1.MedEdWS_MedEdUpdateChild_Input();
                    WSOD_Event_v1.MedEdWS_MedEdUpdateChild_Output outputEvent = new WSOD_Event_v1.MedEdWS_MedEdUpdateChild_Output();

                    inputEvent.ListOfMedEd = new WSOD_Event_v1.MedEd1[1];
                    inputEvent.ListOfMedEd[0] = new WSOD_Event_v1.MedEd1();
                    inputEvent.ListOfMedEd[0].ListOfInvitee = new WSOD_Event_v1.Invitee[1];
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0] = new WSOD_Event_v1.Invitee();

                    inputEvent.ListOfMedEd[0].MedEdId = pEventId;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].ContactId = pContactId;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].Status = pInviteeStatus;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].plInvitee_Source = pInviteeSource;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].dtDateTime_of_Registration = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                    EnterLoginCredentials();

                    srvcEvent.Url = mSession.GetURL();
                    srvcEvent.CookieContainer = mSession.GetCookieContainer();
                    outputEvent = srvcEvent.MedEdUpdateChild(inputEvent);

                    result = "Success";
                    LogToFile("Success");
                }
                catch (Exception ex)
                {
                    LogToFile("Error on postInviteeStatus:" + ex.Message + "--> retry using InsertChild");

                    WSOD_Event_v1.MedEd srvcEvent = new WSOD_Event_v1.MedEd();
                    WSOD_Event_v1.MedEdWS_MedEdInsertChild_Input inputEvent = new WSOD_Event_v1.MedEdWS_MedEdInsertChild_Input();
                    WSOD_Event_v1.MedEdWS_MedEdInsertChild_Output outputEvent = new WSOD_Event_v1.MedEdWS_MedEdInsertChild_Output();

                    inputEvent.ListOfMedEd = new WSOD_Event_v1.MedEd1[1];
                    inputEvent.ListOfMedEd[0] = new WSOD_Event_v1.MedEd1();
                    inputEvent.ListOfMedEd[0].ListOfInvitee = new WSOD_Event_v1.Invitee[1];
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0] = new WSOD_Event_v1.Invitee();

                    inputEvent.ListOfMedEd[0].MedEdId = pEventId;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].ContactId = pContactId;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].Status = pInviteeStatus;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].plInvitee_Source = pInviteeSource;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].dtDateTime_of_Registration = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                    EnterLoginCredentials();

                    srvcEvent.Url = mSession.GetURL();
                    srvcEvent.CookieContainer = mSession.GetCookieContainer();
                    outputEvent = srvcEvent.MedEdInsertChild(inputEvent);

                    result = "Success";
                    LogToFile("Successful on retry");
                }
            }
            catch (Exception ex)
            {
                LogToFile("Error on postInviteeStatus:" + ex.Message);
                result = "Failed: " + ex.Message;
            }

            return result;
        }

        /* 20160509 / RDVillamor
         * This function is developed for the enhanced partnerplus registration process
         * t=11
         */
        public ArrayList processPartnerplusSignUpDetails(OnDemandSession pSession, String pFirstName, String pLastName, String pEmail, String pMobileNumber, String pAddress, String pCountry, String pBusinessName, String pPartnerplusMembershipId, String pOptIn)
        {
            ArrayList arrPartnerplusContactDetails = new ArrayList();

            ArrayList vContactDetails = new ArrayList();
            String vContactIdOfExistingRecord = "";

            try
            {
                //arrPartnerplusContactDetails = validateIfExistingContact(pSession, pFirstName, pLastName, pEmail, pMobileNumber);

                vContactIdOfExistingRecord = validateIfExistingContact(pSession, pFirstName, pLastName, pEmail, pMobileNumber);

                if ((vContactIdOfExistingRecord == "") || (vContactIdOfExistingRecord == "Error"))
                //if (validateIfExistingContact(pSession, pFirstName, pLastName, pEmail, pMobileNumber).Count == 0)
                {
                    arrPartnerplusContactDetails = createNewPartnerPlusContactRegistration_Asia(pSession, pFirstName, pLastName, pEmail, pMobileNumber, pCountry, pBusinessName, pAddress, pPartnerplusMembershipId, pOptIn);
                }
                else
                {
                    arrPartnerplusContactDetails = updateExistingContactAsNewPartnerPlusRegistrant_Asia(pSession, vContactIdOfExistingRecord, pFirstName, pLastName, pEmail, pMobileNumber, pCountry, pBusinessName, pAddress, pPartnerplusMembershipId, pOptIn);

                }

            }
            catch (Exception ex)
            {
                ContactV1 csContact = new ContactV1();

                csContact.ContactMembershipId = pPartnerplusMembershipId;
                csContact.ErrorMessage = ex.Message;

                arrPartnerplusContactDetails.Add(csContact);
            }

            return arrPartnerplusContactDetails;
        }

        /* 20160708 / RDVillamor
         * This function is developed for the enhanced partnerplus registration process of MEA
         * t=11
         */
        public ArrayList processPartnerplusSignUpDetails_MEA(OnDemandSession pSession, String pFirstName, String pLastName, String pEmail, String pMobileNumber, String pAddress, String pCountry, String pBusinessName, String pPartnerplusMembershipId, String pOptIn)
        {
            ArrayList arrPartnerplusContactDetails = new ArrayList();

            ArrayList vContactDetails = new ArrayList();
            String vContactIdOfExistingRecord = "";

            try
            {
                //arrPartnerplusContactDetails = validateIfExistingContact(pSession, pFirstName, pLastName, pEmail, pMobileNumber);

                vContactIdOfExistingRecord = validateIfExistingContact(pSession, pFirstName, pLastName, pEmail, pMobileNumber);

                if ((vContactIdOfExistingRecord == "") || (vContactIdOfExistingRecord == "Error"))
                {
                    arrPartnerplusContactDetails = createNewPartnerPlusContactRegistration_MEA(pSession, pFirstName, pLastName, pEmail, pMobileNumber, pCountry, pBusinessName, pAddress, pPartnerplusMembershipId, pOptIn);
                }
                else
                {
                    arrPartnerplusContactDetails = updateExistingContactAsNewPartnerPlusRegistrant_MEA(pSession, vContactIdOfExistingRecord, pFirstName, pLastName, pEmail, pMobileNumber, pCountry, pBusinessName, pAddress, pPartnerplusMembershipId, pOptIn);
                }

            }
            catch (Exception ex)
            {
                ContactV1 csContact = new ContactV1();

                csContact.ContactMembershipId = pPartnerplusMembershipId;
                csContact.ErrorMessage = ex.Message;

                arrPartnerplusContactDetails.Add(csContact);
            }

            return arrPartnerplusContactDetails;
        }

        /*
         * 20160803
         * This function is developed for the enhanced partnerplus registration under Phase 3 project
         * t=11
         */
        public ArrayList processPartnerplusSignUpDetails_Phase3_Enhanced(OnDemandSession pSession, String pFirstName, String pLastName, String pEmail, String pMobileNumber, String pCity, String pCountry, String pBusinessName, String pPartnerplusMembershipId, String pOptIn, String pGroup)
        {
            ArrayList arrPartnerplusContactDetails = new ArrayList();

            ArrayList vContactDetails = new ArrayList();
            String vContactIdOfExistingRecord = "";

            try
            {
                vContactIdOfExistingRecord = validateIfExistingContact(pSession, pFirstName, pLastName, pEmail, pMobileNumber);

                if ((vContactIdOfExistingRecord == "") || (vContactIdOfExistingRecord == "Error"))
                {
                        arrPartnerplusContactDetails = createNewPartnerPlusContactRegistration_Phase3_Enhancement(pSession, pFirstName, pLastName, pEmail, pMobileNumber, pCountry, pBusinessName, pCity, pPartnerplusMembershipId, pOptIn,pGroup);
                }
                else
                {
                    arrPartnerplusContactDetails = updateExistingContactAsNewPartnerPlusRegistrant_Phase3_Enhancement(pSession, vContactIdOfExistingRecord, pFirstName, pLastName, pEmail, pMobileNumber, pCountry, pBusinessName, pCity, pPartnerplusMembershipId, pOptIn,pGroup);
                }
            }
            catch (Exception ex)
            {
                ContactV1 csContact = new ContactV1();

                csContact.ContactMembershipId = pPartnerplusMembershipId;
                csContact.ErrorMessage = ex.Message;

                arrPartnerplusContactDetails.Add(csContact);
            }

            return arrPartnerplusContactDetails;
        }


        /* 20160510 / RDVillamor
         * This function is developed for the enhanced partnerplus registration process to put a checking if the encoded registration details already exist on CRM
         * 
         * 20160526 / RDVillamor
         * enhanced to set the primary way of checking ing First Name and Last Name. Use mobile number and email address on 2nd level of query)
         */
        public String validateIfExistingContact(OnDemandSession pSession, String pFirstName, String pLastName, String pEmailAddress, String pMobileNumber)
        {
            ArrayList arrPartnerWebContactDetails = new ArrayList();
            String vContactRowId = "";
        

            WSOD_Contact_v2.Contact srvcContact = new WSOD_Contact_v2.Contact();
            WSOD_Contact_v2.ContactQueryPage_Input inputContactQuery = new WSOD_Contact_v2.ContactQueryPage_Input();
            WSOD_Contact_v2.ContactQueryPage_Output outputContactQuery = new WSOD_Contact_v2.ContactQueryPage_Output();
          
            try
            {
                inputContactQuery.ListOfContact = new WSOD_Contact_v2.ListOfContactQuery();
                inputContactQuery.ListOfContact.Contact = new WSOD_Contact_v2.ContactQuery();

                inputContactQuery.ListOfContact.pagesize = "100";
                inputContactQuery.ListOfContact.startrownum = "0";

                inputContactQuery.ListOfContact.Contact.Id = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.IndexedShortText0 = new WSOD_Contact_v2.queryType();

                inputContactQuery.ListOfContact.Contact.AccountId = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AccountLocation = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AccountName = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.wCRM_Contact_Link = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.bNever_SMS = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactFirstName = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactLastName = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactEmail = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.WorkPhone = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.CellularPhone = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.HomePhone = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.JobTitle = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.stSecond_Email = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactType = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateAddressId = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateAddress1 = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateAddress2 = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateAddress3 = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateCity = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateCountry = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateProvince = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateStateProvince = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AlternateZipCode = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.IndexedPick0 = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.OwnerId = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.NeverEmail = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ltPartnerPlus_Business_Name = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.stPartnerPlus_Roles_By_Country = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.plMembership_Status = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.stPartnerWeb_Membership_ID = new WSOD_Contact_v2.queryType();

                ////20160715 - RDVIllamor, added to reflect the IndexedShortText1 which is the real holder of Partner+ Membership ID
                inputContactQuery.ListOfContact.Contact.IndexedShortText1 = new WSOD_Contact_v2.queryType();

                ////commented on 20160526, see above remarks
                //if (pEmailAddress != "")
                //{
                //    inputContactQuery.ListOfContact.Contact.searchspec = "[ContactFirstName]~='" + pFirstName + "' AND [ContactLastName]~='" + pLastName + "' AND [ContactEmail]~='" + pEmailAddress + "'";
                //}
                //else if (pMobileNumber != "")
                //{
                //    inputContactQuery.ListOfContact.Contact.searchspec = "[ContactFirstName]~='" + pFirstName + "' AND [ContactLastName]~='" + pLastName + "' AND [CellularPhone]~='" + pMobileNumber + "'";
                //}
                //inputContactQuery.ListOfContact.Contact.ModifiedDate.sortorder = "desc";

                //20160526, newly added
                inputContactQuery.ListOfContact.Contact.searchspec = "[ContactFirstName]~='" + pFirstName + "' AND [ContactLastName]~='" + pLastName + "'";

                //inputContactQuery.ListOfContact.Contact.ModifiedDate.sortorder = "DESC";

                if (pSession == null)
                {
                    pSession = EnterLoginCredentials();
                }

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();
                outputContactQuery = srvcContact.ContactQueryPage(inputContactQuery);

                if (outputContactQuery != null)
                {
                    if (outputContactQuery.ListOfContact.Contact != null)
                    {
                        if (outputContactQuery.ListOfContact.Contact.Length > 0)
                        {
                            ContactV1 csContact = new ContactV1();

                            for (int idx = 0; idx < outputContactQuery.ListOfContact.Contact.Length; idx++)
                            {
                                if ((pEmailAddress.ToUpper() == outputContactQuery.ListOfContact.Contact[idx].ContactEmail.ToUpper() || (outputContactQuery.ListOfContact.Contact[idx].IndexedShortText1 == null) || (outputContactQuery.ListOfContact.Contact[idx].IndexedShortText1 == "")) && ((outputContactQuery.ListOfContact.Contact[idx].IndexedShortText1 == "") || (outputContactQuery.ListOfContact.Contact[idx].IndexedShortText1 ==null)))
                                {
                                    csContact.ContactId = outputContactQuery.ListOfContact.Contact[idx].Id;
                                    csContact.ContactMembershipId = outputContactQuery.ListOfContact.Contact[idx].IndexedShortText1;
                                    csContact.ContactFirstName = outputContactQuery.ListOfContact.Contact[idx].ContactFirstName;
                                    csContact.ContactLastName = outputContactQuery.ListOfContact.Contact[idx].ContactLastName;
                                    csContact.ContactEmailAddress = outputContactQuery.ListOfContact.Contact[idx].ContactEmail;
                                    csContact.PartnerplusMembershipStatus = outputContactQuery.ListOfContact.Contact[idx].plMembership_Status;
                                    csContact.ContactCellularPhone = outputContactQuery.ListOfContact.Contact[idx].CellularPhone;

                                    arrPartnerWebContactDetails.Add(csContact);

                                    vContactRowId = outputContactQuery.ListOfContact.Contact[idx].Id;

                                    break;
                                }
                                else if (("+" + pMobileNumber.Trim().Replace(" ","") == outputContactQuery.ListOfContact.Contact[idx].CellularPhone.Trim().Replace(" ","")) && ((outputContactQuery.ListOfContact.Contact[idx].IndexedShortText1 == "") || (outputContactQuery.ListOfContact.Contact[idx].IndexedShortText1 ==null)))
                                {
                                    csContact.ContactId = outputContactQuery.ListOfContact.Contact[idx].Id;
                                    csContact.ContactMembershipId = outputContactQuery.ListOfContact.Contact[idx].IndexedShortText1;
                                    csContact.ContactFirstName = outputContactQuery.ListOfContact.Contact[idx].ContactFirstName;
                                    csContact.ContactLastName = outputContactQuery.ListOfContact.Contact[idx].ContactLastName;
                                    csContact.ContactEmailAddress = outputContactQuery.ListOfContact.Contact[idx].ContactEmail;
                                    csContact.PartnerplusMembershipStatus = outputContactQuery.ListOfContact.Contact[idx].plMembership_Status;
                                    csContact.ContactCellularPhone = outputContactQuery.ListOfContact.Contact[idx].CellularPhone;

                                    arrPartnerWebContactDetails.Add(csContact);

                                    vContactRowId = outputContactQuery.ListOfContact.Contact[idx].Id;

                                    break;
                                }

                            }

                        }
                    }
                }


            }
            catch(Exception ex)
            {
                vContactRowId = "Error";
            }

            return vContactRowId;
        }

        /* 20160513 RDVillamor
         * function to convert the string the first letter to uppercase then all others as is
         */
        private String capitalizeFirstLetter(String pString)
        {
            String s = "";

            s = pString.Substring(0, 1).ToUpper() + pString.Substring(1);

            return s;
        }

        private String transformSpecialCharacters(String pString)
        {
            String s = "";

            s = pString.Replace("\u0026", "&").Replace("'", "''");

            return s;
        }

        /*20160511 RDVillamor
         * createNewPartnerPlusRegistration - 
         */
        public ArrayList createNewPartnerPlusContactRegistration_Asia(OnDemandSession pSession, String pFirstName, String pLastName, String pEmail, String pMobileNumber, String pCountry, String pBusinessName, String pAddress, String pPartnerplusMembershipId, String pOptIn)
        {
            String isOptOut = "";
            String contactOwner = ConfigurationManager.AppSettings["defaultOwner"].ToString();
            String vCountry="";
            int iAddressLen=0;

            ArrayList arrContact = new ArrayList();

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactInsert_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactInsert_Input();
            WSOD_Contact_v1.ContactWS_ContactInsert_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactInsert_Output();

            try
            {
                iAddressLen = pAddress.Length;

                if ((pCountry == "South Korea"))
                {
                    vCountry = "Korea";
                }
                else
                {
                    vCountry = pCountry;
                }

                if (pOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactFirstName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim();
                objListOfContact[0].ContactLastName = capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].ContactEmail = pEmail.Trim();
                objListOfContact[0].ContactFullName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim() + " " + capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].IndexedPick0 = "Asia";

                objListOfContact[0].IndexedShortText1 = pPartnerplusMembershipId;
                objListOfContact[0].OwnerFullName = contactOwner;
                objListOfContact[0].CellularPhone = "+" + pMobileNumber;
                objListOfContact[0].bNever_SMS = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].stPartner_Campaign_Opt_Out_YN = isOptOut;
                //objListOfContact[0].stPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();
                objListOfContact[0].ltPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();

                if (iAddressLen <= 40)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress;
                }
                else if ((iAddressLen >40 ) && (iAddressLen<=80))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0,40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40);
                }
                else if ((iAddressLen > 80) && (iAddressLen < 121))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40,40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80);
                }
                else if (iAddressLen > 120)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40,40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80,40);
                }

                objListOfContact[0].AlternateCountry = vCountry;

                objListOfContact[0].plMembership_Status = "Member";
                objListOfContact[0].dMembership_Validity = DateTime.Now.ToShortDateString();

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactInsert(inputContact);

                if (outputContact != null)
                {
                    if (outputContact.ListOfContact != null)
                    {
                        ContactV1 csContact = new ContactV1();
                        csContact.ContactId = outputContact.ListOfContact[0].ContactId;
                        csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                        csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;

                        arrContact.Add(csContact);
                    }
                }
            }
            catch (Exception ex)
            {
                ContactV1 csContact = new ContactV1();
                csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;
                csContact.ErrorMessage = ex.Message;

                arrContact.Add(csContact);

                LogToFile("Error on createNewPartnerPlusContactRegistration_Asia:" + pPartnerplusMembershipId + ":" + ex.Message);
            }

            return arrContact;
        }

        /*20160708 RDVillamor
         * createNewPartnerPlusRegistration - for MEA
         */
        public ArrayList createNewPartnerPlusContactRegistration_MEA(OnDemandSession pSession, String pFirstName, String pLastName, String pEmail, String pMobileNumber, String pCountry, String pBusinessName, String pAddress, String pPartnerplusMembershipId, String pOptIn)
        {
            String isOptOut = "";
            String contactOwner = ConfigurationManager.AppSettings["defaultOwner"].ToString();
            String vCountry = "";
            int iAddressLen = 0;

            ArrayList arrContact = new ArrayList();

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactInsert_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactInsert_Input();
            WSOD_Contact_v1.ContactWS_ContactInsert_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactInsert_Output();

            try
            {
                iAddressLen = pAddress.Length;

                if (pCountry == "Democratic Republic of the Congo")
                {
                    vCountry = "Congo, Democratic Republic of";
                }
                else if (pCountry == "Libya")
                {
                    vCountry = "Libyan Arab Jamahiriya";
                }
                else if (pCountry == "Tanzania")
                {
                    vCountry = "Tanzania, United Republic of";
                }
                else
                {
                    vCountry = pCountry;
                }

                if (pOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactFirstName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim();
                objListOfContact[0].ContactLastName = capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].ContactEmail = pEmail.Trim();
                objListOfContact[0].ContactFullName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim() + " " + capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].IndexedPick0 = "MEA";

                objListOfContact[0].IndexedShortText1 = pPartnerplusMembershipId;
                objListOfContact[0].OwnerFullName = contactOwner;
                objListOfContact[0].CellularPhone = "+" + pMobileNumber;
                objListOfContact[0].bNever_SMS = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].stPartner_Campaign_Opt_Out_YN = isOptOut;
                //objListOfContact[0].stPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();
                objListOfContact[0].ltPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();

                if (iAddressLen <= 40)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress;
                }
                else if ((iAddressLen > 40) && (iAddressLen <= 80))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40);
                }
                else if ((iAddressLen > 80) && (iAddressLen < 121))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80);
                }
                else if (iAddressLen > 120)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80, 40);
                }

                objListOfContact[0].AlternateCountry = vCountry;

                objListOfContact[0].plMembership_Status = "Member";
                objListOfContact[0].dMembership_Validity = DateTime.Now.ToShortDateString();

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactInsert(inputContact);

                if (outputContact != null)
                {
                    if (outputContact.ListOfContact != null)
                    {
                        ContactV1 csContact = new ContactV1();
                        csContact.ContactId = outputContact.ListOfContact[0].ContactId;
                        csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                        csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;

                        arrContact.Add(csContact);
                    }
                }
            }
            catch (Exception ex)
            {
                ContactV1 csContact = new ContactV1();
                csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;
                csContact.ErrorMessage = ex.Message;

                arrContact.Add(csContact);

                LogToFile("Error on createNewPartnerPlusContactRegistration_MEA:" + pPartnerplusMembershipId + ":" + ex.Message);
            }

            return arrContact;
        }

        /* 20160805 RDVillamor
         * createNewPartnerPlusContactRegistration_Phase3_Enhancement - for MEA and Asia
         */
        public ArrayList createNewPartnerPlusContactRegistration_Phase3_Enhancement(OnDemandSession pSession, String pFirstName, String pLastName, String pEmail, String pMobileNumber, String pCountry, String pBusinessName, String pCity, String pPartnerplusMembershipId, String pOptIn, String pGroup)
        {
            String isOptOut = "";
            String contactOwner = ConfigurationManager.AppSettings["defaultOwner"].ToString();
            String vCountry = "";

            ArrayList arrContact = new ArrayList();

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactInsert_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactInsert_Input();
            WSOD_Contact_v1.ContactWS_ContactInsert_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactInsert_Output();

            try
            {
                if (pCountry == "Guinea-Bissau")
                {
                    vCountry = "Guinea-bissau";
                }
                else if (pCountry == "Democratic Republic of the Congo")
                {
                    vCountry = "Congo, Democratic Republic of";
                }
                else if (pCountry == "Libya")
                {
                    vCountry = "Libyan Arab Jamahiriya";
                }
                else if (pCountry == "Tanzania")
                {
                    vCountry = "Tanzania, United Republic of";
                }
                else if ((pCountry == "South Korea"))
                {
                    vCountry = "Korea";
                }
                else
                {
                    vCountry = pCountry;
                }

                if (pOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactFirstName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim();
                objListOfContact[0].ContactLastName = capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].ContactEmail = pEmail.Trim();
                objListOfContact[0].ContactFullName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim() + " " + capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].IndexedPick0 = pGroup;

                objListOfContact[0].IndexedShortText1 = pPartnerplusMembershipId;
                objListOfContact[0].OwnerFullName = contactOwner;
                objListOfContact[0].CellularPhone = "+" + pMobileNumber;
                objListOfContact[0].bNever_SMS = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].stPartner_Campaign_Opt_Out_YN = isOptOut;
                objListOfContact[0].ltPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();

                objListOfContact[0].AlternateCountry = vCountry;
                objListOfContact[0].AlternateCity = pCity;
                objListOfContact[0].plMembership_Status = "Member";
                objListOfContact[0].dMembership_Validity = DateTime.Now.ToShortDateString();

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactInsert(inputContact);

                if (outputContact != null)
                {
                    if (outputContact.ListOfContact != null)
                    {
                        ContactV1 csContact = new ContactV1();
                        csContact.ContactId = outputContact.ListOfContact[0].ContactId;
                        csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                        csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;

                        arrContact.Add(csContact);
                    }
                }
            }
            catch (Exception ex)
            {
                ContactV1 csContact = new ContactV1();
                csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;
                csContact.ErrorMessage = ex.Message;

                arrContact.Add(csContact);

                LogToFile("Error on createNewPartnerPlusContactRegistration_Phase3_Enhancement:" + pPartnerplusMembershipId + ":" + ex.Message);
            }

            return arrContact;
        }


        /* 20160511 RDVillamor
         * updateExistingContactAsNewPartnerPlusRegistrant - this method will be access if CRM validated that the registrant is an existing CRM Contact
         */
        public ArrayList updateExistingContactAsNewPartnerPlusRegistrant_Asia(OnDemandSession pSession, String pExistingContactId,String pFirstName, String pLastName, String pEmail, String pMobileNumber, String pCountry, String pBusinessName, String pAddress, String pPartnerplusMembershipId, String pOptIn)
        {
            String isOptOut = "";
            String vCountry = "";
            int iAddressLen = 0;

            ArrayList arrContact = new ArrayList();

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Output();

            try
            {
                iAddressLen = pAddress.Length;

                if ((pCountry == "South Korea"))
                {
                    vCountry = "Korea";
                }
                else
                {
                    vCountry = pCountry;
                }

                if (pOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactFirstName = capitalizeFirstLetter(pFirstName.Replace("'","''")).Trim();
                objListOfContact[0].ContactLastName = capitalizeFirstLetter(pLastName.Replace("'","''")).Trim();
                objListOfContact[0].ContactEmail = pEmail.Trim();
                objListOfContact[0].ContactFullName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim() + " " + capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].IndexedShortText1 = pPartnerplusMembershipId.Trim();
                objListOfContact[0].CellularPhone = "+" + pMobileNumber.Trim();
                objListOfContact[0].ContactId = pExistingContactId;

                //insert address of account
                if (iAddressLen <= 40)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress;
                    objListOfContact[0].AlternateAddress2 = "";
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 40) && (iAddressLen <= 80))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40);
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 80) && (iAddressLen < 121))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80);
                }
                else if (iAddressLen > 120)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80, 40);
                }

                objListOfContact[0].AlternateCountry = vCountry;
                objListOfContact[0].bNever_SMS = isOptOut;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                //objListOfContact[0].stPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'","''")).Trim();
                objListOfContact[0].ltPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();

                //20151006 - added to fix the bug that date of registration was not included;
                objListOfContact[0].dMembership_Validity = DateTime.Now.ToShortDateString();
                objListOfContact[0].plMembership_Status = "Member";

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactUpdate(inputContact);

                if (outputContact != null)
                {
                    if (outputContact.ListOfContact != null)
                    {
                        ContactV1 csContact = new ContactV1();
                        csContact.ContactId = outputContact.ListOfContact[0].ContactId;
                        csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                        csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;

                        arrContact.Add(csContact);
                    }
                }

            }
            catch (Exception ex)
            {
                ContactV1 csContact = new ContactV1();

                csContact.ContactMembershipId = pPartnerplusMembershipId;
                csContact.ErrorMessage = ex.Message;

                arrContact.Add(csContact);

                LogToFile("Error on updateExistingContactAsNewPartnerPlusRegistrant_Asia:" + pPartnerplusMembershipId + ":" + ex.Message);
            }
            return arrContact;
        }

        /* 20160805 RDVillamor
         * updateExistingContactAsNewPartnerPlusRegistrant_Phase3_Enhancement - this method will be access if CRM validated that the registrant is an existing CRM Contact
         */
        public ArrayList updateExistingContactAsNewPartnerPlusRegistrant_Phase3_Enhancement(OnDemandSession pSession, String pExistingContactId, String pFirstName, String pLastName, String pEmail, String pMobileNumber, String pCountry, String pBusinessName, String pAddress, String pPartnerplusMembershipId, String pOptIn, String pGroup)
        {
            String isOptOut = "";
            String vCountry = "";
            int iAddressLen = 0;

            ArrayList arrContact = new ArrayList();

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Output();

            try
            {
                iAddressLen = pAddress.Length;

                if (pCountry == "Guinea-Bissau")
                {
                    vCountry = "Guinea-bissau";
                }
                else if (pCountry == "Democratic Republic of the Congo")
                {
                    vCountry = "Congo, Democratic Republic of";
                }
                else if (pCountry == "Libya")
                {
                    vCountry = "Libyan Arab Jamahiriya";
                }
                else if (pCountry == "Tanzania")
                {
                    vCountry = "Tanzania, United Republic of";
                }
                else if ((pCountry == "South Korea"))
                {
                    vCountry = "Korea";
                }
                else
                {
                    vCountry = pCountry;
                }

                if (pOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactFirstName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim();
                objListOfContact[0].ContactLastName = capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].ContactEmail = pEmail.Trim();
                objListOfContact[0].ContactFullName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim() + " " + capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].IndexedShortText1 = pPartnerplusMembershipId.Trim();
                objListOfContact[0].CellularPhone = "+" + pMobileNumber.Trim();
                objListOfContact[0].ContactId = pExistingContactId;
                objListOfContact[0].IndexedPick0 = pGroup;

                //insert address of account
                if (iAddressLen <= 40)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress;
                    objListOfContact[0].AlternateAddress2 = "";
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 40) && (iAddressLen <= 80))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40);
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 80) && (iAddressLen < 121))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80);
                }
                else if (iAddressLen > 120)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80, 40);
                }

                objListOfContact[0].AlternateCountry = vCountry;
                objListOfContact[0].bNever_SMS = isOptOut;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                //objListOfContact[0].stPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();
                objListOfContact[0].ltPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();

                //20151006 - added to fix the bug that date of registration was not included;
                objListOfContact[0].dMembership_Validity = DateTime.Now.ToShortDateString();
                objListOfContact[0].plMembership_Status = "Member";

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactUpdate(inputContact);

                if (outputContact != null)
                {
                    if (outputContact.ListOfContact != null)
                    {
                        ContactV1 csContact = new ContactV1();
                        csContact.ContactId = outputContact.ListOfContact[0].ContactId;
                        csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                        csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;

                        arrContact.Add(csContact);
                    }
                }

            }
            catch (Exception ex)
            {
                ContactV1 csContact = new ContactV1();

                csContact.ContactMembershipId = pPartnerplusMembershipId;
                csContact.ErrorMessage = ex.Message;

                arrContact.Add(csContact);

                LogToFile("Error on updateExistingContactAsNewPartnerPlusRegistrant_MEA:" + pPartnerplusMembershipId + ":" + ex.Message);
            }
            return arrContact;
        }

        /* 20160708 RDVillamor
         * updateExistingContactAsNewPartnerPlusRegistrant_MEA - this method will be access if CRM validated that the registrant is an existing CRM Contact
         */
        public ArrayList updateExistingContactAsNewPartnerPlusRegistrant_MEA(OnDemandSession pSession, String pExistingContactId, String pFirstName, String pLastName, String pEmail, String pMobileNumber, String pCountry, String pBusinessName, String pAddress, String pPartnerplusMembershipId, String pOptIn)
        {
            String isOptOut = "";
            String vCountry = "";
            int iAddressLen = 0;

            ArrayList arrContact = new ArrayList();

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Output();

            try
            {
                iAddressLen = pAddress.Length;

                if (pCountry == "Guinea-Bissau")
                {
                    vCountry = "Guinea-bissau";
                }
                else if (pCountry == "Democratic Republic of the Congo")
                {
                    vCountry = "Congo, Democratic Republic of";
                }
                else if (pCountry == "Libya")
                {
                    vCountry = "Libyan Arab Jamahiriya";
                }
                else if (pCountry == "Tanzania")
                {
                    vCountry = "Tanzania, United Republic of";
                }
                else
                {
                    vCountry = pCountry;
                }

                if (pOptIn == "Y")
                {
                    isOptOut = "N";
                }
                else
                {
                    isOptOut = "Y";
                }

                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].ContactFirstName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim();
                objListOfContact[0].ContactLastName = capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].ContactEmail = pEmail.Trim();
                objListOfContact[0].ContactFullName = capitalizeFirstLetter(pFirstName.Replace("'", "''")).Trim() + " " + capitalizeFirstLetter(pLastName.Replace("'", "''")).Trim();
                objListOfContact[0].IndexedShortText1 = pPartnerplusMembershipId.Trim();
                objListOfContact[0].CellularPhone = "+" + pMobileNumber.Trim();
                objListOfContact[0].ContactId = pExistingContactId;
                objListOfContact[0].IndexedPick0 = "MEA";

                //insert address of account
                if (iAddressLen <= 40)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress;
                    objListOfContact[0].AlternateAddress2 = "";
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 40) && (iAddressLen <= 80))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40);
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 80) && (iAddressLen < 121))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80);
                }
                else if (iAddressLen > 120)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80, 40);
                }

                objListOfContact[0].AlternateCountry = vCountry;
                objListOfContact[0].bNever_SMS = isOptOut;
                objListOfContact[0].NeverEmail = isOptOut;
                objListOfContact[0].bNever_Phone = isOptOut;
                //objListOfContact[0].stPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();
                objListOfContact[0].ltPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();

                //20151006 - added to fix the bug that date of registration was not included;
                objListOfContact[0].dMembership_Validity = DateTime.Now.ToShortDateString();
                objListOfContact[0].plMembership_Status = "Member";

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactUpdate(inputContact);

                if (outputContact != null)
                {
                    if (outputContact.ListOfContact != null)
                    {
                        ContactV1 csContact = new ContactV1();
                        csContact.ContactId = outputContact.ListOfContact[0].ContactId;
                        csContact.ContactMembershipId = outputContact.ListOfContact[0].IndexedShortText1;
                        csContact.ContactDateOfRegistration = outputContact.ListOfContact[0].dMembership_Validity;

                        arrContact.Add(csContact);
                    }
                }

            }
            catch (Exception ex)
            {
                ContactV1 csContact = new ContactV1();

                csContact.ContactMembershipId = pPartnerplusMembershipId;
                csContact.ErrorMessage = ex.Message;

                arrContact.Add(csContact);

                LogToFile("Error on updateExistingContactAsNewPartnerPlusRegistrant_MEA:" + pPartnerplusMembershipId + ":" + ex.Message);
            }
            return arrContact;
        }

        /*20160512 / RDVillamor
        * This function is developed to flow-in the approved Partnerplus Asia registration using User Management
         * 
         * 20160617 / RDVillamor
         * 1. Included the First Name and Last name on the fields to flow-in from User Management ; at 6:40PM, first and last name has been removed on the fields being updated
        */
        public String processPartnerplusUserManagementApproval_Enhanced(OnDemandSession pSession, String pBusinessName, String pAccountId, String pContactId, String pMiddleName, String pMobileNumber,String pRoles,String pAccountType, String pAddress, String pCity, String pStateProvince, String pCountry, String pPartnerplusUserType)
        {
            String result="";
            ArrayList vAccountContactDetails = new ArrayList();
            String vAccountId = "";
            try
            {
                if (pAccountId == "New")
                {
                   vAccountId = createNewPartnerplusAccount_Enhanced(pSession, pBusinessName, pAccountType, pAddress, pStateProvince, pCity, pCountry, "", "Asia");

                   if (vAccountId.IndexOf("Error") >= 0)
                   {
                       result = vAccountId;
                       vAccountId = "";
                   }
                   else
                   {
                       result="Success";
                   }
                }
                else
                {
                    vAccountId = pAccountId;
                }

                result = updateApprovedPartnerplusContactRegistration_Asia(pSession, pContactId, vAccountId, pMiddleName, pMobileNumber, pRoles, pAccountType, pPartnerplusUserType, pBusinessName, pAddress, pCity, pStateProvince, pCountry);
            }
            catch (Exception ex)
            {
                result="Error: " + ex.Message;
                LogToFile("Error on processPartnerplusUserManagementApproval_Enhanced: " + pBusinessName + ", Contact Id: " + pContactId + "--> " + ex.Message);
            }

            return result;
        }

        /*20160708 / RDVillamor
        * This function is developed to flow-in the approved Partnerplus MEA registration using User Management
         * 
         * 20160708 / RDVillamor
         * 1. Included the First Name and Last name on the fields to flow-in from User Management ; 
        */
        public String processPartnerplusUserManagementApproval_Enhanced_MEA(OnDemandSession pSession, String pBusinessName, String pAccountId, String pContactId, String pMiddleName, String pMobileNumber, String pRoles, String pAccountType, String pAddress, String pCity, String pStateProvince, String pCountry, String pPartnerplusUserType)
        {
            String result = "";
            ArrayList vAccountContactDetails = new ArrayList();
            String vAccountId = "";
            try
            {
                if (pAccountId == "New")
                {
                    vAccountId = createNewPartnerplusAccount_Enhanced(pSession, pBusinessName, pAccountType, pAddress, pStateProvince, pCity, pCountry, "", "MEA");

                    if (vAccountId.IndexOf("Error") >= 0)
                    {
                        result = vAccountId;
                        vAccountId = "";
                    }
                    else
                    {
                        result = "Success";
                    }
                }
                else
                {
                    vAccountId = pAccountId;
                }

                result = updateApprovedPartnerplusContactRegistration_MEA(pSession, pContactId, vAccountId, pMiddleName, pMobileNumber, pRoles, pAccountType, pPartnerplusUserType, pBusinessName, pAddress, pCity, pStateProvince, pCountry);
                
            }
            catch (Exception ex)
            {
                result = "Error: " + ex.Message;
                LogToFile("Error on processPartnerplusUserManagementApproval_Enhanced: " + pBusinessName + ", Contact Id: " + pContactId + "--> " + ex.Message);
            }

            return result;
        }

        /*20160512 / RDVillamor
         * This function is developed to flow-in the approved Partnerplus Asia registration using User Management
         * 
         * 20160617 / RDVillamor
         * 1. Include the First Name and Last Name on the values saved on Contact Level, at6:40PM, first name and last name has been decided to be removed on the fields updated
         * 
         * 20160907 / RDVillamor
         * 1. Reactivated the saving of pAccountId as the new Primary Account Id
         */
        public String updateApprovedPartnerplusContactRegistration_Asia(OnDemandSession pSession, String pContactId, String pAccountId, String pMiddleName, String pMobileNumber, String pRoles, String pContactType, String pPartnerplusUserType, String pBusinessName, String pAddress, String pCity, String pStateProvince,String pCountry)
        {
            String vCountry = "";
            String vContactType = "";
            String result="";
            int iAddressLen = 0;

            ArrayList arrContact = new ArrayList();

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Output();

            WSOD_Contact_v1.ContactWS_ContactInsertChild_Input inputContactChild = new WSOD_Contact_v1.ContactWS_ContactInsertChild_Input();
            WSOD_Contact_v1.ContactWS_ContactInsertChild_Output outputContactChild = new WSOD_Contact_v1.ContactWS_ContactInsertChild_Output(); 
           
            try
            {
                iAddressLen = pAddress.Length;

                if ((pCountry == "South Korea"))
                {
                    vCountry = "Korea";
                }
                else
                {
                    vCountry = pCountry;
                }

                if (pContactType == "End-User")
                {
                    vContactType = "End User";
                }
                else if (pContactType.IndexOf("Dealer") >= 0)
                {
                    vContactType = "Dealer";
                }
                else if (pContactType.IndexOf("Contractor") >= 0)
                {
                    vContactType = "Contractor";
                }
                else if (pContactType.IndexOf("Wholesaler") >= 0)
                {
                    vContactType = "Wholesaler";
                }
                else if (pContactType.IndexOf("OEM") >= 0)
                {
                    vContactType = "OEM";
                }
                else
                {
                    vContactType = pContactType;
                }

                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].CellularPhone = "+" + pMobileNumber.Trim();
                objListOfContact[0].ContactId = pContactId;
                objListOfContact[0].ContactType = vContactType;
                
                ////20160907 - Reactivated, refer on item#1
                objListOfContact[0].AccountId = pAccountId;

                //insert address of account
                if (iAddressLen <= 40)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress;
                    objListOfContact[0].AlternateAddress2 = "";
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 40) && (iAddressLen <= 80))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40);
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 80) && (iAddressLen < 121))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80);
                }
                else if (iAddressLen > 120)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80, 40);
                }

                objListOfContact[0].AlternateCity = pCity;
                objListOfContact[0].AlternateProvince = pStateProvince;
                objListOfContact[0].AlternateCountry = vCountry;

                //objListOfContact[0].stPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'","''")).Trim();
                objListOfContact[0].ltPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();
                objListOfContact[0].stPartnerPlus_Roles_By_Country = pRoles;
                objListOfContact[0].plPartnerPlus_User_Type = pPartnerplusUserType;
                objListOfContact[0].MiddleName = pMiddleName;

                ////20160617, added first name and last name on the first being updated
                //objListOfContact[0].ContactFirstName = pFirstName.Replace("'", "''");
                //objListOfContact[0].ContactLastName = pLastName.Replace("'","''");

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactUpdate(inputContact);

                if (outputContact != null)
                {
                    ////20160907 - Commented, refer on item#1
                    //if (outputContact.ListOfContact != null)
                    //{

                    //    WSOD_Contact_v1.Contact1[] objListOfContactAccount = new WSOD_Contact_v1.Contact1[1];
                    //    objListOfContactAccount[0] = new WSOD_Contact_v1.Contact1();
                    //    objListOfContactAccount[0].ListOfAccount = new WSOD_Contact_v1.Account[1];
                    //    objListOfContactAccount[0].ListOfAccount[0] = new WSOD_Contact_v1.Account();

                    //    objListOfContactAccount[0].ContactId = pContactId;

                    //    objListOfContactAccount[0].ListOfAccount[0].AccountId = pAccountId;

                    //    inputContactChild.ListOfContact = objListOfContactAccount;

                    //    srvcContact.Url = pSession.GetURL();

                    //    //20160422: added this missing item on the code. This is a bug
                    //    srvcContact.CookieContainer = pSession.GetCookieContainer();

                    //    outputContactChild = srvcContact.ContactInsertChild(inputContactChild);

                    //    result = "Success";
                    //}

                    result = "Success";
                }
                else
                {
                    result = "No record updated";
                }
            }
            catch (Exception ex)
            {
                result = "Error on updateApprovedPartnerplusRegistration:" + ex.Message;
                LogToFile("Error on updateApprovedPartnerplusRegistration:" + pContactId + ":" + ex.Message);
            }
            return result;
        }

        /*20160708 / RDVillamor
         * This function is developed to flow-in the approved Partnerplus Asia registration using User Management
         */
        public String updateApprovedPartnerplusContactRegistration_MEA(OnDemandSession pSession, String pContactId, String pAccountId, String pMiddleName, String pMobileNumber, String pRoles, String pContactType, String pPartnerplusUserType, String pBusinessName, String pAddress, String pCity, String pStateProvince, String pCountry)
        {
            String vCountry = "";
            String vContactType = "";
            String result = "";
            int iAddressLen = 0;

            ArrayList arrContact = new ArrayList();

            WSOD_Contact_v1.Contact srvcContact = new WSOD_Contact_v1.Contact();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Input inputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Input();
            WSOD_Contact_v1.ContactWS_ContactUpdate_Output outputContact = new WSOD_Contact_v1.ContactWS_ContactUpdate_Output();

            WSOD_Contact_v1.ContactWS_ContactInsertChild_Input inputContactChild = new WSOD_Contact_v1.ContactWS_ContactInsertChild_Input();
            WSOD_Contact_v1.ContactWS_ContactInsertChild_Output outputContactChild = new WSOD_Contact_v1.ContactWS_ContactInsertChild_Output();

            try
            {
                iAddressLen = pAddress.Length;

                if (pCountry == "Democratic Republic of the Congo")
                {
                    vCountry = "Congo, Democratic Republic of";
                }
                else if (pCountry == "Libya")
                {
                    vCountry = "Libyan Arab Jamahiriya";
                }
                else if (pCountry == "Tanzania")
                {
                    vCountry = "Tanzania, United Republic of";
                }
                else
                {
                    vCountry = pCountry;
                }

                if (pContactType == "End-User")
                {
                    vContactType = "End User";
                }
                else if (pContactType.IndexOf("Dealer") >= 0)
                {
                    vContactType = "Dealer";
                }
                else if (pContactType.IndexOf("Contractor") >= 0)
                {
                    vContactType = "Contractor";
                }
                else if (pContactType.IndexOf("Wholesaler") >= 0)
                {
                    vContactType = "Wholesaler";
                }
                else if (pContactType.IndexOf("OEM") >= 0)
                {
                    vContactType = "OEM";
                }
                else
                {
                    vContactType = pContactType;
                }

                //Build the list
                WSOD_Contact_v1.Contact1[] objListOfContact = new WSOD_Contact_v1.Contact1[1];
                objListOfContact[0] = new WSOD_Contact_v1.Contact1();

                //Populate the list
                objListOfContact[0].CellularPhone = "+" + pMobileNumber.Trim();
                objListOfContact[0].ContactId = pContactId;
                objListOfContact[0].ContactType = vContactType;
                
                ////20160907 - Reactivated to set the new pAccountId
                objListOfContact[0].AccountId = pAccountId;

                //insert address of account
                if (iAddressLen <= 40)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress;
                    objListOfContact[0].AlternateAddress2 = "";
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 40) && (iAddressLen <= 80))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40);
                    objListOfContact[0].AlternateAddress3 = "";
                }
                else if ((iAddressLen > 80) && (iAddressLen < 121))
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80);
                }
                else if (iAddressLen > 120)
                {
                    objListOfContact[0].AlternateAddress1 = pAddress.Substring(0, 40);
                    objListOfContact[0].AlternateAddress2 = pAddress.Substring(40, 40);
                    objListOfContact[0].AlternateAddress3 = pAddress.Substring(80, 40);
                }

                objListOfContact[0].AlternateCity = pCity;
                objListOfContact[0].AlternateProvince = pStateProvince;
                objListOfContact[0].AlternateCountry = vCountry;

                //objListOfContact[0].stPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();
                objListOfContact[0].ltPartnerPlus_Business_Name = capitalizeFirstLetter(pBusinessName.Replace("'", "''")).Trim();
                objListOfContact[0].stPartnerPlus_Roles_By_Country = pRoles;
                objListOfContact[0].plPartnerPlus_User_Type = pPartnerplusUserType;
                objListOfContact[0].MiddleName = pMiddleName;
                objListOfContact[0].IndexedPick0 = "MEA";

                inputContact.ListOfContact = objListOfContact;

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContact = srvcContact.ContactUpdate(inputContact);

                if (outputContact != null)
                {
                    ////20160907 - Commented to set the AccountId as the Primary Account
                    //if (outputContact.ListOfContact != null)
                    //{

                    //    WSOD_Contact_v1.Contact1[] objListOfContactAccount = new WSOD_Contact_v1.Contact1[1];
                    //    objListOfContactAccount[0] = new WSOD_Contact_v1.Contact1();
                    //    objListOfContactAccount[0].ListOfAccount = new WSOD_Contact_v1.Account[1];
                    //    objListOfContactAccount[0].ListOfAccount[0] = new WSOD_Contact_v1.Account();

                    //    objListOfContactAccount[0].ContactId = pContactId;

                    //    objListOfContactAccount[0].ListOfAccount[0].AccountId = pAccountId;

                    //    inputContactChild.ListOfContact = objListOfContactAccount;

                    //    srvcContact.Url = pSession.GetURL();

                    //    //20160422: added this missing item on the code. This is a bug
                    //    srvcContact.CookieContainer = pSession.GetCookieContainer();

                    //    outputContactChild = srvcContact.ContactInsertChild(inputContactChild);

                    //    result = "Success";
                    //}

                    result = "Success";
                }
                else
                {
                    result = "No record updated";
                }
            }
            catch (Exception ex)
            {
                result = "Error on updateApprovedPartnerplusContactRegistration_MEA:" + ex.Message;
                LogToFile("Error on updateApprovedPartnerplusContactRegistration_MEA:" + pContactId + ":" + ex.Message);
            }
            return result;
        }


        /*20160512 / RDVillamor
         * This function is developed to handle creation of new account during the user management approval
         */
        public String createNewPartnerplusAccount_Enhanced(OnDemandSession pSession, String pAccountName, String pAccountType, String pAccountLocation, String pAStateProvince, String pACity, String pACountry, String pAPostalCode, String pGroupMember)
        {
            String accountId = "";
            int iAddressLen = 0;
            String vAccountType = "";
            String vCountry = "";
            String accountOwner = ConfigurationManager.AppSettings["defaultOwner"].ToString();

            WSOD_Account_v1.Account srvcAccount = new WSOD_Account_v1.Account();
            WSOD_Account_v1.AccountWS_AccountInsert_Input inputAccount = new WSOD_Account_v1.AccountWS_AccountInsert_Input();
            WSOD_Account_v1.AccountWS_AccountInsert_Output outputAccount = new WSOD_Account_v1.AccountWS_AccountInsert_Output();

            try
            {
                iAddressLen = pAccountLocation.Length;

                if (pAccountType == "End-User")
                {
                    vAccountType = "End User";
                }
                else if (pAccountType.IndexOf("Dealer") >= 0)
                {
                    vAccountType = "Dealer";
                }
                else
                {
                    vAccountType = pAccountType;
                }

                if (pACountry == "South Korea")
                {
                    vCountry = "Korea";
                }
                else if (pACountry == "Democratic Republic of the Congo")
                {
                    vCountry = "Congo, Democratic Republic of";
                }
                else if (pACountry == "Guinea-Bissau")
                {
                    vCountry = "Guinea-bissau";
                }
                else if (pACountry == "Libya")
                {
                    vCountry = "Libyan Arab Jamahiriya";
                }
                else if (pACountry == "Tanzania")
                {
                    vCountry = "Tanzania, United Republic of";
                }
                else
                {
                    vCountry = pACountry;
                }

                WSOD_Account_v1.Account1[] objListOfAccount = new WSOD_Account_v1.Account1[1];
                objListOfAccount[0] = new WSOD_Account_v1.Account1();

                objListOfAccount[0].AccountName = capitalizeFirstLetter(pAccountName.Replace("'","''")).Trim();
                objListOfAccount[0].AccountType = vAccountType;
                objListOfAccount[0].IndexedPick0 = pGroupMember; //"Asia";
                objListOfAccount[0].OwnerFullName = accountOwner;
                objListOfAccount[0].IndexedBoolean0 = "Y";
                objListOfAccount[0].Location = pACity;

                if (iAddressLen <= 40)
                {
                    objListOfAccount[0].PrimaryShipToStreetAddress = pAccountLocation.Substring(0);
                    objListOfAccount[0].PrimaryBillToStreetAddress = pAccountLocation.Substring(0);
                }
                else if ((iAddressLen > 40) && (iAddressLen <= 80))
                {
                    objListOfAccount[0].PrimaryShipToStreetAddress = pAccountLocation.Substring(0, 40);
                    objListOfAccount[0].PrimaryShipToStreetAddress2 = pAccountLocation.Substring(40);


                    objListOfAccount[0].PrimaryBillToStreetAddress = pAccountLocation.Substring(0,40);
                    objListOfAccount[0].PrimaryBillToStreetAddress2 = pAccountLocation.Substring(40);

                }
                else if ((iAddressLen > 80) && (iAddressLen < 121))
                {
                    objListOfAccount[0].PrimaryShipToStreetAddress = pAccountLocation.Substring(0,40);
                    objListOfAccount[0].PrimaryShipToStreetAddress2 = pAccountLocation.Substring(40,40);
                    objListOfAccount[0].PrimaryShipToStreetAddress3 = pAccountLocation.Substring(80);

                    objListOfAccount[0].PrimaryBillToStreetAddress = pAccountLocation.Substring(0, 40);
                    objListOfAccount[0].PrimaryBillToStreetAddress2 = pAccountLocation.Substring(40, 40);
                    objListOfAccount[0].PrimaryBillToStreetAddress3 = pAccountLocation.Substring(80);

                }
                else if ((iAddressLen > 120))
                {
                    objListOfAccount[0].Location = pAccountLocation.Substring(0, 40);
                    objListOfAccount[0].PrimaryShipToStreetAddress = pAccountLocation.Substring(0,40);
                    objListOfAccount[0].PrimaryShipToStreetAddress2 = pAccountLocation.Substring(40,40);
                    objListOfAccount[0].PrimaryShipToStreetAddress3 = pAccountLocation.Substring(80,40);

                    objListOfAccount[0].PrimaryBillToStreetAddress = pAccountLocation.Substring(0, 40);
                    objListOfAccount[0].PrimaryBillToStreetAddress2 = pAccountLocation.Substring(40, 40);
                    objListOfAccount[0].PrimaryBillToStreetAddress3 = pAccountLocation.Substring(80, 40);
                }

                objListOfAccount[0].PrimaryShipToCity = pACity;
                objListOfAccount[0].PrimaryShipToProvince = pAStateProvince;
                objListOfAccount[0].PrimaryShipToCountry = vCountry;
                objListOfAccount[0].PrimaryShipToPostalCode = pAPostalCode;

                objListOfAccount[0].PrimaryBillToCity = pACity;
                objListOfAccount[0].PrimaryBillToProvince = pAStateProvince;
                objListOfAccount[0].PrimaryBillToCountry = vCountry;
                objListOfAccount[0].PrimaryBillToPostalCode = pAPostalCode;

                inputAccount.ListOfAccount = objListOfAccount;

                srvcAccount.Url = pSession.GetURL();
                outputAccount = srvcAccount.AccountInsert(inputAccount);

                accountId = outputAccount.ListOfAccount[0].AccountId;
            }
            catch (Exception ex)
            {
                accountId = "Error:" + ex.Message;
                LogToFile("Error on createNewPartnerplusAccount_Enhanced:" + pAccountName + ":" + ex.Message);
            }

            return accountId;
        }

        /*
         * 20160513 / RDVillamor
         * This function is developed to get the list of Accounts that contains the charaters of the encoded Business Name
         */
        public ArrayList getCRMContactAccounts(OnDemandSession pSession,String pContactId)
        {
            ArrayList arrAccountDetails = new ArrayList();

            WSOD_Contact_v2.Contact srvcContact = new WSOD_Contact_v2.Contact();
            WSOD_Contact_v2.ContactQueryPage_Input inputContactQuery = new WSOD_Contact_v2.ContactQueryPage_Input();
            WSOD_Contact_v2.ContactQueryPage_Output outputContactQuery = new WSOD_Contact_v2.ContactQueryPage_Output();

            try
            {
                inputContactQuery.ListOfContact = new WSOD_Contact_v2.ListOfContactQuery();
                inputContactQuery.ListOfContact.Contact = new WSOD_Contact_v2.ContactQuery();
                inputContactQuery.ListOfContact.Contact.ListOfAccount = new WSOD_Contact_v2.ListOfAccountQuery();
                inputContactQuery.ListOfContact.Contact.ListOfAccount.Account = new WSOD_Contact_v2.AccountQuery();

                inputContactQuery.ListOfContact.pagesize = "1";
                inputContactQuery.ListOfContact.startrownum = "0";

                inputContactQuery.ListOfContact.Contact.ListOfAccount.pagesize = "100";
                inputContactQuery.ListOfContact.Contact.ListOfAccount.startrownum = "0";

                inputContactQuery.ListOfContact.Contact.Id = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.IndexedShortText0 = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactEmail = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactFirstName = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactLastName = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ContactType = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.CellularPhone = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.HomePhone = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.JobTitle = new WSOD_Contact_v2.queryType();

                inputContactQuery.ListOfContact.Contact.AccountId = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AccountLocation = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.AccountName = new WSOD_Contact_v2.queryType();

                inputContactQuery.ListOfContact.Contact.ListOfAccount.Account.AccountId = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ListOfAccount.Account.AccountName = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ListOfAccount.Account.AccountLocation = new WSOD_Contact_v2.queryType();
                inputContactQuery.ListOfContact.Contact.ListOfAccount.Account.PrimaryAccount = new WSOD_Contact_v2.queryType();
     
                inputContactQuery.ListOfContact.Contact.searchspec = "[Id]='" + pContactId + "'";

                if (pSession == null)
                {
                    pSession = EnterLoginCredentials();
                }

                srvcContact.Url = pSession.GetURL();
                srvcContact.CookieContainer = pSession.GetCookieContainer();

                outputContactQuery = srvcContact.ContactQueryPage(inputContactQuery);

                if (outputContactQuery != null)
                {
                    if (outputContactQuery.ListOfContact.Contact != null)
                    {
                        if (outputContactQuery.ListOfContact.Contact[0].ListOfAccount != null)
                        {
                            if (outputContactQuery.ListOfContact.Contact[0].ListOfAccount.Account.Length > 0)
                            {
                                for (int idx = 0; idx < outputContactQuery.ListOfContact.Contact[0].ListOfAccount.Account.Length; idx++)
                                {
                                    AccountV1 cAccounts = new AccountV1();
                                    cAccounts.AccountId = outputContactQuery.ListOfContact.Contact[0].ListOfAccount.Account[idx].AccountId;
                                    cAccounts.AccountName = outputContactQuery.ListOfContact.Contact[0].ListOfAccount.Account[idx].AccountName;
                                    cAccounts.AccountLocation = outputContactQuery.ListOfContact.Contact[0].ListOfAccount.Account[idx].AccountLocation;
                                    cAccounts.PrimaryAccount = outputContactQuery.ListOfContact.Contact[0].ListOfAccount.Account[idx].PrimaryAccount;

                                    arrAccountDetails.Add(cAccounts);
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                AccountV1 cAccounts = new AccountV1();
                cAccounts.ErrorMessage = ex.Message;

                arrAccountDetails.Add(cAccounts);
            }

            pSession.Destroy();

            return arrAccountDetails;
        }

        /*
         * 20160513 / RDVillamor
         * This function is developed to get the list of Accounts that is associated to the Partner+ registrant
         * 
         * Updates:
         * 20160524 by RDVillamor
         * 1. Use "New" as the Default Account Id
         */
        public ArrayList getCRMAccountsByBusinessName(OnDemandSession pSession, String pBusinessName, String pLocation)
        {
            ArrayList arrAccountDetails = new ArrayList();

            WSOD_Account_v1.Account srvcAccount = new WSOD_Account_v1.Account();
            WSOD_Account_v1.AccountWS_AccountQueryPage_Input inputAccountQuery = new WSOD_Account_v1.AccountWS_AccountQueryPage_Input();
            WSOD_Account_v1.AccountWS_AccountQueryPage_Output outputAccountQuery = new WSOD_Account_v1.AccountWS_AccountQueryPage_Output();

            try
            {
                inputAccountQuery.ListOfAccount = new WSOD_Account_v1.Account1[1];
                inputAccountQuery.ListOfAccount[0] = new WSOD_Account_v1.Account1();

                inputAccountQuery.PageSize = "100";
                inputAccountQuery.StartRowNum = "0";

                inputAccountQuery.ListOfAccount[0].AccountId = "";
                inputAccountQuery.ListOfAccount[0].AccountName = "~LIKE '" + pBusinessName.Replace("'", "''") + "*'";
                inputAccountQuery.ListOfAccount[0].Location = "";
                inputAccountQuery.ListOfAccount[0].Owner = "";
                inputAccountQuery.ListOfAccount[0].bGeneric_Account = "";

                if (pSession == null)
                {
                    pSession = EnterLoginCredentials();
                }

                srvcAccount.Url = pSession.GetURL();
                srvcAccount.CookieContainer = pSession.GetCookieContainer();

                outputAccountQuery = srvcAccount.AccountQueryPage(inputAccountQuery);

                if (outputAccountQuery != null)
                {
                    if (outputAccountQuery.ListOfAccount != null)
                    {
                        if (outputAccountQuery.ListOfAccount.Length > 0)
                        {
                            AccountV1 cAccounts = new AccountV1();
                            cAccounts.AccountName = pBusinessName;
                            cAccounts.AccountLocation = pLocation;

                            //enhanced on 20160524, see item#1
                            cAccounts.AccountId = "New";

                            arrAccountDetails.Add(cAccounts);

                            for (int idx = 0; idx < outputAccountQuery.ListOfAccount.Length; idx++)
                            {
                                AccountV1 cAccounts2 = new AccountV1();

                                if (outputAccountQuery.ListOfAccount[idx].bGeneric_Account != "Y")
                                {
                                    if ((outputAccountQuery.ListOfAccount[idx].AccountName.ToUpper() == pBusinessName.Trim().ToUpper()) && (outputAccountQuery.ListOfAccount[idx].Location.ToUpper() == pLocation.Trim().ToUpper()))
                                    {
                                        // update the assigned AccountId for the first record
                                        arrAccountDetails.RemoveAt(0);
                                        //arrAccountDetails.Insert(0, outputAccountQuery.ListOfAccount[idx].AccountId);

                                        cAccounts2.AccountName = outputAccountQuery.ListOfAccount[idx].AccountName;
                                        cAccounts2.AccountLocation = outputAccountQuery.ListOfAccount[idx].Location;
                                        cAccounts2.AccountId = outputAccountQuery.ListOfAccount[idx].AccountId;
                                        cAccounts2.GenericAccountFlag = outputAccountQuery.ListOfAccount[idx].bGeneric_Account;
                                    }
                                    else
                                    {
                                        cAccounts2.AccountName = outputAccountQuery.ListOfAccount[idx].AccountName;
                                        cAccounts2.AccountLocation = outputAccountQuery.ListOfAccount[idx].Location;
                                        cAccounts2.AccountId = outputAccountQuery.ListOfAccount[idx].AccountId;
                                        cAccounts2.GenericAccountFlag = outputAccountQuery.ListOfAccount[idx].bGeneric_Account;
                                    }

                                    arrAccountDetails.Add(cAccounts2);
                                }
                            }
                        }
                        else
                        {
                            AccountV1 cAccounts = new AccountV1();
                            cAccounts.AccountName = pBusinessName;
                            cAccounts.AccountLocation = pLocation;

                            //enhanced on 20160524, see item#1
                            cAccounts.AccountId = "New";

                            arrAccountDetails.Add(cAccounts);
                        }
                    }
                }
                else
                {
                    AccountV1 cAccounts = new AccountV1();
                    cAccounts.AccountName = pBusinessName;
                    cAccounts.AccountLocation = pLocation;
                    cAccounts.AccountId = "New";

                    arrAccountDetails.Add(cAccounts);
                }

            }
            catch (Exception ex)
            {
                AccountV1 cAccounts = new AccountV1();
                cAccounts.ErrorMessage = ex.Message;
                cAccounts.AccountName = pBusinessName;

                arrAccountDetails.Add(cAccounts);
            }

            return arrAccountDetails;
        }

        /*
         * 20160721 / RDVillamor
         * This is to process the deletion of the existing Contact Extension details associated to the contact, for the targetted Type
         */
        public String processDeleteOfExtistingAccountExtensionDetails(OnDemandSession pSession, String pContactId, String pAccountId)
        {
            // CustomObject10 = Account Extensions
            ArrayList arrAccountExtensions = new ArrayList();

            WSOD_AccountExtension_v2.CustomObject10 srvcAccountExt = new WSOD_AccountExtension_v2.CustomObject10();
            WSOD_AccountExtension_v2.CustomObject10QueryPage_Input inputQuery = new WSOD_AccountExtension_v2.CustomObject10QueryPage_Input();
            WSOD_AccountExtension_v2.CustomObject10QueryPage_Output outputQuery = new WSOD_AccountExtension_v2.CustomObject10QueryPage_Output();

            String result = "";

            try
            {
                inputQuery.ListOfCustomObject10 = new WSOD_AccountExtension_v2.ListOfCustomObject10Query();
                inputQuery.ListOfCustomObject10.CustomObject10 = new WSOD_AccountExtension_v2.CustomObject10Query();

                inputQuery.ListOfCustomObject10.pagesize = "100";
                inputQuery.ListOfCustomObject10.startrownum = "0";

                inputQuery.ListOfCustomObject10.CustomObject10.Id = new WSOD_AccountExtension_v2.queryType();
                inputQuery.ListOfCustomObject10.CustomObject10.Type = new WSOD_AccountExtension_v2.queryType();
                inputQuery.ListOfCustomObject10.CustomObject10.Name = new WSOD_AccountExtension_v2.queryType();
                inputQuery.ListOfCustomObject10.CustomObject10.ContactId = new WSOD_AccountExtension_v2.queryType();
                inputQuery.ListOfCustomObject10.CustomObject10.stoptimizedAttribute_01_Contact_Id = new WSOD_AccountExtension_v2.queryType();
                inputQuery.ListOfCustomObject10.CustomObject10.AccountId = new WSOD_AccountExtension_v2.queryType();

                if (pAccountId == "")
                {
                    inputQuery.ListOfCustomObject10.CustomObject10.searchspec = "[stoptimizedAttribute_01_Contact_Id]='" + pContactId + "' AND ([Type]='PartnerPlus Company Service' OR [Type]='PartnerPlus Customer Industry')";
                }
                else
                {
                    inputQuery.ListOfCustomObject10.CustomObject10.searchspec = "[AccountId]='" + pAccountId + "' AND ([Type]='PartnerPlus Company Service' OR [Type]='PartnerPlus Customer Industry')";
                }

                if (pSession == null)
                {
                    pSession = EnterLoginCredentials();
                }

                srvcAccountExt.Url = pSession.GetURL();
                srvcAccountExt.CookieContainer = pSession.GetCookieContainer();
                outputQuery = srvcAccountExt.CustomObject10QueryPage(inputQuery);

                if (outputQuery != null)
                {
                    if (outputQuery.ListOfCustomObject10.CustomObject10 != null)
                    {
                        if (outputQuery.ListOfCustomObject10.CustomObject10.Length > 0)
                        {
                            WSOD_AccountExtension_v2.CustomObject10Delete_Input inputDelete = new WSOD_AccountExtension_v2.CustomObject10Delete_Input();
                            WSOD_AccountExtension_v2.CustomObject10Delete_Output outputDelete = new WSOD_AccountExtension_v2.CustomObject10Delete_Output();

                            for(int idxCT=0; idxCT<outputQuery.ListOfCustomObject10.CustomObject10.Length; idxCT++)
                            {

                                inputDelete.ListOfCustomObject10 = new WSOD_AccountExtension_v2.ListOfCustomObject10Data();
                                inputDelete.ListOfCustomObject10.CustomObject10 = new WSOD_AccountExtension_v2.CustomObject10Data[1];
                                inputDelete.ListOfCustomObject10.CustomObject10[0] = new WSOD_AccountExtension_v2.CustomObject10Data();

                                inputDelete.ListOfCustomObject10.CustomObject10[0].Id = outputQuery.ListOfCustomObject10.CustomObject10[idxCT].Id;

                                if (pSession == null)
                                {
                                    pSession = EnterLoginCredentials();
                                }

                                srvcAccountExt.Url = pSession.GetURL();
                                srvcAccountExt.CookieContainer = pSession.GetCookieContainer();
                                outputDelete = srvcAccountExt.CustomObject10Delete(inputDelete);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogToFile("Error on processDeleteOfExtistingAccountExtensionDetails: " + pContactId + ": " + ex.Message.ToString());
                result = "Error on processDeleteOfExtistingAccountExtensionDetails: " + pContactId + ": " + ex.Message.ToString();
            }

            return result;
        }

        /*
         * 20160721 / RDVillamor
         * This is to process the deletion of the existing Contact Extension details associated to the contact, for the targetted Type
         */
        public String processDeleteOfExtistingContactExtensionDetails(OnDemandSession pSession, String pContactId)
        {
            // CustomObject11 = Contact Extensions
            ArrayList arrContactExtensions = new ArrayList();

            WSOD_ContactExtension_v21.CustomObject11 svcContactExt = new WSOD_ContactExtension_v21.CustomObject11();
            WSOD_ContactExtension_v21.CustomObject11QueryPage_Input inputQuery = new WSOD_ContactExtension_v21.CustomObject11QueryPage_Input();
            WSOD_ContactExtension_v21.CustomObject11QueryPage_Output outputQuery = new WSOD_ContactExtension_v21.CustomObject11QueryPage_Output();

            String result = "";

            try
            {
                inputQuery.ListOfCustomObject11 = new WSOD_ContactExtension_v21.ListOfCustomObject11Query();
                inputQuery.ListOfCustomObject11.CustomObject11 = new WSOD_ContactExtension_v21.CustomObject11Query();

                inputQuery.ListOfCustomObject11.pagesize = "100";
                inputQuery.ListOfCustomObject11.startrownum = "0";

                inputQuery.ListOfCustomObject11.CustomObject11.Id = new WSOD_ContactExtension_v21.queryType();
                inputQuery.ListOfCustomObject11.CustomObject11.Type = new WSOD_ContactExtension_v21.queryType();
                inputQuery.ListOfCustomObject11.CustomObject11.Name = new WSOD_ContactExtension_v21.queryType();
                inputQuery.ListOfCustomObject11.CustomObject11.ContactId = new WSOD_ContactExtension_v21.queryType();

                inputQuery.ListOfCustomObject11.CustomObject11.searchspec = "[ContactId]='" + pContactId + "' AND ([Type]='PartnerPlus Job Site Type' OR [Type]='PartnerPlus Areas to Improve' OR [Type]='PartnerPlus Primary Job Focus')";

                if (pSession == null)
                {
                    pSession = EnterLoginCredentials();
                }

                svcContactExt.Url = pSession.GetURL();
                svcContactExt.CookieContainer = pSession.GetCookieContainer();
                outputQuery = svcContactExt.CustomObject11QueryPage(inputQuery);

                if (outputQuery != null)
                {
                    if (outputQuery.ListOfCustomObject11.CustomObject11 != null)
                    {
                        if (outputQuery.ListOfCustomObject11.CustomObject11.Length > 0)
                        {
                            WSOD_ContactExtension_v21.CustomObject11Delete_Input inputDelete = new WSOD_ContactExtension_v21.CustomObject11Delete_Input();
                            WSOD_ContactExtension_v21.CustomObject11Delete_Output outputDelete = new WSOD_ContactExtension_v21.CustomObject11Delete_Output();

                            for (int idxCT = 0; idxCT < outputQuery.ListOfCustomObject11.CustomObject11.Length; idxCT++)
                            {

                                inputDelete.ListOfCustomObject11 = new WSOD_ContactExtension_v21.ListOfCustomObject11Data();
                                inputDelete.ListOfCustomObject11.CustomObject11 = new WSOD_ContactExtension_v21.CustomObject11Data[1];
                                inputDelete.ListOfCustomObject11.CustomObject11[0] = new WSOD_ContactExtension_v21.CustomObject11Data();

                                inputDelete.ListOfCustomObject11.CustomObject11[0].Id = outputQuery.ListOfCustomObject11.CustomObject11[idxCT].Id;

                                if (pSession == null)
                                {
                                    pSession = EnterLoginCredentials();
                                }

                                svcContactExt.Url = pSession.GetURL();
                                svcContactExt.CookieContainer = pSession.GetCookieContainer();
                                outputDelete = svcContactExt.CustomObject11Delete(inputDelete);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogToFile("Error on processDeleteOfExtistingContactExtensionDetails: " + pContactId + ": " + ex.Message.ToString());
                result = "Error on processDeleteOfExtistingContactExtensionDetails: " + pContactId + ": " + ex.Message.ToString();
            }

            return result;
        }

        /*
         * 20160721 / RDVillamor
         * This is to process the deletion of the existing Contact Extension details associated to the contact, for the targetted Type
         * 
         * Changes:
         * 20160809 / RDVillamor
         * 1. Hide the creation of Contact Extension records under Job Site type.
         * 
         * 20170109 / RDVillamor
         * 1. Include the Account Id on the fields being updated
         */
        public String processCreationOfContactExtensionDetails(OnDemandSession pSession, String pContactId, String pKeyAreasToImprove, String pPrimaryJobFocus, String pAccountId)
        {
            String result = "";

            WSOD_ContactExtension_v21.CustomObject11 svcContactExt = new WSOD_ContactExtension_v21.CustomObject11();
           
            try
            {
                WSOD_ContactExtension_v21.CustomObject11Insert_Input inputInsert = new WSOD_ContactExtension_v21.CustomObject11Insert_Input();
                WSOD_ContactExtension_v21.CustomObject11Insert_Output outputInsert = new WSOD_ContactExtension_v21.CustomObject11Insert_Output();

                if (pPrimaryJobFocus != "")
                {
                    inputInsert.ListOfCustomObject11 = new WSOD_ContactExtension_v21.ListOfCustomObject11Data();
                    inputInsert.ListOfCustomObject11.CustomObject11 = new WSOD_ContactExtension_v21.CustomObject11Data[1];
                    inputInsert.ListOfCustomObject11.CustomObject11[0] = new WSOD_ContactExtension_v21.CustomObject11Data();

                    inputInsert.ListOfCustomObject11.CustomObject11[0].ContactId = pContactId;
                    inputInsert.ListOfCustomObject11.CustomObject11[0].Name = transformSpecialCharacters(pPrimaryJobFocus);
                    inputInsert.ListOfCustomObject11.CustomObject11[0].Type = "PartnerPlus Primary Job Focus";

                    //see 20170109
                    inputInsert.ListOfCustomObject11.CustomObject11[0].AccountId = pAccountId;
                    //end

                    if (pSession == null)
                    {
                        pSession = EnterLoginCredentials();
                    }

                    svcContactExt.Url = pSession.GetURL();
                    svcContactExt.CookieContainer = pSession.GetCookieContainer();
                    outputInsert = svcContactExt.CustomObject11Insert(inputInsert);

                }

                ////20160809 - commented, see item#1
                //if ((pJobSiteType.Length > 0) && ((pJobSiteType != null) || (pJobSiteType != "")))
                //{
                //    string[] arrJobSiteType = pJobSiteType.Split(',');

                //    if (arrJobSiteType.Length > 0)
                //    {
                       
                //        for (int idxJS = 0; idxJS < arrJobSiteType.Length; idxJS++)
                //        {
                //            if (arrJobSiteType[idxJS] != "")
                //            {
                //                inputInsert.ListOfCustomObject11 = new WSOD_ContactExtension_v21.ListOfCustomObject11Data();
                //                inputInsert.ListOfCustomObject11.CustomObject11 = new WSOD_ContactExtension_v21.CustomObject11Data[1];
                //                inputInsert.ListOfCustomObject11.CustomObject11[0] = new WSOD_ContactExtension_v21.CustomObject11Data();

                //                inputInsert.ListOfCustomObject11.CustomObject11[0].ContactId = pContactId;
                //                inputInsert.ListOfCustomObject11.CustomObject11[0].Name = transformSpecialCharacters(arrJobSiteType[idxJS].ToString());
                //                inputInsert.ListOfCustomObject11.CustomObject11[0].Type = "PartnerPlus Job Site Type";

                //                if (pSession == null)
                //                {
                //                    pSession = EnterLoginCredentials();
                //                }

                //                svcContactExt.Url = pSession.GetURL();
                //                svcContactExt.CookieContainer = pSession.GetCookieContainer();
                //                outputInsert = svcContactExt.CustomObject11Insert(inputInsert);
                //            }
                //        }


                //    }
                //}

                if ((pKeyAreasToImprove.Length > 0) && ((pKeyAreasToImprove != null) || (pKeyAreasToImprove != "")))
                {
                    string[] arrKeyAreasToImprove = pKeyAreasToImprove.Split(',');

                    if (arrKeyAreasToImprove.Length > 0)
                    {
                        for (int idxKA = 0; idxKA < arrKeyAreasToImprove.Length; idxKA++)
                        {
                            if (arrKeyAreasToImprove[idxKA] != "")
                            {
                                inputInsert.ListOfCustomObject11 = new WSOD_ContactExtension_v21.ListOfCustomObject11Data();
                                inputInsert.ListOfCustomObject11.CustomObject11 = new WSOD_ContactExtension_v21.CustomObject11Data[1];
                                inputInsert.ListOfCustomObject11.CustomObject11[0] = new WSOD_ContactExtension_v21.CustomObject11Data();

                                inputInsert.ListOfCustomObject11.CustomObject11[0].ContactId = pContactId;
                                inputInsert.ListOfCustomObject11.CustomObject11[0].Name = transformSpecialCharacters(arrKeyAreasToImprove[idxKA].ToString());
                                inputInsert.ListOfCustomObject11.CustomObject11[0].Type = "PartnerPlus Areas to Improve";

                                if (pSession == null)
                                {
                                    pSession = EnterLoginCredentials();
                                }

                                svcContactExt.Url = pSession.GetURL();
                                svcContactExt.CookieContainer = pSession.GetCookieContainer();
                                outputInsert = svcContactExt.CustomObject11Insert(inputInsert);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogToFile("Error on processCreationOfContactExtensionDetails: " + pContactId + ": " + ex.Message.ToString());
                result = "Error on processCreationOfContactExtensionDetails: " + pContactId + ": " + ex.Message.ToString();
            }
            return result;
        }

        /*
         * 20160721 / RDVillamor
         * This is to process the creation of Account Extension details associated to the contact, for the targetted Type
         * 
         * Changes:
         * 20160809
         * 1. Hide the saving of PartnerPlus Customer Industry, as this should already be saving on Contact Extensions.PartnerPlus Job Site. 
         * 2. Activated the saving of PartnerPlus Customer Industry, as saving on Contact Extensions.PartnerPlus Job Site will now be hidden.
         * 
         * 20170109
         * 1. Add Contact Id to be populated when creating Account Extension record
         */
        public String processCreationOfAccountExtensionDetails(OnDemandSession pSession, String pContactId, String pAccountId, String pCompanyService, String pAJobSiteTypeORCustomerIndustry)
        {
            String result = "";

            WSOD_AccountExtension_v2.CustomObject10 svcAccountExt = new WSOD_AccountExtension_v2.CustomObject10();

            try
            {
                WSOD_AccountExtension_v2.CustomObject10Insert_Input inputInsert = new WSOD_AccountExtension_v2.CustomObject10Insert_Input();
                WSOD_AccountExtension_v2.CustomObject10Insert_Output outputInsert = new WSOD_AccountExtension_v2.CustomObject10Insert_Output();

                if ((pCompanyService.Length > 0) && ((pCompanyService != null) || (pCompanyService != "")))
                {
                    string[] arrCompanyService = pCompanyService.Split(',');

                    if (arrCompanyService.Length > 0)
                    {

                        for (int idxCS = 0; idxCS < arrCompanyService.Length; idxCS++)
                        {
                            if (arrCompanyService[idxCS] != "")
                            {
                                inputInsert.ListOfCustomObject10 = new WSOD_AccountExtension_v2.ListOfCustomObject10Data();
                                inputInsert.ListOfCustomObject10.CustomObject10 = new WSOD_AccountExtension_v2.CustomObject10Data[1];
                                inputInsert.ListOfCustomObject10.CustomObject10[0]= new WSOD_AccountExtension_v2.CustomObject10Data();

                                if (pAccountId != "")
                                {
                                    inputInsert.ListOfCustomObject10.CustomObject10[0].AccountId = pAccountId;
                                }

                                inputInsert.ListOfCustomObject10.CustomObject10[0].stoptimizedAttribute_01_Contact_Id = pContactId;
                                inputInsert.ListOfCustomObject10.CustomObject10[0].Name = transformSpecialCharacters(arrCompanyService[idxCS].ToString());
                                inputInsert.ListOfCustomObject10.CustomObject10[0].Type = "PartnerPlus Company Service";

                                //see 20170109 #1
                                inputInsert.ListOfCustomObject10.CustomObject10[0].ContactId = pContactId;
                                // end 

                                if (pSession == null)
                                {
                                    pSession = EnterLoginCredentials();
                                }

                                svcAccountExt.Url = pSession.GetURL();
                                svcAccountExt.CookieContainer = pSession.GetCookieContainer();
                                outputInsert = svcAccountExt.CustomObject10Insert(inputInsert);
                            }
                        }


                    }
                }

                //20160809, commented see item#1
                if ((pAJobSiteTypeORCustomerIndustry.Length > 0) && ((pAJobSiteTypeORCustomerIndustry != null) || (pAJobSiteTypeORCustomerIndustry != "")))
                {
                    string[] arrCustomerIndustry = pAJobSiteTypeORCustomerIndustry.Split(',');

                    if (arrCustomerIndustry.Length > 0)
                    {
                        for (int idxCI = 0; idxCI < arrCustomerIndustry.Length; idxCI++)
                        {
                            if (arrCustomerIndustry[idxCI] != "")
                            {
                                inputInsert.ListOfCustomObject10 = new WSOD_AccountExtension_v2.ListOfCustomObject10Data();
                                inputInsert.ListOfCustomObject10.CustomObject10 = new WSOD_AccountExtension_v2.CustomObject10Data[1];
                                inputInsert.ListOfCustomObject10.CustomObject10[0] = new WSOD_AccountExtension_v2.CustomObject10Data();

                                if (pAccountId != "")
                                {
                                    inputInsert.ListOfCustomObject10.CustomObject10[0].AccountId = pAccountId;
                                }

                                inputInsert.ListOfCustomObject10.CustomObject10[0].Name = transformSpecialCharacters(arrCustomerIndustry[idxCI].ToString());
                                inputInsert.ListOfCustomObject10.CustomObject10[0].Type = "PartnerPlus Customer Industry";
                                inputInsert.ListOfCustomObject10.CustomObject10[0].stoptimizedAttribute_01_Contact_Id = pContactId;

                                //see 20170109 #1
                                inputInsert.ListOfCustomObject10.CustomObject10[0].ContactId = pContactId;
                                // end 

                                if (pSession == null)
                                {
                                    pSession = EnterLoginCredentials();
                                }

                                svcAccountExt.Url = pSession.GetURL();
                                svcAccountExt.CookieContainer = pSession.GetCookieContainer();
                                outputInsert = svcAccountExt.CustomObject10Insert(inputInsert);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogToFile("Error on processCreationOfAccountExtensionDetails: " + pContactId + ": " + ex.Message.ToString());
                result = "Error on processCreationOfAccountExtensionDetails: " + pContactId + ": " + ex.Message.ToString();
            }
            return result;
        }

        public OnDemandSession EnterLoginCredentials()
        {
            String username = "";
            String password = "";
            String servername = "";
            OnDemandSession pSession = new OnDemandSession();
            try
            {
                // for testing purposes
                username = ConfigurationManager.AppSettings["username"].ToString();
                password = ConfigurationManager.AppSettings["password"].ToString();

                servername = ConfigurationManager.AppSettings["servername"].ToString();

                pSession = connectToCRMonDemand(servername, username, password);
            }
            catch (Exception ex)
            {
                pSession = null;
            }

            return pSession;
        }

        public static OnDemandSession connectToCRMonDemand(String pServerName, String pUsername, String pPassword)
        {
            OnDemandSession  mSession = new OnDemandSession();

            try
            {
                //Retrieve Connecton Parameters
                mSession.server = pServerName;
                mSession.username = pUsername;
                mSession.password = pPassword;

                mSession.Establish();
            }
            catch (Exception ex)
            {
                mSession = null;
            }

            return mSession;
        }

        public static void LogToFile(String strLogText)
        {
            String filename = "LogFile_CRMIntegration_" + DateTime.Today.ToString("yyyyMMdd") + ".txt";
            String path = ConfigurationManager.AppSettings["LogFilesFolder"].ToString();
            bool isPathExist = Directory.Exists(path);
            StreamWriter swLogTransaction;


            try
            {
                if (!isPathExist)
                {
                    Directory.CreateDirectory(path);
                }

                if (!File.Exists(path + filename))
                {
                    swLogTransaction = new StreamWriter(path + filename);
                }
                else
                {
                    swLogTransaction = File.AppendText(path + filename);
                }

                try
                {
                    //Write to the File
                    swLogTransaction.WriteLine(DateTime.Now + " -- " + strLogText);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
                finally
                {
                    swLogTransaction.Dispose();
                    swLogTransaction.Close();
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }
    }

    public class AccountV1
    {
        private String accountId;

        public String AccountId
        {
            get { return accountId; }
            set { accountId = value; }
        }

        private String accountName;

        public String AccountName
        {
            get { return accountName; }
            set { accountName = value; }
        }
        private String accountShipToCountry;

        public String AccountShipToCountry
        {
            get { return accountShipToCountry; }
            set { accountShipToCountry = value; }
        }
        private String accountCustomerId;

        public String AccountCustomerId
        {
            get { return accountCustomerId; }
            set { accountCustomerId = value; }
        }
        private String accountLocation;

        public String AccountLocation
        {
            get { return accountLocation; }
            set { accountLocation = value; }
        }
        private String accountOwnerId;

        public String AccountOwnerId
        {
            get { return accountOwnerId; }
            set { accountOwnerId = value; }
        }

        private String accountCustID;

        public String AccountCustID
        {
            get { return accountCustID; }
            set { accountCustID = value; }
        }

        //20160513 : RDVillamor, added to indicate if the record is the primary account of a contact
        private bool primaryAccount;

        public bool PrimaryAccount
        {
            get { return primaryAccount; }
            set { primaryAccount = value; }
        }

        private String errorMessage;

        public String ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }

        private String genericAccountFlag;

        public String GenericAccountFlag
        {
            get { return genericAccountFlag; }
            set { genericAccountFlag = value; }
        }


    }

    public class ContactV1
    {
        private String partnerplusUserType;

        public String PartnerplusUserType
        {
            get { return partnerplusUserType; }
            set { partnerplusUserType = value; }
        }

        private String partnerplusRole;

        public String PartnerplusRole
        {
            get { return partnerplusRole; }
            set { partnerplusRole = value; }
        }

        private String partnerplusMembershipStatus;

        public String PartnerplusMembershipStatus
        {
            get { return partnerplusMembershipStatus; }
            set { partnerplusMembershipStatus = value; }
        }

        private String errorMessage;

        public String ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; }
        }

        private String businessName;

        public String BusinessName
        {
            get { return businessName; }
            set { businessName = value; }
        }

        private String contactEmailAddress;
        public String ContactEmailAddress
        {
            get { return contactEmailAddress; }
            set { contactEmailAddress = value; }
        }

        private String contactId;
        public String ContactId
        {
            get { return contactId; }
            set { contactId = value; }
        }

        private String contactFullName;

        public String ContactFullName
        {
            get { return contactFullName; }
            set { contactFullName = value; }
        }
        private String contactFirstName;

        public String ContactFirstName
        {
            get { return contactFirstName; }
            set { contactFirstName = value; }
        }

        private String contactMiddleName;

        public String ContactMiddleName
        {
            get { return contactMiddleName; }
            set { contactMiddleName = value; }
        }

        private String contactLastName;

        public String ContactLastName
        {
            get { return contactLastName; }
            set { contactLastName = value; }
        }
        
        private String contactAccountName;

        public String ContactAccountName
        {
            get { return contactAccountName; }
            set { contactAccountName = value; }
        }

        ////added on 20140911 - Additional fields to Update Contact Address
        private String contactCountry;

        public String ContactCountry
        {
            get { return contactCountry; }
            set { contactCountry = value; }
        }
        private String contactAddress1;

        public String ContactAddress1
        {
            get { return contactAddress1; }
            set { contactAddress1 = value; }
        }
        private String contactAddress2;

        public String ContactAddress2
        {
            get { return contactAddress2; }
            set { contactAddress2 = value; }
        }

        private String contactAddress3;

        public String ContactAddress3
        {
            get { return contactAddress3; }
            set { contactAddress3 = value; }
        }


        private String contactCity;

        public String ContactCity
        {
            get { return contactCity; }
            set { contactCity = value; }
        }
        private String contactZIPCode;

        public String ContactZIPCode
        {
            get { return contactZIPCode; }
            set { contactZIPCode = value; }
        }
        private String contactState;

        public String ContactState
        {
            get { return contactState; }
            set { contactState = value; }
        }

        private String contactProvince;

        public String ContactProvince
        {
            get { return contactProvince; }
            set { contactProvince = value; }
        }

        private String contactPrimaryCountry;

        public String ContactPrimaryCountry
        {
            get { return contactPrimaryCountry; }
            set { contactPrimaryCountry = value; }
        }

        private String contactJobTitle;

        public String ContactJobTitle
        {
            get { return contactJobTitle; }
            set { contactJobTitle = value; }
        }

        private String contactAccountId;

        public String ContactAccountId
        {
            get { return contactAccountId; }
            set { contactAccountId = value; }
        }
        private String contactAccountLocation;

        public String ContactAccountLocation
        {
            get { return contactAccountLocation; }
            set { contactAccountLocation = value; }
        }

        private String contactAccountNumber;

        public String ContactAccountNumber
        {
            get { return contactAccountNumber; }
            set { contactAccountNumber = value; }
        }

        private String contactMembershipId;

        public String ContactMembershipId
        {
            get { return contactMembershipId; }
            set { contactMembershipId = value; }
        }

        private String contactYearsOfHVACExp;

        public String ContactYearsOfHVACExp
        {
            get { return contactYearsOfHVACExp; }
            set { contactYearsOfHVACExp = value; }
        }

        private String contactNeverEmail;

        public String ContactNeverEmail
        {
            get { return contactNeverEmail; }
            set { contactNeverEmail = value; }
        }

        private String contactNeverSMS;

        public String ContactNeverSMS
        {
            get { return contactNeverSMS; }
            set { contactNeverSMS = value; }
        }

        private String contactNeverPhone;

        public String ContactNeverPhone
        {
            get { return contactNeverPhone; }
            set { contactNeverPhone = value; }
        }

        private String contactType;

        public String ContactType
        {
            get { return contactType; }
            set { contactType = value; }
        }

        private String contactReferredBy;

        public String ContactReferredBy
        {
            get { return contactReferredBy; }
            set { contactReferredBy = value; }
        }

        private String contactListOfAccountId;

        public String ContactListOfAccountId
        {
            get { return contactListOfAccountId; }
            set { contactListOfAccountId = value; }
        }

        private String contactListOfAccountName;

        public String ContactListOfAccountName
        {
            get { return contactListOfAccountName; }
            set { contactListOfAccountName = value; }
        }

        private String contactListOfAccountLocation;

        public String ContactListOfAccountLocation
        {
            get { return contactListOfAccountLocation; }
            set { contactListOfAccountLocation = value; }
        }

        private String contactListOfAccountIsPrimary;

        public String ContactListOfAccountIsPrimary
        {
            get { return contactListOfAccountIsPrimary; }
            set { contactListOfAccountIsPrimary = value; }
        }

        private String contactPartnerWebComments;

        public String ContactPartnerWebComments
        {
            get { return contactPartnerWebComments; }
            set { contactPartnerWebComments = value; }
        }

        private String contactInterests;

        public String ContactInterests
        {
            get { return contactInterests; }
            set { contactInterests = value; }
        }

        private String contactMarket;

        public String ContactMarket
        {
            get { return contactMarket; }
            set { contactMarket = value; }
        }

        private String isCompressorContact;

        public String IsCompressorContact
        {
            get { return isCompressorContact; }
            set { isCompressorContact = value; }
        }

        private String isCondensingUnitsContact;

        public String IsCondensingUnitsContact
        {
            get { return isCondensingUnitsContact; }
            set { isCondensingUnitsContact = value; }
        }

        private String isControllersContact;

        public String IsControllersContact
        {
            get { return isControllersContact; }
            set { isControllersContact = value; }
        }

        private String isLineComponentsContact;

        public String IsLineComponentsContact
        {
            get { return isLineComponentsContact; }
            set { isLineComponentsContact = value; }
        }

        private String contactScopeOfWork;

        public String ContactScopeOfWork
        {
            get { return contactScopeOfWork; }
            set { contactScopeOfWork = value; }
        }

        private String contactDateOfRegistration;

        public String ContactDateOfRegistration
        {
            get { return contactDateOfRegistration; }
            set { contactDateOfRegistration = value; }
        }

        private String contactCellularPhone;

        public String ContactCellularPhone
        {
            get { return contactCellularPhone; }
            set { contactCellularPhone = value; }
        }

        private String contactListOfAccountCountry;

        public String ContactListOfAccountCountry
        {
            get { return contactListOfAccountCountry; }
            set { contactListOfAccountCountry = value; }
        }
       
        private String contactListOfAccountCity;

        public String ContactListOfAccountCity
        {
            get { return contactListOfAccountCity; }
            set { contactListOfAccountCity = value; }
        }
        
        private String contactListOfAccountAddress;

        public String ContactListOfAccountAddress
        {
            get { return contactListOfAccountAddress; }
            set { contactListOfAccountAddress = value; }
        }
        
        private String contactListOfAccountState;

        public String ContactListOfAccountState
        {
            get { return contactListOfAccountState; }
            set { contactListOfAccountState = value; }
        }
       
        private String contactListOfAccountProvince;

        public String ContactListOfAccountProvince
        {
            get { return contactListOfAccountProvince; }
            set { contactListOfAccountProvince = value; }
        }
        
        private String contactListOfAccountZIP;

        public String ContactListOfAccountZIP
        {
            get { return contactListOfAccountZIP; }
            set { contactListOfAccountZIP = value; }
        }
        private String contactListOfAccountType;

        public String ContactListOfAccountType
        {
            get { return contactListOfAccountType; }
            set { contactListOfAccountType = value; }
        }
       
        private String contactListOfAccountMainPhone;

        public String ContactListOfAccountMainPhone
        {
            get { return contactListOfAccountMainPhone; }
            set { contactListOfAccountMainPhone = value; }
        }
       
        private String contactListOfAccountIsBrandUseEmerson;

        public String ContactListOfAccountIsBrandUseEmerson
        {
            get { return contactListOfAccountIsBrandUseEmerson; }
            set { contactListOfAccountIsBrandUseEmerson = value; }
        }
       
        private String contactListOfAccountIfOthersBrandUseEmerson;

        public String ContactListOfAccountIfOthersBrandUseEmerson
        {
            get { return contactListOfAccountIfOthersBrandUseEmerson; }
            set { contactListOfAccountIfOthersBrandUseEmerson = value; }
        }
        
        private String contactListOfAccountBusinessIndustry;

        public String ContactListOfAccountBusinessIndustry
        {
            get { return contactListOfAccountBusinessIndustry; }
            set { contactListOfAccountBusinessIndustry = value; }
        }
        
        private String contactListOfAccountServicesOffered;

        public String ContactListOfAccountServicesOffered
        {
            get { return contactListOfAccountServicesOffered; }
            set { contactListOfAccountServicesOffered = value; }
        }

        private String contactListOfKeyAreasToImprove;

        public String ContactListOfKeyAreasToImprove
        {
            get { return contactListOfKeyAreasToImprove; }
            set { contactListOfKeyAreasToImprove = value; }
        }

        //private String contactListOfJobSiteType;

        //public String ContactListOfJobSiteType
        //{
        //    get { return contactListOfJobSiteType; }
        //    set { contactListOfJobSiteType = value; }
        //}

        private String contactListOfCompanyService;

        public String ContactListOfCompanyService
        {
            get { return contactListOfCompanyService; }
            set { contactListOfCompanyService = value; }
        }

        private String contactListOfCustomerIndustry;

        public String ContactListOfCustomerIndustry
        {
            get { return contactListOfCustomerIndustry; }
            set { contactListOfCustomerIndustry = value; }
        }

        private String contactMobileDevice;

        public String ContactMobileDevice
        {
            get { return contactMobileDevice; }
            set { contactMobileDevice = value; }
        }

        private String contactLanguage;

        public String ContactLanguage
        {
            get { return contactLanguage; }
            set { contactLanguage = value; }
        }

        private String contactHasMobileAccessOutsideOffice;

        public String ContactHasMobileAccessOutsideOffice
        {
            get { return contactHasMobileAccessOutsideOffice; }
            set { contactHasMobileAccessOutsideOffice = value; }
        }

        private String contactPrimaryJobFocus;

        public String ContactPrimaryJobFocus
        {
            get { return contactPrimaryJobFocus; }
            set { contactPrimaryJobFocus = value; }
        }

        private String jobFunction;

        public String JobFunction
        {
            get { return jobFunction; }
            set { jobFunction = value; }
        }

        private String jobSpecificRole;

        public String JobSpecificRole
        {
            get { return jobSpecificRole; }
            set { jobSpecificRole = value; }
        }

        public ContactV1()
        {

        }

    }

    public class ServiceRequest
    {
        private String serviceRequestContactId;

        public String ServiceRequestContactId
        {
            get { return serviceRequestContactId; }
            set { serviceRequestContactId = value; }
        }
        private String serviceRequestContactMembershipId;

        public String ServiceRequestContactMembershipId
        {
            get { return serviceRequestContactMembershipId; }
            set { serviceRequestContactMembershipId = value; }
        }
        private String serviceRequestId;

        public String ServiceRequestId
        {
            get { return serviceRequestId; }
            set { serviceRequestId = value; }
        }
        private String serviceRequestNumber;

        public String ServiceRequestNumber
        {
            get { return serviceRequestNumber; }
            set { serviceRequestNumber = value; }
        }
        private String serviceRequestStatus;

        public String ServiceRequestStatus
        {
            get { return serviceRequestStatus; }
            set { serviceRequestStatus = value; }
        }
        private String serviceRequestSubject;

        public String ServiceRequestSubject
        {
            get { return serviceRequestSubject; }
            set { serviceRequestSubject = value; }
        }
        private String serviceRequestOpenedTime;

        public String ServiceRequestOpenedTime
        {
            get { return serviceRequestOpenedTime; }
            set { serviceRequestOpenedTime = value; }
        }
        private String serviceRequestType;

        public String ServiceRequestType
        {
            get { return serviceRequestType; }
            set { serviceRequestType = value; }
        }

        private String serviceRequestArea;

        public String ServiceRequestArea
        {
            get { return serviceRequestArea; }
            set { serviceRequestArea = value; }
        }

        public ServiceRequest()
        {
 
        }
    }

    public class EventV1
    {
        private String eventId;

        public String EventId
        {
            get { return eventId; }
            set { eventId = value; }
        }
        private String eventName;

        public String EventName
        {
            get { return eventName; }
            set { eventName = value; }
        }

        private String eventAlternateName;

        public String EventAlternateName
        {
            get { return eventAlternateName; }
            set { eventAlternateName = value; }
        }

        private String eventObjective;

        public String EventObjective
        {
            get { return eventObjective; }
            set { eventObjective = value; }
        }
        private String eventStatus;

        public String EventStatus
        {
            get { return eventStatus; }
            set { eventStatus = value; }
        }
        private String eventContactName;

        public String EventContactName
        {
            get { return eventContactName; }
            set { eventContactName = value; }
        }
        private String eventStartDate;

        public String EventStartDate
        {
            get { return eventStartDate; }
            set { eventStartDate = value; }
        }
        private String eventEndDate;

        public String EventEndDate
        {
            get { return eventEndDate; }
            set { eventEndDate = value; }
        }
        private String eventOwner;

        public String EventOwner
        {
            get { return eventOwner; }
            set { eventOwner = value; }
        }

        private String eventDescription;

        public String EventDescription
        {
            get { return eventDescription; }
            set { eventDescription = value; }
        }

        private String inviteeContactId;

        public String InviteeContactId
        {
            get { return inviteeContactId; }
            set { inviteeContactId = value; }
        }

        private String inviteeStatus;

        public String InviteeStatus
        {
            get { return inviteeStatus; }
            set { inviteeStatus = value; }
        }

        private String eventType;

        public String EventType
        {
            get { return eventType; }
            set { eventType = value; }
        }

    }

    public class AccountExtension
    {
        private String accountExtensionRowId;

        public String AccountExtensionRowId
        {
            get { return accountExtensionRowId; }
            set { accountExtensionRowId = value; }
        }

        private String accountExtensionName;

        public String AccountExtensionName
        {
            get { return accountExtensionName; }
            set { accountExtensionName = value; }
        }

        private String accountExtensionType;

        public String AccountExtensionType
        {
            get { return accountExtensionType; }
            set { accountExtensionType = value; }
        }
    }

    public class LeadV1
    {
        private String leadId;

        public String LeadId
        {
            get { return leadId; }
            set { leadId = value; }
        }

        private String leadFirstName;

        public String LeadFirstName
        {
            get { return leadFirstName; }
            set { leadFirstName = value; }
        }

        private String leadLastName;

        public String LeadLastName
        {
            get { return leadLastName; }
            set { leadLastName = value; }
        }

        private String leadEmail;

        public String LeadEmail
        {
            get { return leadEmail; }
            set { leadEmail = value; }
        }

        private String leadCountry;

        public String LeadCountry
        {
            get { return leadCountry; }
            set { leadCountry = value; }
        }

        private String leadCompanyName;

        public String LeadCompanyName
        {
            get { return leadCompanyName; }
            set { leadCompanyName = value; }
        }

        private String leadJobCategory;

        public String LeadJobCategory
        {
            get { return leadJobCategory; }
            set { leadJobCategory = value; }
        }

        private String leadOwnerName;

        public String LeadOwnerName
        {
            get { return leadOwnerName; }
            set { leadOwnerName = value; }
        }

        private String leadIsOptOut;

        public String LeadIsOptOut
        {
            get { return leadIsOptOut; }
            set { leadIsOptOut = value; }
        }

        private String leadIndustry;

        public String LeadIndustry
        {
            get { return leadIndustry; }
            set { leadIndustry = value; }
        }

        private String leadType;

        public String LeadType
        {
            get { return leadType; }
            set { leadType = value; }
        }

        private String leadGroupMember;

        public String LeadGroupMember
        {
            get { return leadGroupMember; }
            set { leadGroupMember = value; }
        }

        private String leadNextStep;

        public String LeadNextStep
        {
            get { return leadNextStep; }
            set { leadNextStep = value; }
        }

        private String leadSource;

        public String LeadSource
        {
            get { return leadSource; }
            set { leadSource = value; }
        }

        private String leadSourceName;

        public String LeadSourceName
        {
            get { return leadSourceName; }
            set { leadSourceName = value; }
        }

    }

    public class OnDemandSession
    {
        public string server;
        public string username;
        public string password;
        public string port;
        public string sessionId;
        public Cookie cookie;

        private string httpBase = "https://";	// Use https for production implementations
        public void Establish()
        {
            // Check that username and password have been specified
            CheckUsernamePassword();

            // create a container for an HTTP request
            string userl = GetLogInURL();
            //System.Windows.Forms.MessageBox.Show(userl);
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(userl);
            //System.Windows.Forms.MessageBox.Show(req.Address.AbsoluteUri);
            //req.Method = "POST";

            // username and password are passed as HTTP headers
            req.Headers.Add("UserName", username);
            req.Headers.Add("Password", password);
            
            ////20160510: added by RDVillamor to allow login using stateless session
            req.Headers.Add("SessionType", "Stateless");

            // cookie container has to be added to request in order to 
            // retrieve the cookie from the response. 
            req.CookieContainer = new CookieContainer();

            // make the HTTP call
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            if (resp.StatusCode == System.Net.HttpStatusCode.OK)
            {
                // store cookie for later...
                cookie = resp.Cookies["JSESSIONID"];
                if (cookie == null)
                {
                    throw new Exception("No JSESSIONID cookie found in log-in response!");
                }
                sessionId = cookie.Value;
            }
        }

        public void Destroy()
        {
            // create a container for an HTTP request
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(GetLogOffURL());

            // reuse the cookie that was received at Login
            req.CookieContainer = new CookieContainer();
            req.CookieContainer.Add(cookie);

            // make the HTTP call
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
            if (resp.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception("Logging off failed!");
            }
            // forget current session id
            sessionId = null;
        }

        public CookieContainer GetCookieContainer()
        {
            // Returns a CookieContainer that can be used in a web services call
            if (sessionId == null)
            {
                throw new Exception("No session has been established!");
            }
            CookieContainer cc = new CookieContainer();
            Cookie sCookie = new Cookie("JSESSIONID", sessionId);
            sCookie.Domain = ".siebel.com";
            cc.Add(sCookie);

            return cc;
        }

        public string GetURL()
        {
            if (sessionId == null)
            {
                Establish();

                if (sessionId == null)
                {
                    throw new Exception("No session has been established!");
                }
            }
            //CheckServerPort();
            return httpBase + server + "/Services/Integration;jsessionid=" + sessionId;
        }
        private string GetLogInURL()
        {
            //CheckServerPort();

            //return httpBase + server + ":" + "/Services/Integration?command=login";
            return httpBase + server + "/Services/Integration?command=login";
            //return httpBase + server + ":" + port + "/OnDemand/logon.jsp";

        }
        private string GetLogOffURL()
        {
            //CheckServerPort();
            return httpBase + server + "/Services/Integration?command=logoff";
        }

        private void CheckUsernamePassword()
        {
            if (username == null)
            {
                throw new Exception("Username not specified!");
            }
            if (password == null)
            {
                throw new Exception("Password not specified!");
            }
        }
    }

    public class ResponseObject
    {
        private object responseItem;
        private string errorMessage;

        public object ResponseItem { get { return this.responseItem; } set { this.responseItem = value; } }
        public string ErrorMessage { get { return this.errorMessage; } set { this.errorMessage = value; } }

        public ResponseObject() { }
        public ResponseObject(object _responseItem, string _errorMessage)
        {
            this.responseItem = _responseItem;
            this.errorMessage = _errorMessage;
        }
    }
}