﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.IO;
using System.Text;

namespace CRMIntegration
{
    public partial class myPartnerFeedback : System.Web.UI.Page
    {
        public static CRM_myPartnerPlus_SRAssessment cCRM_myPartnerPlus_Assessment;
        public OnDemandSession mSession;

        protected void Page_Load(object sender, EventArgs e)
        {
            ////if (Page.IsPostBack == false)
            ////{
            //    mSession = new OnDemandSession();
            //    String servername = ConfigurationManager.AppSettings["survey_servername"].ToString();
            //    String surveryPage = "https://" + servername +"/OnDemand/user/SRCustSatSurveyEditPage?"; //https://secure-vmsomxmxa.crmondemand.com/OnDemand/user/SRCustSatSurveyEditPage?SRCustSatSurveyScriptEdit.Id=AMXA-4VVE01&OMRET0=ServiceRequestHomepage&OCNOEDITTYPE=Y&ServiceRequestDetailForm.Id=AMXA-4SGFPW&ocEdit=Y&AssessmentDetailId=AMXA-4VVE01&OMCR0=AMXA-4VVE01&OMTGT=SRCustSatSurveyScriptEdit&OMTHD=SalesAssessmentEditNav&IsAssessmentNew=Y&OMCBO=OnDemand%20Sales%20Assessment
            //    String redirectPage = "";

            //    if (Request.QueryString["sid"] != null)
            //    {
            //        String srId = Request.QueryString["sid"].ToString();
            //        String assessmentTemplateId = ConfigurationManager.AppSettings["myPartnerPlusFeedbackAssessmentTemplateId"].ToString(); //Request.QueryString["tid"].ToString();
            //        WSOD_ServiceRequest_v2.ServiceRequestData retrievedSRData = new WSOD_ServiceRequest_v2.ServiceRequestData();
            //        WSOD_ServiceRequest_v2.ListOfSalesAssessmentData retrievedSRAssessmentValueData = new WSOD_ServiceRequest_v2.ListOfSalesAssessmentData();
            //        String salesAssessmentTemplateId = "";
            //        try
            //        {
            //            mSession = EnterLoginCredentials();

            //            retrievedSRData = getSRDetails(srId, mSession);
                        
            //            //insert sales assessment record
            //            salesAssessmentTemplateId = createSRAssessmentRecord(mSession, retrievedSRData.Id, retrievedSRData.Subject, retrievedSRData.Status, retrievedSRData.AccountId, retrievedSRData.OwnerId, assessmentTemplateId).SalesAssessment[0].Id ;

            //            redirectPage = surveryPage + "&SRCustSatSurveyScriptEdit.Id=" + salesAssessmentTemplateId + "&ServiceRequestDetailForm.Id=" + srId + "&AssessmentDetailId=" + salesAssessmentTemplateId + "&OMCR0=" + salesAssessmentTemplateId +"&OMRET0=ServiceRequestHomepage&OCNOEDITTYPE=Y&ocEdit=Y&OMTGT=SRCustSatSurveyScriptEdit&OMTHD=SalesAssessmentEditNav&IsAssessmentNew=Y&OMCBO=OnDemand%20Sales%20Assessment&ssoid=emersoncc";

            //            //open SR-Assessment Page link, as if logging in using username and password
            //            Response.Redirect(redirectPage,false);

            //        }
            //        catch (Exception ex)
            //        {

            //        }
            //    }
            ////}
            
        }

        public WSOD_ServiceRequest_v2.ServiceRequestData getSRDetails(String pServiceRequestRowId, OnDemandSession pSession)
        {
            mSession = new OnDemandSession();

            WSOD_ServiceRequest_v2.ServiceRequest srvcService = new WSOD_ServiceRequest_v2.ServiceRequest();
            WSOD_ServiceRequest_v2.ServiceRequestQueryPage_Input inputSR = new WSOD_ServiceRequest_v2.ServiceRequestQueryPage_Input();
            WSOD_ServiceRequest_v2.ServiceRequestQueryPage_Output outputSR = new WSOD_ServiceRequest_v2.ServiceRequestQueryPage_Output();

            inputSR.ViewMode = "AllBooks";
            inputSR.ListOfServiceRequest = new WSOD_ServiceRequest_v2.ListOfServiceRequestQuery();

            inputSR.ListOfServiceRequest.pagesize = "1";
            inputSR.ListOfServiceRequest.startrownum = "0";

            inputSR.ListOfServiceRequest.ServiceRequest = new WSOD_ServiceRequest_v2.ServiceRequestQuery();
            inputSR.ListOfServiceRequest.ServiceRequest.Id = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.AccountId = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.ContactEmail = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.Owner = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.Subject = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.Status = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.SRNumber = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.OwnerId = new WSOD_ServiceRequest_v2.queryType();

            inputSR.ListOfServiceRequest.ServiceRequest.searchspec = "[Id]='" + pServiceRequestRowId+ "'";

            pSession = EnterLoginCredentials();

            srvcService.Url = pSession.GetURL();
            srvcService.CookieContainer = pSession.GetCookieContainer();
            outputSR = srvcService.ServiceRequestQueryPage(inputSR);

            if (outputSR != null)
            {
                return outputSR.ListOfServiceRequest.ServiceRequest[0];
            }
            else
            {

                return null;
            }
        }

        public WSOD_ServiceRequest.ListOfSalesAssessmentData createSRAssessmentRecord(OnDemandSession pSession, String pServiceRequestRowId, String pSubject, String pStatus, String pAccountId, String pOwnerId, String pAssessmentTemplateId)
        {
            mSession = new OnDemandSession();

            try
            {
                WSOD_ServiceRequest.ServiceRequest srvcService = new WSOD_ServiceRequest.ServiceRequest();
                WSOD_ServiceRequest.ServiceRequestExecute_Input inputSRAssessment = new WSOD_ServiceRequest.ServiceRequestExecute_Input();
                WSOD_ServiceRequest.ServiceRequestExecute_Output outputSRAssessment = new WSOD_ServiceRequest.ServiceRequestExecute_Output();

                inputSRAssessment.LOVLanguageMode = "LIC";
                inputSRAssessment.ViewMode = "AllBooks";
                inputSRAssessment.Echo = "On";

                inputSRAssessment.ListOfServiceRequest = new WSOD_ServiceRequest.ListOfServiceRequestData();
                inputSRAssessment.ListOfServiceRequest.ServiceRequest = new WSOD_ServiceRequest.ServiceRequestData[1];
                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0] = new WSOD_ServiceRequest.ServiceRequestData();
                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].operation = "skipnode";
                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].Id = pServiceRequestRowId;
                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].Subject = pSubject;
                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].Status = pStatus;
                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].OwnerId = pOwnerId;

                if (pAccountId != "" || pAccountId != null)
                {
                    inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].AccountId = pAccountId;
                }

                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].ListOfSalesAssessment = new WSOD_ServiceRequest.ListOfSalesAssessmentData();
                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].ListOfSalesAssessment.SalesAssessment = new WSOD_ServiceRequest.SalesAssessmentData[1];
                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].ListOfSalesAssessment.SalesAssessment[0] = new WSOD_ServiceRequest.SalesAssessmentData();
                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].ListOfSalesAssessment.SalesAssessment[0].operation = "insert";
                inputSRAssessment.ListOfServiceRequest.ServiceRequest[0].ListOfSalesAssessment.SalesAssessment[0].TemplateId = pAssessmentTemplateId;

                if (pSession == null)
                {
                    pSession = EnterLoginCredentials();
                }

                srvcService.Url = pSession.GetURL();
                srvcService.CookieContainer = pSession.GetCookieContainer();
                outputSRAssessment = srvcService.ServiceRequestExecute(inputSRAssessment);

                if (outputSRAssessment != null)
                {
                    if (outputSRAssessment.ListOfServiceRequest.ServiceRequest != null)
                    {
                        return outputSRAssessment.ListOfServiceRequest.ServiceRequest[0].ListOfSalesAssessment; // outputSRAssessment.ListOfServiceRequest.ServiceRequest[0].ListOfSalesAssessment;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                LogToFile("Error:" + ex.Message);
                return null;
            }



        }


        public OnDemandSession EnterLoginCredentials()
        {
            String username = "";
            String password = "";
            String servername = "";
            OnDemandSession pSession = new OnDemandSession();
            try
            {
                // for testing purposes
                username = ConfigurationManager.AppSettings["survey_username"].ToString();
                password = ConfigurationManager.AppSettings["survey_password"].ToString();

                servername = ConfigurationManager.AppSettings["survey_servername"].ToString();

                pSession = connectToCRMonDemand(servername, username, password);
            }
            catch (Exception ex)
            {
                pSession = null;
            }

            return pSession;
        }

        public static OnDemandSession connectToCRMonDemand(String pServerName, String pUsername, String pPassword)
        {
            OnDemandSession  mSession = new OnDemandSession();

            try
            {
                //Retrieve Connecton Parameters
                mSession.server = pServerName;
                mSession.username = pUsername;
                mSession.password = pPassword;
               
                mSession.Establish();
            }
            catch (Exception ex)
            {
                mSession = null;
            }

            return mSession;
        }

        public static void LogToFile(String strLogText)
        {
            String filename = "LogFile_CRM_PP_SRAssessment_Integration_" + DateTime.Today.ToString("yyyyMMdd") + ".txt";
            String path = ConfigurationManager.AppSettings["LogFilesFolder"].ToString();
            bool isPathExist = Directory.Exists(path);
            StreamWriter swLogTransaction;


            try
            {
                if (!isPathExist)
                {
                    Directory.CreateDirectory(path);
                }

                if (!File.Exists(path + filename))
                {
                    swLogTransaction = new StreamWriter(path + filename);
                }
                else
                {
                    swLogTransaction = File.AppendText(path + filename);
                }

                try
                {
                    //Write to the File
                    swLogTransaction.WriteLine(DateTime.Now + " -- " + strLogText);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
                finally
                {
                    swLogTransaction.Dispose();
                    swLogTransaction.Close();
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }
    }

}