﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using System.Xml.Linq;
using System.Xml;
using System.Net;


namespace CRMIntegration
{
    /// <summary>
    /// Summary description for CRMoDIntegration
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    
    [System.Web.Script.Services.ScriptService]
    [Serializable]

    public class CRMoDIntegration : System.Web.Services.WebService
    {

        //[WebMethod]
        //public string HelloWorld()
        //{
        //    return "Hello World";
        //}

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public ResponseObject saveNintexRequestForPricing(String accountID, String requestStatus,String nintexRequestID, String nintexRequester, String nintexRequestURL)
        //{
        //    ResponseObject ro = new ResponseObject();

        //    try
        //    {
        //        DataAccess da = new DataAccess();
        //        ro.ResponseItem = da.saveNintexRequestForPricing(accountID, requestStatus, nintexRequestID, nintexRequester, nintexRequestURL);
        //    }
        //    catch (Exception ex)
        //    {
        //        ro.ErrorMessage = ex.Message;
        //    }

        //    return ro;
        //}

        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        //public ResponseObject getAccountDetails(String pAccountRowId, String pOSCReferenceNumber)
        //{
        //    ResponseObject ro = new ResponseObject();
        //    try
        //    {
        //        DataAccess da = new DataAccess();
        //        ro.ResponseItem = da.getAccountDetails(pAccountRowId, pOSCReferenceNumber);
        //    }
        //    catch (Exception ex)
        //    {
        //        ro.ErrorMessage = ex.Message;
        //    }

        //    return ro;
        //}
        
        [WebMethod]
        [ScriptMethod(UseHttpGet = true)]
        public String postInviteeStatus(String crmContactRowId, String crmEventRowId, String crmEventInviteeStatus,String crmEventInviteeSource, String EmailAddress)
        {
            String result="";
            try
            {
                DataAccess da = new DataAccess();
                result = da.postInviteeStatus(crmContactRowId, crmEventRowId, crmEventInviteeStatus, crmEventInviteeSource, EmailAddress);
            }
            catch(Exception ex)
            {
                result = "Failed: " + ex.Message;
            }
            
            return result;
        }

        /*
         * 20170126 - RDVillamor
         * Created to save responses into XML file
         */
        [WebMethod]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json,UseHttpGet=false)]
        public void saveMyPartnerPlusSurveyResponsesToXML(String pSRRowId, String pTemplateId, String pSRSubject, String pQ1Response, String pQ2Response, String pQ3Response)
        {
            CRM_myPartnerPlus_SRAssessment cSRAssessment = new CRM_myPartnerPlus_SRAssessment();

            cSRAssessment.saveMyPartnerPlusSurveyResponsesToXML(pSRRowId, pTemplateId, pSRSubject, pQ1Response, pQ2Response, pQ3Response);
            
        }

    }
}
