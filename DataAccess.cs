﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections;
using System.Web.SessionState;
using System.IO;
using System.Text;
using System.Web.SessionState;
using System.Net;

namespace CRMIntegration
{
    public class DataAccess
    {
        public static OnDemandSession mSession ;

        public static String username = "";
        public static String password = "";
        public static String servername = "";

        protected static void LogToFile(String strLogText)
        {
            String filename = "LogFile_CRM_ODM_" + DateTime.Today.ToString("yyyyMMdd") + ".txt";
            String path = ConfigurationManager.AppSettings["LogFilesFolder"].ToString();
            bool isPathExist = Directory.Exists(path);
            StreamWriter swLogTransaction;

            try
            {
                if (!isPathExist)
                {
                    Directory.CreateDirectory(path);
                }

                if (!File.Exists(path + filename))
                {
                    swLogTransaction = new StreamWriter(path + filename);
                }
                else
                {
                    swLogTransaction = File.AppendText(path + filename);
                }

                try
                {
                    //Write to the File
                    swLogTransaction.WriteLine(DateTime.Now + " -- " + strLogText);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
                finally
                {
                    swLogTransaction.Dispose();
                    swLogTransaction.Close();
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
        }

        //public String enrollNewSubscriberAsLead(String pFirstName, String pLastName, String pEmailAddress, String pCompanyName, String pCountry)
        //{
        //    String result = "";
        //    WSOD_Lead_v1.Lead srvcLead = new WSOD_Lead_v1.Lead();
        //    WSOD_Lead_v1.LeadWS_LeadInsert_Input inputNewLead = new WSOD_Lead_v1.LeadWS_LeadInsert_Input();
        //    WSOD_Lead_v1.LeadWS_LeadInsert_Output outputNewLead = new WSOD_Lead_v1.LeadWS_LeadInsert_Output();

        //    WSOD_Lead_v1.LeadWS_LeadQueryPage_Input inputQueryLead = new WSOD_Lead_v1.LeadWS_LeadQueryPage_Input();
        //    WSOD_Lead_v1.LeadWS_LeadQueryPage_Output outputQueryLead = new WSOD_Lead_v1.LeadWS_LeadQueryPage_Output();

        //    try
        //    {   
        //        //Build the list
        //        WSOD_Lead_v1.Lead1[] objListOfLead = new WSOD_Lead_v1.Lead1[1];
        //        objListOfLead[0] = new WSOD_Lead_v1.Lead1();

        //        //Populate the list
        //        objListOfLead[0].LeadFirstName = pFirstName;
        //        objListOfLead[0].LeadLastName = pLastName;
        //        objListOfLead[0].LeadEmail = pEmailAddress;
        //        objListOfLead[0].Company = pCompanyName;
        //        objListOfLead[0].Source = ConfigurationManager.AppSettings["defaultLeadSource"].ToString();
        //        objListOfLead[0].plLead_Source_Name = ConfigurationManager.AppSettings["defaultLeadSourceName"].ToString();
        //        objListOfLead[0].LeadOwner = ConfigurationManager.AppSettings["defaultLeadOwner"].ToString();
        //        objListOfLead[0].IndexedPick0 = "Contractor & Wholesaler";
        //        objListOfLead[0].msplPermission_Marketing_Target = ConfigurationManager.AppSettings["defaultLeadMarketingTarget"].ToString();
        //        objListOfLead[0].Country = pCountry;

        //        inputNewLead.ListOfLead = objListOfLead;

        //        EnterLoginCredentials();

        //        srvcLead.Url = mSession.GetURL();
        //        outputNewLead = srvcLead.LeadInsert(inputNewLead);

        //        result = "L," + outputNewLead.ListOfLead[0].LeadId;

        //        //result = "Success";

        //    }
        //    catch (Exception ex)
        //    {
        //        LogToFile("Error on enrollNewSubscriberAsLead: --> " + ex.Message);
        //        result = ex.Message;
        //    }

        //    return result;
        //}

        public String saveNintexRequestForPricing(String pAccountID, String pNintexRequestStatus, String pNintexRequestID, String pNintexRequester, String pNintexRequestURL)
        {
            String result = "";

            WSDL_Activity_v1.Activity srvcActivity = new WSDL_Activity_v1.Activity();
            WSDL_Activity_v1.ActivityNWS_Activity_Insert_Input inputActivity = new WSDL_Activity_v1.ActivityNWS_Activity_Insert_Input();
            WSDL_Activity_v1.ActivityNWS_Activity_Insert_Output outputActivity = new WSDL_Activity_v1.ActivityNWS_Activity_Insert_Output();
            WSDL_Activity_v1.Activity1[] objListOfActivity = new WSDL_Activity_v1.Activity1[1];
            
            try
            {
                objListOfActivity[0] = new WSDL_Activity_v1.Activity1();

                objListOfActivity[0].Activity = "Task";
                objListOfActivity[0].Type = "Special Pricing";
                objListOfActivity[0].Status = "Completed";
                objListOfActivity[0].Subject = "Request for Special Pricing";
                objListOfActivity[0].Description = pNintexRequestURL.ToString();
                objListOfActivity[0].plNintex_Request_Status = pNintexRequestStatus.ToString();
                objListOfActivity[0].stNintex_Requester = pNintexRequester.ToString();
                objListOfActivity[0].AccountId = pAccountID.ToString();
                objListOfActivity[0].ExternalSystemId = pNintexRequestID.ToString();
                objListOfActivity[0].OwnerId = ConfigurationManager.AppSettings["defaultOwnerId"].ToString();

                inputActivity.ListOfActivity = objListOfActivity;

                EnterLoginCredentials();

                srvcActivity.Url = mSession.GetURL();
                srvcActivity.CookieContainer = mSession.GetCookieContainer();
                outputActivity = srvcActivity.Activity_Insert(inputActivity);

                if (outputActivity != null)
                {
                    result = "Success";
                }
                else
                {
                    result = "Failed";
                }


            }
            catch (Exception ex)
            {
                LogToFile("Error on saveNintexRequestForPricing: " + ex.Message);
                result = "Failed";
            }

            return result;
        }

        public ArrayList getAccountDetails(String pAccountRowId, String pOSCReferenceNumber)
        {
            AccountWS.Account srvcAccount = new AccountWS.Account();
            AccountWS.AccountQueryPage_Input inputAccountQuery = new AccountWS.AccountQueryPage_Input();
            AccountWS.AccountQueryPage_Output outputAccountQuery = new AccountWS.AccountQueryPage_Output();
            ArrayList alAccountDetails = new ArrayList();

            try
            {
                inputAccountQuery.ListOfAccount = new AccountWS.ListOfAccountQuery();
                inputAccountQuery.ListOfAccount.Account = new AccountWS.AccountQuery();

                inputAccountQuery.ListOfAccount.pagesize = "100";
                inputAccountQuery.ListOfAccount.startrownum = "0";

                inputAccountQuery.ListOfAccount.Account.AccountName = new AccountWS.queryType();
                inputAccountQuery.ListOfAccount.Account.Location = new AccountWS.queryType();
                inputAccountQuery.ListOfAccount.Account.Id = new AccountWS.queryType();
                inputAccountQuery.ListOfAccount.Account.IndexedPick0 = new AccountWS.queryType();
                inputAccountQuery.ListOfAccount.Account.Owner = new AccountWS.queryType();
                inputAccountQuery.ListOfAccount.Account.stOracle_Service_Cloud_Reference_No = new AccountWS.queryType();
                inputAccountQuery.ListOfAccount.Account.ModifiedDate = new AccountWS.queryType();

                inputAccountQuery.ListOfAccount.Account.searchspec = "[Id]='" + pAccountRowId + "' OR [stOracle_Service_Cloud_Reference_No]='" + pOSCReferenceNumber + "'";

                EnterLoginCredentials();

                srvcAccount.Url = mSession.GetURL();
                srvcAccount.CookieContainer = mSession.GetCookieContainer();

                outputAccountQuery = srvcAccount.AccountQueryPage(inputAccountQuery);

                if (outputAccountQuery != null)
                {
                    Account csAccount = new Account();

                    csAccount.AccountName = outputAccountQuery.ListOfAccount.Account[0].AccountName;
                    csAccount.Location = outputAccountQuery.ListOfAccount.Account[0].Location;
                    csAccount.AccountRowId = outputAccountQuery.ListOfAccount.Account[0].Id;

                    alAccountDetails.Add(csAccount);
                }

            }
            catch (Exception ex)
            {
                alAccountDetails = null;
            }


            return alAccountDetails;
        }

        ////20160316 - RDVillamor
        //public String postInviteeStatus(String pContactId, String pEventId, String pInviteeStatus, String pInviteeSource)
        //{ 
        //    String result = "";


        //    return result;
        //}

        //20160314 - RDVillamor
        public String insertInviteeStatus(String pContactId, String pEventId, String pInviteeStatus, String pInviteeSource)
        {
            String result = "";
            try
            {
                WSOD_Event_v1.MedEd srvcEvent = new WSOD_Event_v1.MedEd();
                WSOD_Event_v1.MedEdWS_MedEdInsertChild_Input inputEvent = new WSOD_Event_v1.MedEdWS_MedEdInsertChild_Input();
                WSOD_Event_v1.MedEdWS_MedEdInsertChild_Output outputEvent = new WSOD_Event_v1.MedEdWS_MedEdInsertChild_Output();

                inputEvent.ListOfMedEd = new WSOD_Event_v1.MedEd1[1];
                inputEvent.ListOfMedEd[0] = new WSOD_Event_v1.MedEd1();
                inputEvent.ListOfMedEd[0].ListOfInvitee = new WSOD_Event_v1.Invitee[1];
                inputEvent.ListOfMedEd[0].ListOfInvitee[0] = new WSOD_Event_v1.Invitee();

                inputEvent.ListOfMedEd[0].MedEdId = pEventId;
                inputEvent.ListOfMedEd[0].ListOfInvitee[0].ContactId = pContactId;
                inputEvent.ListOfMedEd[0].ListOfInvitee[0].Status = pInviteeStatus;
                inputEvent.ListOfMedEd[0].ListOfInvitee[0].plInvitee_Source = pInviteeSource;
                inputEvent.ListOfMedEd[0].ListOfInvitee[0].dtDateTime_of_Registration = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                EnterLoginCredentials();

                srvcEvent.Url = mSession.GetURL();
                srvcEvent.CookieContainer = mSession.GetCookieContainer();
                outputEvent = srvcEvent.MedEdInsertChild(inputEvent);

                result = "Success";
            }
            catch (Exception ex)
            {
                LogToFile("Error on updateInviteeStatus:" + ex.Message);
                result = ex.Message;
            }

            return result;
        }

        public String postInviteeStatus(String pContactId, String pEventId, String pInviteeStatus, String pInviteeSource, String pEmail)
        {
            LogToFile("Start processing postInviteeStatus:pContactId" + pContactId + ",pEventId:" + pEventId + ",pInviteeStatus:" + pInviteeStatus + ",pInviteeSource:" + pInviteeSource + ",pEmail:" + pEmail);

            String result = "";
            try
            {
                try
                {
                    WSOD_Event_v1.MedEd srvcEvent = new WSOD_Event_v1.MedEd();
                    WSOD_Event_v1.MedEdWS_MedEdUpdateChild_Input inputEvent = new WSOD_Event_v1.MedEdWS_MedEdUpdateChild_Input();
                    WSOD_Event_v1.MedEdWS_MedEdUpdateChild_Output outputEvent = new WSOD_Event_v1.MedEdWS_MedEdUpdateChild_Output();

                    inputEvent.ListOfMedEd = new WSOD_Event_v1.MedEd1[1];
                    inputEvent.ListOfMedEd[0] = new WSOD_Event_v1.MedEd1();
                    inputEvent.ListOfMedEd[0].ListOfInvitee = new WSOD_Event_v1.Invitee[1];
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0] = new WSOD_Event_v1.Invitee();

                    inputEvent.ListOfMedEd[0].MedEdId = pEventId;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].ContactId = pContactId;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].Status = pInviteeStatus;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].plInvitee_Source = pInviteeSource;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].dtDateTime_of_Registration = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                    EnterLoginCredentials();

                    srvcEvent.Url = mSession.GetURL();
                    srvcEvent.CookieContainer = mSession.GetCookieContainer();
                    outputEvent = srvcEvent.MedEdUpdateChild(inputEvent);

                    result = "Success";
                    LogToFile("Success");
                }
                catch (Exception ex)
                {
                    LogToFile("Error on postInviteeStatus:" + ex.Message + "--> retry using InsertChild");

                    WSOD_Event_v1.MedEd srvcEvent = new WSOD_Event_v1.MedEd();
                    WSOD_Event_v1.MedEdWS_MedEdInsertChild_Input inputEvent = new WSOD_Event_v1.MedEdWS_MedEdInsertChild_Input();
                    WSOD_Event_v1.MedEdWS_MedEdInsertChild_Output outputEvent = new WSOD_Event_v1.MedEdWS_MedEdInsertChild_Output();

                    inputEvent.ListOfMedEd = new WSOD_Event_v1.MedEd1[1];
                    inputEvent.ListOfMedEd[0] = new WSOD_Event_v1.MedEd1();
                    inputEvent.ListOfMedEd[0].ListOfInvitee = new WSOD_Event_v1.Invitee[1];
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0] = new WSOD_Event_v1.Invitee();

                    inputEvent.ListOfMedEd[0].MedEdId = pEventId;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].ContactId = pContactId;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].Status = pInviteeStatus;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].plInvitee_Source = pInviteeSource;
                    inputEvent.ListOfMedEd[0].ListOfInvitee[0].dtDateTime_of_Registration = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");

                    EnterLoginCredentials();

                    srvcEvent.Url = mSession.GetURL();
                    srvcEvent.CookieContainer = mSession.GetCookieContainer();
                    outputEvent = srvcEvent.MedEdInsertChild(inputEvent);

                    result = "Success";
                    LogToFile("Successful on retry");
                }
            }
            catch (Exception ex)
            {
                LogToFile("Error on postInviteeStatus:" + ex.Message);
                result = "Failed: " + ex.Message;
            }

            return result;
        }

        public ArrayList getSRDetails(String srRowId)
        {
            ArrayList resultArr = new ArrayList();

            WSOD_ServiceRequest_v2.ServiceRequest srvcService = new WSOD_ServiceRequest_v2.ServiceRequest();
            WSOD_ServiceRequest_v2.ServiceRequestQueryPage_Input inputSR = new WSOD_ServiceRequest_v2.ServiceRequestQueryPage_Input();
            WSOD_ServiceRequest_v2.ServiceRequestQueryPage_Output outputSR = new WSOD_ServiceRequest_v2.ServiceRequestQueryPage_Output();

            inputSR.ViewMode = "AllBooks";
            inputSR.ListOfServiceRequest = new WSOD_ServiceRequest_v2.ListOfServiceRequestQuery();

            inputSR.ListOfServiceRequest.pagesize = "1";
            inputSR.ListOfServiceRequest.startrownum = "0";

            inputSR.ListOfServiceRequest.ServiceRequest = new WSOD_ServiceRequest_v2.ServiceRequestQuery();
            inputSR.ListOfServiceRequest.ServiceRequest.Id = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.AccountId = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.ContactEmail = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.Owner = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.Subject = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.Status = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.SRNumber = new WSOD_ServiceRequest_v2.queryType();
            inputSR.ListOfServiceRequest.ServiceRequest.OwnerId = new WSOD_ServiceRequest_v2.queryType();

            inputSR.ListOfServiceRequest.ServiceRequest.searchspec = "[Id]='" + srRowId + "'";

            EnterLoginCredentials();

            srvcService.Url = mSession.GetURL();
            srvcService.CookieContainer = mSession.GetCookieContainer();
            outputSR = srvcService.ServiceRequestQueryPage(inputSR);

            if (outputSR != null)
            {
                return resultArr;
            }
            else
            {
                resultArr= null;
            }

            return resultArr;
        }

         public void EnterLoginCredentials()
        {

            LogToFile("Initializing login credentials...");

            try
            {
                // for testing purposes
                username = ConfigurationManager.AppSettings["username"].ToString();
                password = ConfigurationManager.AppSettings["password"].ToString();


                LogToFile("Enter Username: " + username.ToString());

                //username = Session["username"].ToString();
                //password = Session["password"].ToString();

                servername = ConfigurationManager.AppSettings["servername"].ToString();
                LogToFile("Enter Server Name: " + servername.ToString());

                connectToCRMonDemand(servername, username, password);
            }
            catch (Exception ex)
            {
                LogToFile("Error on Login Credentials --> " + ex.Message.ToString());
            }
        }

        public static void connectToCRMonDemand(String pServerName, String pUsername, String pPassword)
        {
            mSession = new OnDemandSession();

            try
            {
                //Retrieve Connecton Parameters
                mSession.server = pServerName;
                mSession.username = pUsername;
                mSession.password = pPassword;

                mSession.Establish();
            }
            catch (Exception ex)
            {
                LogToFile("Error: connectToCRMonDemand --> " + ex.Message);
            }
        }

    }
}
