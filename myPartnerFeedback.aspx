﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="myPartnerFeedback.aspx.cs" Inherits="CRMIntegration.myPartnerFeedback" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Emerson Commercial & Residential Solutions - Customer Feedback Survey</title>
        <script type="text/javascript"  lang="javascript" src="js/external/jquery.1.9.0.min.js"></script>
    <script type="text/javascript" lang="javascript" src="js/external/json2.js"></script>
    <script type="text/javascript" lang="javascript" src="js/ajaxFunction.js"></script>
    <script type="text/javascript" lang="javascript" src="js/external/jquery-1.10.1.js"></script>
    <script type="text/javascript" lang="javascript" src="js/external/jquery-1.4.3.min.js"></script>
    <script type="text/javascript" lang="javascript" src="js/external/prototype.js"></script>
    <script type="text/javascript" lang="javascript" src="js/myPartnerPlusFeedback.js"></script>
    <script>
        var tid = '<%=ConfigurationManager.AppSettings["myPartnerPlusFeedbackAssessmentTemplateId"].ToString() %>';
        var pQ1ID = '<%=ConfigurationManager.AppSettings["myPartnerPlusFeedbackAssessmentTemplateAttribId_Q1"].ToString() %>';
        var pQ2ID = '<%=ConfigurationManager.AppSettings["myPartnerPlusFeedbackAssessmentTemplateAttribId_Q2"].ToString() %>';
        var pQ3ID = '<%=ConfigurationManager.AppSettings["myPartnerPlusFeedbackAssessmentTemplateAttribId_Q3"].ToString() %>';
    </script>

    <script type="text/javascript" language="javascript">
        function CloseWindow() {
            window.open('', '_self', '');
            window.close();
        }
    </script>

     <script>jQuery.noConflict();</script>

    <style>
        td.text
        {
            text-align: left;
            font-family: 'Segoe UI', Segoe, SegoeUIalt, Segoealt, sans-serif;
            font-size: 16px;
            line-height: 34.511px;
            color:#3f4040;
            font-weight:normal;
        }

        td.label
        {
            text-align: left;
            font-family: 'Segoe UI', Segoe, SegoeUIalt, Segoealt, sans-serif;
            font-size:  21.33px;
            line-height: 34.511px;
            color:#3f4040;
            font-weight:bold;
        }

        td.header
        {
            text-align: left;
            font-family: 'Segoe UI', Segoe, SegoeUIalt, Segoealt, sans-serif;
            font-weight: 400;
            font-size: 42.67px;
            line-height: 69.04px;
            color:#FFFFFF;
            background-color:#0079c1;
        }

        input.button 
        {
                background-color: #62bb46; /* Green */
                border-radius: 12px;
                color: white;
                padding: 15px 32px;
                text-align: center;
                font-family: 'Segoe UI', Segoe, SegoeUIalt, Segoealt, sans-serif;
                font-size: 16px;
                margin-right:auto;
         }

        #q2R_comments
        {
            text-align: left;
            font-family: 'Segoe UI', Segoe, SegoeUIalt, Segoealt, sans-serif;
            font-size: 16px;
            line-height: 34.511px;
            color:#3f4040;
            font-weight:normal;
            padding: 10px;
        }

        div.default
        {
            background-color:#0079c1
            
        }

        
    </style>
</head>
<body onload="javascript:getSRId()">
    <form id="form1" runat="server">
    <div id="pageheader">
        <table>
            <tr>
                <td class="header" style="width: 100%; padding:10px"><img src="images/logo-emr-white.png"/> Partner+ Customer Feedback Survey</td>
            </tr>
        </table>
        </div>     
    <div id="surveydetails">
    </div>
    </form>
</body>
</html>
